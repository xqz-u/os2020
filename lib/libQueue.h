#ifndef LIBQUEUE_H
#define LIBQUEUE_H

#include "libProcess.h"

typedef struct Queue {
  Process *procs;
  int front, back, size;
} Queue;

void *safeMalloc(int sz);
Queue *newQueue(int size);
int isEmptyQueue(Queue *qp);
void queueEmptyError();
void doubleQueueSize(Queue *qp);
void addFront(Queue *qp, Process p);
Process deleteLast(Queue *qp);
void freeQueue(Queue *qp);
Process peekFront(Queue *qp);

#endif
