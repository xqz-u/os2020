#include <assert.h>
#include <stdlib.h>

#include "libDebug.h"
#include "libHeap.h"
#include "libPrinters.h"

Heap *newHeap(int size) {
  Heap *hp = safeMalloc(sizeof *hp);
  *hp = (Heap){.procs = safeMalloc(sizeof(Process) * size), size, .last = 0};
  return hp;
}

int isEmptyHeap(Heap *hp) { return (hp->last < HEAP_ROOT); }

void heapEmptyError() {
  throw_error(stderr, "heap empty! exit(%d)", EXIT_FAILURE);
  exit(EXIT_FAILURE);
}

void doubleHeapSize(Heap *hp) {
  hp->size *= 2;
  hp->procs = realloc(hp->procs, sizeof *(hp->procs) * hp->size);
  assert(hp->procs != NULL);
}

void swapProc(Process *a, Process *b) {
  Process tmp = *a;
  *a = *b;
  *b = tmp;
}

int satisfyHeapProperty(Process *a, Process *b) {
  if (a->prio < b->prio)
    return 1;

  if (a->prio == b->prio) {
    if (a->last_ready < b->last_ready)
      return 1;
    if (a->last_ready == b->last_ready)
      return (a->exec_time < b->exec_time);
  }

  return 0;
}

void upHeap(Heap *hp, int idx) {
  if (idx <= HEAP_ROOT)
    return;
  Process *parent = &hp->procs[PARENT(idx)];
  Process *curr = &hp->procs[idx];
  if (satisfyHeapProperty(parent, curr))
    return;
  swapProc(parent, curr);
  upHeap(hp, idx / 2);
}

// NOTE this method allocates new memory, remember to free the original process!
void insert(Heap *hp, Process p) {
  if (++hp->last >= hp->size)
    doubleHeapSize(hp);
  hp->procs[hp->last] = deepCopy(p);
  upHeap(hp, hp->last);
  printf("Heap after insertion:\n");
  printHeap(hp);
}

int findMinChild(Heap *hp, int idx) {
  printf("Parent proc at idx %d is:\n", idx);
  printProc(hp->procs[idx]);

  if (LEFT(idx) > hp->last) {
    printf("Parent does not have children! min child is parent itself\n");
    return idx;
  }

  if (RIGHT(idx) > hp->last) {
    printf("No right child, min child will be left!\n");
    return LEFT(idx);
  }

  printf("Left child proc at index %d is:\n", LEFT(idx));
  printProc(hp->procs[LEFT(idx)]);
  printf("Right child proc at idx %d is:\n", RIGHT(idx));
  printProc(hp->procs[RIGHT(idx)]);
  return (satisfyHeapProperty(&hp->procs[LEFT(idx)], &hp->procs[RIGHT(idx)])
              ? LEFT(idx)
              : RIGHT(idx));
}

void downHeap(Heap *hp, int idx) {
  if (idx >= hp->last) {
    printf("idx %d == last %d, return!\n", idx, hp->last);
    return;
  }
  int min_child_idx = findMinChild(hp, idx);
  Process *parent = &hp->procs[idx];
  printf("min child proc is at index %d!\n", min_child_idx);
  Process *min_child = &hp->procs[min_child_idx];
  printProc(*min_child);
  if (satisfyHeapProperty(parent, min_child) || idx == min_child_idx) {
    printf(
        "Either parent's prio (%d) is higher than min child's (%d) or "
        "they're equal and parent is older (%d) than min child (%d), return!\n",
        parent->prio, min_child->prio, parent->age, min_child->age);
    return;
  }
  printf("Heap property is not satisfied or it is (==) but parent is younger "
         "than min child (%d par age, %d child age, swap and recur!)\n",
         parent->age, min_child->age);
  swapProc(parent, min_child);
  printf("DONE SWAP!\n");
  downHeap(hp, min_child_idx);
}

// NOTE this method returns allocated memory to be freed!
Process deleteRoot(Heap *hp) {
  if (isEmptyHeap(hp))
    heapEmptyError();
  printf("Inital heap:\n");
  printHeap(hp);
  Process root = deepCopy(hp->procs[HEAP_ROOT]);
  freeProcess(hp->procs[HEAP_ROOT]);
  // Avoid invalid read when last process remaining is the root!
  if (hp->last == HEAP_ROOT) {
    hp->last--;
    return root;
  }
  hp->procs[HEAP_ROOT] = deepCopy(hp->procs[hp->last]);
  freeProcess(hp->procs[hp->last--]);
  printf(",,,,,,,,,,,,,,,,,,,,,,,\n");
  downHeap(hp, HEAP_ROOT);
  printf(",,,,,,,,,,,,,,,,,,,,,,,\n");
  printf("Heap before returning old root:\n");
  printHeap(hp);
  return root;
}

Process peekRoot(Heap *hp) {
  if (isEmptyHeap(hp))
    heapEmptyError();
  return hp->procs[HEAP_ROOT];
}

void freeHeap(Heap *hp) {
  for (int i = HEAP_ROOT; i <= hp->last; i++)
    freeProcess(hp->procs[i]);
  free(hp->procs);
  free(hp);
}
