#ifndef LIBPROCESS_H
#define LIBPROCESS_H

typedef struct Process {
  int start, prio;
  int size, n_jobs, *jobs, curr;
  int exec_time, age, served, last_ready;
} Process;

Process newProcess(int start, int prio, int size);
void resizeProcess(Process *pp);
void appendJob(Process *pp, int job);
int retrieveJob(Process p, int idx);
void freeProcess(Process p);
int *copyArr(int *og, int og_len);
Process deepCopy(Process og);

#endif
