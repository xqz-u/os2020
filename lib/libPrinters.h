#ifndef LIBPRINTERS_H
#define LIBPRINTERS_H

#include "./libProcess.h"
#include "./libScheduling.h"

void printProc(Process p);
void printQueue(Queue *qp);
void printHeap(Heap *hp);
void printQueues(Queues container);

#endif
