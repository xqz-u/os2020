#include <stdio.h>
#include <stdlib.h>

#include "libDebug.h"
#include "libProcess.h"

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    throw_error(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

Process newProcess(int start, int prio, int size) {
  return (Process){start,
                   prio,
                   size,
                   .n_jobs = 0,
                   .jobs = safeMalloc(sizeof(int) * size),
                   .curr = 0,
                   .exec_time = 0,
                   .age = 0,
                   .served = 0,
                   .last_ready = 0};
}

void resizeProcess(Process *pp) {
  pp->size *= 2;
  if ((pp->jobs = realloc(pp->jobs, sizeof *(pp->jobs) * pp->size)) == NULL) {
    throw_error(stderr, "Failed memory reallocation of size %d!", pp->size);
    exit(EXIT_FAILURE);
  }
}

void appendJob(Process *pp, int new_job) {
  if (pp->n_jobs >= pp->size)
    resizeProcess(pp);
  pp->jobs[pp->n_jobs++] = new_job;
}

int retrieveJob(Process p, int idx) {
  if (idx > p.n_jobs)
    throw_error(stderr, "idx too high! have %d, current tot is %d", idx,
                p.n_jobs);
  return p.jobs[idx];
}

void freeProcess(Process p) { free(p.jobs); }

int *copyArr(int *og, int og_len) {
  int *ret = safeMalloc(sizeof *ret * og_len);
  for (int i = 0; i < og_len; i++)
    ret[i] = og[i];
  return ret;
}

Process deepCopy(Process og) {
  return (Process){
      og.start, og.prio,      og.size, og.n_jobs, copyArr(og.jobs, og.n_jobs),
      og.curr,  og.exec_time, og.age,  og.served, og.last_ready};
}
