#include <stdio.h>
#include <stdlib.h>

#include "./libDebug.h"
#include "./libScheduling.h"

Queues newQueues(int size, QueueType type) {
  Queues ret;
  ret.queue_t = type;
  switch (ret.queue_t) {
  case FIFO:
    ret.store.queue = newQueue(size);
    return ret;
  case Priority:
    ret.store.heap = newHeap(size);
    return ret;
  default:
    throw_error(stderr, "Creating Queues object: type %d is not defined!\n",
                ret.queue_t);
    exit(EXIT_FAILURE);
  }
}

void freeQueues(Queues wrapper) {
  switch (wrapper.queue_t) {
  case FIFO:
    freeQueue(wrapper.store.queue);
    return;
  case Priority:
    freeHeap(wrapper.store.heap);
    return;
  default:
    throw_error(stderr, "Freeing Queues object: not defined type %d found!\n",
                wrapper.queue_t);
    exit(EXIT_FAILURE);
  }
}

void enqueue(Queues wrapper, Process p) {
  switch (wrapper.queue_t) {
  case FIFO:
    addFront(wrapper.store.queue, p);
    return;
  case Priority:
    insert(wrapper.store.heap, p);
    return;
  default:
    throw_error(stderr,
                "Inserting in Queues object: not defined type %d found!\n",
                wrapper.queue_t);
    exit(EXIT_FAILURE);
  }
}

Process dequeue(Queues wrapper) {
  switch (wrapper.queue_t) {
  case FIFO:
    return deleteLast(wrapper.store.queue);
  case Priority:
    return deleteRoot(wrapper.store.heap);
  default:
    throw_error(stderr,
                "Extracting from Queues object: not defined type %d found!\n",
                wrapper.queue_t);
    exit(EXIT_FAILURE);
  }
}

Process peek(Queues wrapper) {
  switch (wrapper.queue_t) {
  case FIFO:
    return peekFront(wrapper.store.queue);
  case Priority:
    return peekRoot(wrapper.store.heap);
  default:
    throw_error(stderr, "Peeking Queues object: not defined type %d found!\n",
                wrapper.queue_t);
    exit(EXIT_FAILURE);
  }
}

int isEmptyQueues(Queues wrapper) {
  switch (wrapper.queue_t) {
  case FIFO:
    return isEmptyQueue(wrapper.store.queue);
  case Priority:
    return isEmptyHeap(wrapper.store.heap);
  default:
    throw_error(stderr,
                "Checking Queues object for empty: undefined type %d found!\n",
                wrapper.queue_t);
    exit(EXIT_FAILURE);
  }
}
