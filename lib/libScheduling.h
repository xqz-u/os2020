#ifndef LIBSCHEDULING_H
#define LIBSCHEDULING_H

#include "libHeap.h"
#include "libQueue.h"

typedef enum QueueType { Priority, FIFO } QueueType;

typedef union QueueWrap {
  Queue *queue;
  Heap *heap;
} QueueWrap;

typedef struct Queues {
  QueueType queue_t;
  QueueWrap store;
} Queues;

typedef void (*queueInspect)(int, Queues, Queues);

Queues newQueues(int size, QueueType type);
void freeQueues(Queues container);
int isEmptyQueues(Queues wrapper);
void enqueue(Queues wrapper, Process p);
Process dequeue(Queues wrapper);
Process peek(Queues wrapper);

#endif
