#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "./libDebug.h"
#include "./libProcess.h"
#include "./libQueue.h"

Queue *newQueue(int size) {
  Queue *qp = safeMalloc(sizeof *qp);
  *qp = (Queue){
      .procs = safeMalloc(sizeof(Process) * size), .front = 0, .back = 0, size};
  return qp;
}

int isEmptyQueue(Queue *qp) { return (qp->back == qp->front); }

void queueEmptyError() {
  throw_error(stderr, "queue empty! exit(%d)", EXIT_FAILURE);
  exit(EXIT_FAILURE);
}

void doubleQueueSize(Queue *qp) {
  int i, oldSize = qp->size;
  qp->size *= 2;
  qp->procs = realloc(qp->procs, sizeof *(qp->procs) * qp->size);
  assert(qp->procs != NULL);
  for (i = 0; i < qp->back; i++) /* eliminate the split configuration */
    qp->procs[oldSize + i] = qp->procs[i];
  qp->back += oldSize; /* update qp->back */
}

// NOTE this method allocates new memory, make sure to free the original
// process!
void addFront(Queue *qp, Process p) {
  qp->procs[qp->back] = deepCopy(p);
  qp->back = (qp->back + 1) % qp->size;
  if (qp->back == qp->front)
    doubleQueueSize(qp);
}

// NOTE this method returns allocated memory to be freed!
Process deleteLast(Queue *qp) {
  if (isEmptyQueue(qp))
    queueEmptyError();
  Process p = deepCopy(qp->procs[qp->front]);
  freeProcess(qp->procs[qp->front]);
  qp->front = (qp->front + 1) % qp->size;
  return p;
}

void freeQueue(Queue *qp) {
  while (!isEmptyQueue(qp))
    freeProcess(deleteLast(qp));
  free(qp->procs);
  free(qp);
}

Process peekFront(Queue *qp) {
  if (isEmptyQueue(qp))
    queueEmptyError();
  return qp->procs[qp->front];
}
