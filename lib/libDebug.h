#ifndef LIBDEBUG_H
#define LIBDEBUG_H

#include <stdio.h>

void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...);

#define throw_error(fd, formatter, ...)                                        \
  errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

#endif
