#include <stdio.h>
#include <stdlib.h>

#include "libDebug.h"
#include "libPrinters.h"

void printProc(Process p) {
  printf("\tstart: %d, priority: %d, size: %d\n", p.start, p.prio, p.size);
  printf(
      "\tcurrent job: %d, execution: %d, age: %d, served: %d, last ready %d\n",
      p.curr, p.exec_time, p.age, p.served, p.last_ready);
  printf("\t%d: [", p.n_jobs);
  if (p.curr < p.n_jobs) {
    printf("%d", retrieveJob(p, p.curr));
    for (int i = p.curr + 1; i < p.n_jobs; i++)
      printf(",%d", retrieveJob(p, i));
  }
  printf("]\n");
}

void printQueue(Queue *qp) {
  printf("Queue size: %d, front: %d, back: %d\n", qp->size, qp->front,
         qp->back);
  for (int i = qp->front; i != qp->back; i = (i + 1) % qp->size) {
    printProc(qp->procs[i]);
    if ((i + 1) % qp->size != qp->back)
      printf("\n");
  }
}

void printHeap(Heap *hp) {
  printf("Processes in heap: %d, heap size: %d\n", hp->last, hp->size);
  for (int i = HEAP_ROOT; i <= hp->last; i++) {
    printProc(hp->procs[i]);
    if (i + 1 <= hp->last)
      printf("\n");
  }
}

void printQueues(Queues container) {
  switch (container.queue_t) {
  case FIFO:
    printQueue(container.store.queue);
    return;
  case Priority:
    printHeap(container.store.heap);
    return;
  default:
    throw_error(stderr, "Printing Queues object of undefined type (%d)!\n",
                container.queue_t);
    exit(EXIT_FAILURE);
  }
}
