#ifndef LIBHEAP_H
#define LIBHEAP_H

#include "libProcess.h"

#define HEAP_ROOT 1
#define LEFT(i) (2 * (i))
#define RIGHT(i) ((2 * (i)) + 1)
#define PARENT(i) ((i) / 2)

typedef struct Heap {
  Process *procs;
  int size, last;
} Heap;

void *safeMalloc(int sz);
Heap *newHeap(int size);
void freeHeap(Heap *hp);
int isEmptyHeap(Heap *hp);
void doubleHeapSize(Heap *hp);
void swapProc(Process *a, Process *b);
int satisfyHeapProperty(Process *a, Process *b);
void upHeap(Heap *hp, int idx);
int findMinChild(Heap *hp, int idx);
void downHeap(Heap *hp, int idx);
void insert(Heap *hp, Process p);
Process deleteRoot(Heap *hp);
Process peekRoot(Heap *hp);

#endif
