#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "../lib/libHeap.h"
#define MAX_PROCESS_SIZE 5

void checkResize(void **store, int filled, int *size, size_t type) {
  if (filled < *size)
    return;
  *size *= 2;
  *store = realloc(*store, type * (*size));
  assert(*store != NULL);
}

void readProcs(Process **store, int *size, int *entries) {
  Process proc;
  int start, prio;
  int in, n_read, valid = 0;

  *entries = 0;
  while ((n_read = scanf("%d", &start)) > 0) {
    scanf("%d ", &prio);
    proc = newProcess(start, prio, MAX_PROCESS_SIZE);
    scanf("%d", &in);
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid) {
      (*store)[(*entries)++] = deepCopy(proc);
      checkResize((void **)store, *entries, size, sizeof(Process));
    }
    freeProcess(proc);
    valid = 0;
  }
}

int main(void) {
  int procs_sz = 1, filled, i;
  int heap_sz = 1;

  Process *test_store = safeMalloc(sizeof *test_store * procs_sz);
  readProcs(&test_store, &procs_sz, &filled);

  printf("OG list, size %d filled %d\n", procs_sz, filled);
  for (int i = 0; i < filled; i++)
    printProc(test_store[i]);

  Heap test_heap = newHeap(heap_sz);
  for (i = 0; i < filled; i++)
    enqueue(&test_heap, test_store[i]);

  printf("Heap list!\n");
  printHeap(test_heap);

  printf("Elements ->\n");
  while (!isEmptyHeap(test_heap))
    printProc(dequeue(&test_heap));

  freeHeap(test_heap);
  freeProcesses(test_store, filled);
  return 0;
}
