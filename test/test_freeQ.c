#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MAX_QUEUE_SIZE 10
#define MAX_PROCESS_SIZE 15

// Error handling
void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  if (vfprintf(err_dest, cformat, err_args) <= 0)
    fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throw_error(fd, formatter, ...)                                        \
  errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

typedef struct Process {
  int start, prio;
  int size, n_jobs, *jobs, curr;
  int exec_time;
} Process;

typedef struct Queue {
  Process *procs;
  int front, back, size, tot;
} Queue;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    throw_error(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

Process newProcess(int start, int prio, int size) {
  return (Process){start,
                   prio,
                   size,
                   .n_jobs = 0,
                   .jobs = safeMalloc(sizeof(int) * size),
                   .curr = 0,
                   .exec_time = 0};
}

void resizeProcess(Process *pp) {
  pp->size *= 2;
  if ((pp->jobs = realloc(pp->jobs, sizeof *(pp->jobs) * pp->size)) == NULL) {
    throw_error(stderr, "Failed memory reallocation of size %d!", pp->size);
    exit(EXIT_FAILURE);
  }
}

void appendJob(Process *pp, int new_job) {
  if (pp->n_jobs >= pp->size)
    resizeProcess(pp);
  pp->jobs[pp->n_jobs++] = new_job;
}

int retrieveJob(Process p, int idx) {
  if (idx > p.n_jobs)
    throw_error(stderr, "idx too high! have %d, current tot is %d", idx,
                p.n_jobs);
  return p.jobs[idx];
}

void freeProcess(Process p) { free(p.jobs); }

void freeProcesses(Process *p_store, int size) {
  for (int i = 0; i < size; i++)
    freeProcess(p_store[i]);
}

void printProc(Process p) {
  printf("start: %d, priority: %d, size: %d\n", p.start, p.prio, p.size);
  printf("\t%d: [", p.n_jobs);
  if (p.n_jobs > 0) {
    printf("%d", retrieveJob(p, 0));
    for (int i = 1; i < p.n_jobs; i++)
      printf(",%d", retrieveJob(p, i));
  }
  printf("]\n");
  printf("\tcurrent job: %d, execution: %d\n", p.curr, p.exec_time);
}

int *copyArr(int *og, int og_len) {
  int *ret = safeMalloc(sizeof *ret * og_len);
  for (int i = 0; i < og_len; i++)
    ret[i] = og[i];
  return ret;
}

Process deepCopy(Process og) {
  return (Process){
      og.start, og.prio,     og.size, og.n_jobs, copyArr(og.jobs, og.n_jobs),
      og.curr,  og.exec_time};
}

Queue newQueue(int size) {
  return (Queue){.procs = safeMalloc(sizeof(Process) * size),
                 .front = 0,
                 .back = 0,
                 size,
                 .tot = 0};
}

int isEmptyQueue(Queue q) { return (q.back == q.front); }

void queueEmptyError() {
  throw_error(stderr, "queue empty! exit(%d)", EXIT_FAILURE);
  exit(EXIT_FAILURE);
}

void doubleQueueSize(Queue *qp) {
  int i, oldSize = qp->size;
  qp->size *= 2;
  qp->procs = realloc(qp->procs, sizeof *(qp->procs) * qp->size);
  assert(qp->procs != NULL);
  for (i = 0; i < qp->back; i++) /* eliminate the split configuration */
    qp->procs[oldSize + i] = qp->procs[i];
  qp->back += oldSize; /* update qp->back */
}

void enqueue(Queue *qp, Process p) {
  qp->procs[qp->back] = p;
  qp->back = (qp->back + 1) % qp->size;
  if (qp->back == qp->front)
    doubleQueueSize(qp);
}

Process dequeue(Queue *qp) {
  if (isEmptyQueue(*qp))
    queueEmptyError();
  Process p = qp->procs[qp->front];
  qp->front = (qp->front + 1) % qp->size;
  return p;
}

void freeQueue(Queue q) {
  freeProcesses(q.procs, q.tot);
  free(q.procs);
}

void printQueue(Queue q) {
  printf("Queue size: %d, total entries: %d\nfront: %d, back: %d\n", q.size,
         q.tot, q.front, q.back);
  for (int i = q.front; i != q.back; i = (i + 1) % q.size) {
    printf("\tidx: %d, ", i);
    printProc(q.procs[i]);
    if ((i + 1) % q.size != q.back)
      printf("\n");
  }
}

Process peek(Queue q) {
  if (isEmptyQueue(q))
    queueEmptyError();
  return q.procs[q.front];
}

void getInput(Queue *qp) {
  Process proc;
  int start, prio;
  int in, n_read, valid = 0;

  while ((n_read = scanf("%d", &start)) > 0) {
    scanf("%d ", &prio);
    proc = newProcess(start, prio, MAX_PROCESS_SIZE);
    scanf("%d", &in);
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid) {
      enqueue(qp, deepCopy(proc));
      qp->tot++;
    }
    freeProcess(proc);
    valid = 0;
  }
}

void addNewProcs(int time_elapsed, Queue *sched_p, Queue *arrival_lp) {
  if (peek(*arrival_lp).start > time_elapsed)
    return;
  while (!isEmptyQueue(*arrival_lp) &&
         peek(*arrival_lp).start <= time_elapsed) {
    enqueue(sched_p, deepCopy(dequeue(arrival_lp)));
    sched_p->tot++;
  }
}

void checkNewRequests(int time_elapsed, Queue *sched_p, Queue *arrival_lp,
                      int job_done, int *total) {
  if (!isEmptyQueue(*arrival_lp))
    addNewProcs(time_elapsed, sched_p, arrival_lp);
  if (job_done)
    *total += 1;
}

int processJob(Process *p, int *time, Queue *q) {
  if (p->curr >= p->n_jobs)
    return 1;
  *time = MAX(p->exec_time, *time);
  p->exec_time = *time + retrieveJob(*p, p->curr++);
  *time = p->exec_time;
  return 0;
}

double averageTurnaround(Queue *scheduler, Queue *inventory) {
  int CPU_time, IO_time;
  int end_job, completed = 0;
  double turnaround = 0;
  Process p;

  CPU_time = IO_time = 0;
  while (completed < inventory->tot) {
    p = dequeue(scheduler);
    end_job = processJob(&p, &CPU_time, scheduler) ||
              processJob(&p, &IO_time, scheduler);
    checkNewRequests(IO_time, scheduler, inventory, end_job, &completed);
    enqueue(scheduler, p);
  }

  while (!isEmptyQueue(*scheduler)) {
    p = dequeue(scheduler);
    turnaround += (p.exec_time - p.start);
  }

  return turnaround / scheduler->tot;
}

void initScheduler(Queue *sched_p, Queue *arrival_lp) {
  if (isEmptyQueue(*arrival_lp)) {
    throw_error(stderr, "Error! initial list is empty! exit(%d)", EXIT_FAILURE);
    exit(EXIT_FAILURE);
  }
  while ((isEmptyQueue(*sched_p) ||
          peek(*arrival_lp).start == peek(*sched_p).start) &&
         !isEmptyQueue(*arrival_lp)) {
    enqueue(sched_p, deepCopy(dequeue(arrival_lp)));
    sched_p->tot++;
  }
}

int main(int argc, char *argv[]) {
  Queue procs_inventory = newQueue(MAX_QUEUE_SIZE);
  getInput(&procs_inventory);

  Queue scheduler_FCFS = newQueue(procs_inventory.tot);
  initScheduler(&scheduler_FCFS, &procs_inventory);

  printf("%.0f\n", averageTurnaround(&scheduler_FCFS, &procs_inventory));

  freeQueue(procs_inventory);
  freeQueue(scheduler_FCFS);
  return 0;
}
