#include <stdio.h>
#include <stdlib.h>

#include "../../../lib/libHeap.h"

#define MAX_HEAP_SIZE 1
#define MAX_PROCESS_SIZE 1

void getInput(Heap *hp) {
  Process proc;
  int start, prio;
  int in, valid = 0;

  while (scanf("%d", &start) > 0) {
    scanf("%d ", &prio);
    proc = newProcess(start, prio, MAX_PROCESS_SIZE);
    scanf("%d", &in);
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid)
      enqueue(hp, proc);
    freeProcess(proc);
    valid = 0;
  }
}

int main(int argc, char *argv[]) {
  Heap procs_inventory = newHeap(MAX_HEAP_SIZE);
  getInput(&procs_inventory);

  printHeap(procs_inventory);

  Process first, sec, third;
  first = dequeue(&procs_inventory);
  sec = dequeue(&procs_inventory);
  third = dequeue(&procs_inventory);

  printHeap(procs_inventory);

  enqueue(&procs_inventory, first);
  enqueue(&procs_inventory, sec);
  enqueue(&procs_inventory, third);

  printHeap(procs_inventory);

  Process tmp;
  while (!isEmptyHeap(procs_inventory)) {
    tmp = dequeue(&procs_inventory);
    printProc(tmp);
    freeProcess(tmp);
    printf("\n");
  }

  freeProcess(first);
  freeProcess(sec);
  freeProcess(third);
  freeHeap(procs_inventory);
  return 0;
}
