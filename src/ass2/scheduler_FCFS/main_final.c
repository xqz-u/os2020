#include <stdio.h>
#include <stdlib.h>

#include "libDebug.h"      // Debugging utility
#include "libScheduling.h" // Main module which imports all the data structures used in both scheduling exercises

#define MAX(a, b) ((a) > (b) ? (a) : (b))
// General macros used when saving the input
#define MAX_PROCESS_NUM 10
#define MAX_JOBS_NUM 15

// NOTE main assumption: there is only one CPU and one IO module, so processes
// cannot use either simultaneously
// NOTE all the function called by checkRequests rely on the input processes
// being sorted by start time

// Simple wrapper function to optimize the dequeue and enqueue of a process
// between two queues
static inline void serveQueue(Queues insertion, Queues deletion) {
  Process tmp = dequeue(deletion);
  enqueue(insertion, tmp);
  freeProcess(tmp);
}

// Free the memory allocated to save the input and exit with an error if the
// input does not comply with the assignment's requirements
int wrongInputError(Queues init_tmp_queue) {
  freeQueues(init_tmp_queue);
  throw_error(stderr, "Error! Empty initial list, return %d", EXIT_FAILURE);
  return EXIT_FAILURE;
}

// Save the input processes and return the number of valid processes read
int getInput(Queues q) {
  Process proc;
  int start, prio;
  int in, valid, cnt = 0;

  while (scanf("%d", &start) > 0) {
    scanf("%d", &prio);
    proc = newProcess(start, prio, MAX_JOBS_NUM);
    scanf("%d", &in);
    valid = 0;
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid) {
      enqueue(q, proc);
      cnt++;
    }
    freeProcess(proc);
  }

  return cnt;
}

// Initialize the scheduler with all those processes that made a request at the
// same time (the first parameter is actually useless and is only present to
// comply with the signature of the function pointer queueInspect).
void initScheduler(int _, Queues scheduler, Queues arrival_list) {
  while (!isEmptyQueues(arrival_list) &&
         ((isEmptyQueues(scheduler) ||
           peek(arrival_list).start == peek(scheduler).start)))
    serveQueue(scheduler, arrival_list);
}

// Schedule for execution all those processes that arrived before the given time
void addNewProcs(int time, Queues scheduler, Queues arrival_list) {
  while (!isEmptyQueues(arrival_list) && peek(arrival_list).start <= time)
    serveQueue(scheduler, arrival_list);
}

// Call the given procs_adder_func when the deletion queue still contains
// processes
void checkRequests(int time, Queues insertion_q, Queues deletion_q,
                   queueInspect procs_adder_func) {
  if (isEmptyQueues(deletion_q))
    return;
  procs_adder_func(time, insertion_q, deletion_q);
}

// Compute the average turnaround after all processes are executed
double averageTurnaround(Queues final_list) {
  double turnaround = 0;
  int tot = 0;
  Process p;
  while (!isEmptyQueues(final_list)) {
    p = dequeue(final_list);
    tot++;
    // Turnaround = time of process completion - start time of process
    // scheduling
    turnaround += (p.exec_time - p.start);
    freeProcess(p);
  }
  freeQueues(final_list);
  // Average turnaround = total turnaround / # of processes
  return turnaround / tot;
}

// This function consumes a process' jobs (either CPU or IO) and updates the
// relevant module's time before and after the job execution
int processJob(Process *p, int *time) {
  // Update the given module's - CPU/IO - time, considering possible time gaps
  // in a module between requests
  *time = MAX(p->exec_time, *time);
  // Update this process' execution time (current module's time + process'
  // current job duration) and its pointer to the next job to be performed
  p->exec_time = *time + retrieveJob(*p, p->curr++);
  // The relevant module's new time equals the module's time after the job
  // execution
  *time = p->exec_time;
  // Check if process has completely finished its execution
  return (p->curr >= p->n_jobs);
}

// The FCFS algorithm used for the scheduling of multiple processes
double FCFSscheduler(Queues scheduler, Queues arrivals) {
  // Queue used to save the final state of the processes after execution
  Queues final = newQueues(arrivals.store.queue->size, FIFO);
  // Two variables used to keep track of a module's (CPU/IO) time and a flag to
  // signal the end of a process' execution
  int cpu_time, io_time, proc_done;
  Process p;

  cpu_time = io_time = 0;
  while (!isEmptyQueues(scheduler)) {
    // Retrieve the right process in a FIFO-like fashion
    p = dequeue(scheduler);
    // A process finishes execution when either of the two calls to processJob
    // returns 1
    proc_done = processJob(&p, &cpu_time) || processJob(&p, &io_time);
    // Schedule those processes that requested CPU while the running process was
    // using it (and possibly doing IO too - so give the maximum between the two
    // times) with the addNewProcs function
    checkRequests(MAX(cpu_time, io_time), scheduler, arrivals, addNewProcs);
    // If the running process finished execution, stop scheduling it and save
    // its final state, otherwise schedule it again in a FIFO-like fashion
    enqueue((proc_done ? final : scheduler), p);
    freeProcess(p);
  }

  return averageTurnaround(final); // Compute the average turnaround time
}

int main(int argc, char *argv[]) {
  // Queue used to save the input
  Queues arrivals = newQueues(MAX_PROCESS_NUM, FIFO);

  if (!getInput(arrivals)) // Exit if illegal input is given
    wrongInputError(arrivals);

  // The actual FCFS scheduling queue
  Queues scheduler = newQueues(arrivals.store.queue->size, FIFO);

  // Initialize the scheduler with the function initScheduler and the input
  // queue
  checkRequests(-1, scheduler, arrivals, initScheduler);

  printf("%.0f\n", FCFSscheduler(scheduler, arrivals));

  // Free the memory
  freeQueues(scheduler);
  freeQueues(arrivals);
  return 0;
}
