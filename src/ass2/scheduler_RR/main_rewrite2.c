#include "../../../lib/libDebug.h"
#include "../../../lib/libPrinters.h"
#include "../../../lib/libScheduling.h"

#define MAX_PROCESS_NUM 10
#define MAX_JOBS_NUM 15
#define QUANTUM 10
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

static inline void serveQueue(Queues insertion, Queues deletion) {
  Process tmp = dequeue(deletion);
  enqueue(insertion, tmp);
  freeProcess(tmp);
}

void getInput(Queues q) {
  Process proc;
  int start, prio;
  int in, valid = 0;

  while (scanf("%d", &start) > 0) {
    scanf("%d ", &prio);
    proc = newProcess(start, prio, MAX_JOBS_NUM);
    scanf("%d", &in);
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid)
      enqueue(q, proc);
    freeProcess(proc);
    valid = 0;
  }
}

void initReadyQueue(Queues ready_queue, Queues arrival_list) {
  while (!isEmptyQueues(arrival_list) &&
         (isEmptyQueues(ready_queue) ||
          peek(arrival_list).start == peek(ready_queue).start))
    serveQueue(ready_queue, arrival_list);
}

double averageTurnaround(Queues final_list) {
  double turnaround = 0;
  int tot = 0;
  Process p;
  while (!isEmptyQueues(final_list)) {
    p = dequeue(final_list);
    tot++;
    // Turnaround = time of process completion - start time of process
    // scheduling
    turnaround += (p.exec_time - p.start);
    freeProcess(p);
  }
  freeQueues(final_list);
  // Average turnaround = total turnaround / n of processes
  return turnaround / tot;
}

void addNewProcs(int time, Queues ready_queue, Queues new_requests) {
  /* printf("inside addNewProcs, time given is %d\n", time); */
  int tmp = 0;
  while (!isEmptyQueues(new_requests) && peek(new_requests).start <= time) {
    serveQueue(ready_queue, new_requests);
    tmp++;
  }
  /* printf("Added %d new procs\n", tmp); */
  return;
}

void addIOProcs(int time, Queues ready_queue, Queues IO_requests) {
  /* printf("inside addIOProcs, time given is %d\n", time); */
  int tmp = 0;
  while (!isEmptyQueues(IO_requests) && peek(IO_requests).exec_time <= time) {
    serveQueue(ready_queue, IO_requests);
    tmp++;
  }
  /* printf("Added %d IO procs\n", tmp); */
}

int increaseAge(Process *p, int cpu_t, int round_exec_t) {
  return p->age =
             (p->served ? p->age + MIN(round_exec_t, (cpu_t - p->exec_time))
                        : cpu_t - p->start);
}

void updatePriority(Process *p, int cpu_t, int round_exec_t) {
  if (increaseAge(p, cpu_t, round_exec_t) > 100 && p->prio != 1) {
    p->prio--;
    p->age = 0;
  }
}

// NOTE this method is not the most general as it relies on the Queues object to
// be a heap, could do a dequeue-enqueue approach while the heap is full but
// this way it is faster (and the original order of processes in the heap is
// maintained)
int dynamicPriorities(Queues ready_procs, int cpu_t, int round_exec_t) {
  int i, cnt = 0;
  for (i = HEAP_ROOT; i <= ready_procs.store.heap->last; i++) {
    updatePriority(&ready_procs.store.heap->procs[i], cpu_t, round_exec_t);
    cnt++;
  }
  /* printf("Updated ages of %d procs!\n", cnt); */
  return cnt;
}

void endProcessing(Process p, Queues ready_queue, Queues final,
                   int jobs_completed, int blocked_IO) {
  if (jobs_completed) {
    enqueue(final, p);
    /* printf("Proc finished jobs, equeue in final list!\n"); */
    return;
  }
  if (!blocked_IO) {
    enqueue(ready_queue, p);
    /* printf("Proc did not finish and is not blocked in IO, enqueue in ready "
     */
    /*        "list!\n"); */
  }
}

void checkRequests(int global_t, Queues insertion_q, Queues deletion_q,
                   queueInspect procs_adder_func) {
  if (isEmptyQueues(deletion_q)) {
    /* printf("... but the given deletion queue is empty!\n"); */
    return;
  }
  procs_adder_func(global_t, insertion_q, deletion_q);
}

void fillTimeGap(Queues ready_queue, Queues IO_requests, Queues new_requests,
                 int is_blocked) {
  if (isEmptyQueues(IO_requests)) {
    /* printf("cannot force IO requests...\n"); */
    if (!isEmptyQueues(new_requests)) {
      serveQueue(ready_queue, new_requests);
      /* printf("So took one from the new ones!\n"); */
    }
    return;
  }

  /* printf("There are eligible IO requests!\n"); */
  if (isEmptyQueues(new_requests)) {
    serveQueue(ready_queue, IO_requests);
    /* printf("And there are no new requests, so force an IO one!\n"); */
    return;
  }

  /* printf("And there are eligible new requests too!\n"); */
  if (peek(new_requests).start <= peek(IO_requests).exec_time) {
    serveQueue(ready_queue, new_requests);
    /* printf("forced the first new request (arrives before the first IO request
     * " */
    /*        "finishes)!\n"); */
    return;
  }

  serveQueue(ready_queue, IO_requests);
  /* printf("forced the first IO request (finishes before the first new request
   * " */
  /*        "arrives)!\n"); */
}

void checkReadyStates(int global_t, Queues ready_queue, Queues IO_requests,
                      Queues new_requests) {
  /* printf("Will check new requests...\n"); */
  checkRequests(global_t, ready_queue, new_requests, addNewProcs);
  /* printf("Will check IO requests...\n"); */
  checkRequests(global_t, ready_queue, IO_requests, addIOProcs);
}

int IOjob(Process *p, int *io_t) {
  if (p->curr >= p->n_jobs)
    return 1;
  *io_t = MAX(p->exec_time, *io_t);
  p->exec_time = *io_t + retrieveJob(*p, p->curr++);
  *io_t = p->exec_time;
  return (p->curr >= p->n_jobs);
}

int CPUjob(Process *p, int *cpu_t, int *round_execution) {
  p->served = MAX(1, p->served);
  p->age = 0;
  int last_job = p->curr;
  *cpu_t = MAX(p->exec_time, *cpu_t);
  *round_execution = MIN(QUANTUM, retrieveJob(*p, last_job));
  p->jobs[last_job] -= *round_execution;
  p->exec_time = *cpu_t + *round_execution;
  *cpu_t = p->exec_time;
  if (!retrieveJob(*p, last_job))
    p->curr++;
  return (p->curr > last_job);
}

double RRscheduler(Queues ready_queue, Queues blocked_queue, Queues arrivals) {
  Queues final = newQueues(arrivals.store.queue->size, FIFO);
  int cpu_time, io_time, age_time;
  int proc_done, blocked_state;
  Process p;

  cpu_time = io_time = 0;
  while (!isEmptyQueues(ready_queue)) {
    p = dequeue(ready_queue);
    /* printf("Processing proc:\n"); */
    /* printProc(p); */
    blocked_state = proc_done = 0;
    if (CPUjob(&p, &cpu_time, &age_time))
      if (!(proc_done = IOjob(&p, &io_time))) {
        blocked_state = 1;
        enqueue(blocked_queue, p);
        /* printf("Proc did not finish jobs and did io, is now in blocked" */
        /*        "list!\n"); */
      }
    /* printf("CPU %d IO %d\n", cpu_time, io_time); */
    checkReadyStates(cpu_time, ready_queue, blocked_queue, arrivals);
    /* printf("UPDATE AGES OF PROCESSES CURRENTLY IN rsQUEUE...\n"); */
    if (!dynamicPriorities(ready_queue, cpu_time, age_time) && blocked_state) {
      /* printf("... but rsQUEUE is empty and last process is blocked in IO, so
       * " */
      /*        "fill the time gap!\n"); */
      fillTimeGap(ready_queue, blocked_queue, arrivals, blocked_state);
    }
    endProcessing(p, ready_queue, final, proc_done, blocked_state);
    freeProcess(p);
    /* printf("++++++++++++++++++++\n"); */
  }

  return averageTurnaround(final);
}

int main(int argc, char *argv[]) {
  Queues arrivals = newQueues(MAX_PROCESS_NUM, FIFO);
  getInput(arrivals);

  Queues ready_state = newQueues(arrivals.store.queue->size, Priority);
  initReadyQueue(ready_state, arrivals);

  Queues blocked_state = newQueues(arrivals.store.queue->size, FIFO);

  printf("%.0f\n", RRscheduler(ready_state, blocked_state, arrivals));

  freeQueues(arrivals);
  freeQueues(blocked_state);
  freeQueues(ready_state);
  return 0;
}
