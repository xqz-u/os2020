#include <stdio.h>
#include <stdlib.h>

#include "../../../lib/libDebug.h"
#include "../../../lib/libPrinters.h"

#define MAX_HEAP_SIZE 10
#define MAX_PROCESS_SIZE 15
#define QUANTUM 10
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

typedef void (*queueInspect)(int, Heap *, Queue *);
typedef struct Inspector {
  queueInspect new_compare, ready_compare;
} Inspector;

void getInput(Queue *qp) {
  Process proc;
  int start, prio;
  int in, valid = 0;

  while (scanf("%d", &start) > 0) {
    scanf("%d ", &prio);
    proc = newProcess(start, prio, MAX_PROCESS_SIZE);
    scanf("%d", &in);
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid)
      enqueue(qp, proc);
    freeProcess(proc);
    valid = 0;
  }
}

void emptyAndRefill(Queue *qp, Heap *hp) {
  Process tmp = dequeue(qp);
  insert(hp, tmp);
  freeProcess(tmp);
}

void initScheduler(Heap *scheduler, Queue *arrival_list) {
  // Safety check in case inconsistent input was given
  if (isEmptyQueue(arrival_list)) {
    throw_error(stderr, "Error! initial list is empty! exit(%d)", EXIT_FAILURE);
    exit(EXIT_FAILURE);
  }
  // Initialize the scheduler with all the processes that arrived at the same
  // time
  while (!isEmptyQueue(arrival_list) &&
         (isEmptyHeap(scheduler) ||
          peek(arrival_list).start == peekRoot(scheduler).start))
    emptyAndRefill(arrival_list, scheduler);
}

double averageTurnaround(Queue *qp) {
  double turnaround = 0;
  int tot = 0;
  Process p;
  while (!isEmptyQueue(qp)) {
    p = dequeue(qp);
    tot++;
    // Turnaround = time of process completion - start time of process
    // scheduling
    turnaround += (p.exec_time - p.start);
    freeProcess(p);
  }
  freeQueue(qp);
  // Average turnaround = total turnaround / n of processes
  return turnaround / tot;
}

void addNewProcs(int time, Heap *cpu_scheduler, Queue *requests) {
  if (peek(requests).start > time)
    return;
  while (!isEmptyQueue(requests) && peek(requests).start <= time)
    emptyAndRefill(requests, cpu_scheduler);
}

void addReadyProcs(int time, Heap *cpu_scheduler, Queue *requests) {
  if (peek(requests).exec_time > time)
    return;
  while (!isEmptyQueue(requests) && peek(requests).exec_time <= time)
    emptyAndRefill(requests, cpu_scheduler);
}

void addProcs(int time, Heap *cpu_scheduler, Queue *requests,
              queueInspect procs_adder_func) {
  if (isEmptyQueue(requests))
    return;
  procs_adder_func(time, cpu_scheduler, requests);
}

int fillTimeGap(Heap *cpu_scheduler, Queue *next_processes, int *cpu_t,
                int update_cpu) {
  if (isEmptyQueue(next_processes))
    return 0;
  emptyAndRefill(next_processes, cpu_scheduler);
  if (update_cpu)
    *cpu_t = peekRoot(cpu_scheduler).start;
  return 1;
}

void lackOfRequests(Heap *cpu_scheduler, Queue *ready_queue,
                    Queue *new_requests, int *cpu_t) {
  if (!fillTimeGap(cpu_scheduler, new_requests, cpu_t, 1))
    fillTimeGap(cpu_scheduler, ready_queue, cpu_t, 0);
}

void checkRequests(int global_t, int *cpu_t, Heap *cpu_scheduler,
                   Queue *ready_queue, Queue *new_requests,
                   Inspector adder_funcs) {
  addProcs(global_t, cpu_scheduler, new_requests, adder_funcs.new_compare);
  addProcs(*cpu_t, cpu_scheduler, ready_queue, adder_funcs.ready_compare);
  if (isEmptyHeap(cpu_scheduler))
    lackOfRequests(cpu_scheduler, ready_queue, new_requests, cpu_t);
}

int IOjob(Process *p, int *io_t) {
  if (p->curr >= p->n_jobs)
    return 1;
  *io_t = MAX(p->exec_time, *io_t);
  p->exec_time = *io_t + retrieveJob(*p, p->curr++);
  *io_t = p->exec_time;
  return (p->curr >= p->n_jobs);
}

int CPUjob(Process *p, int *cpu_t) {
  p->served = MAX(1, p->served); // ???
  p->age = 0;
  int last_job = p->curr;
  *cpu_t = MAX(p->exec_time, *cpu_t);
  int round_execution = MIN(QUANTUM, retrieveJob(*p, last_job));
  p->jobs[last_job] -= round_execution;
  p->exec_time = *cpu_t + round_execution;
  *cpu_t = p->exec_time;
  if (!retrieveJob(*p, last_job))
    p->curr++;
  return (p->curr > last_job);
}

double RRscheduler(Heap *cpu_scheduler, Queue *ready_queue, Queue *inventory) {
  Queue *final = newQueue(inventory->size);
  Inspector procs_adder = (Inspector){addNewProcs, addReadyProcs};
  int cpu_time, io_time;
  int proc_state = 0;
  Process p;

  cpu_time = io_time = 0;
  while (!isEmptyHeap(cpu_scheduler)) {
    p = deleteMax(cpu_scheduler);
    if (CPUjob(&p, &cpu_time))
      proc_state = IOjob(&p, &io_time);
    /* printf("Last process did IO? %d\n", proc_state); */
    enqueue((proc_state ? final : ready_queue), p);
    /* printf("ready queue contains:\n"); */
    /* printf(".............\n"); */
    /* printQueue(ready_queue); */
    /* printf(".............\n"); */
    freeProcess(p);
    /* printf("MAX time is %d (cpu %d io %d)\n", MAX(cpu_time, io_time),
     * cpu_time, */
    /*        io_time); */
    checkRequests(MAX(cpu_time, io_time), &cpu_time, cpu_scheduler, ready_queue,
                  inventory, procs_adder);
    /* printf("--------------\n"); */
  }

  return averageTurnaround(final);
}

int main(int argc, char *argv[]) {
  Queue *procs_inventory = newQueue(MAX_HEAP_SIZE);
  getInput(procs_inventory);

  Heap *scheduler = newHeap(procs_inventory->size);
  initScheduler(scheduler, procs_inventory);

  Queue *ready_state = newQueue(procs_inventory->size);

  printf("%.0f\n", RRscheduler(scheduler, ready_state, procs_inventory));

  freeQueue(procs_inventory);
  freeHeap(scheduler);
  freeQueue(ready_state);
  return 0;
}
