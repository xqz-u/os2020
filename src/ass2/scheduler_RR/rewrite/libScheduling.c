#include <stdlib.h>

#include "libDebug.h"
#include "libScheduling.h"

Queues newQueues(int size) {
  return (Queues){
      .tot_size = 0, newQueue(size), newQueue(size), newQueue(size)};
}

void freeQueues(Queues wrapper) {
  freeQueue(wrapper.high_prio_q);
  freeQueue(wrapper.med_prio_q);
  freeQueue(wrapper.low_prio_q);
}

void enqueue(Queues *wrapper, Process_p p) {
  switch (p->prio) {
  case 1:
    addFront(wrapper->high_prio_q, p);
    break;
  case 2:
    addFront(wrapper->med_prio_q, p);
    break;
  case 3:
    addFront(wrapper->low_prio_q, p);
    break;
  default:
    throw_error(
        stderr,
        "Inserting in Queues object: priority class %d not implemented!\n",
        p->prio);
    exit(EXIT_FAILURE);
  }
  wrapper->tot_size++;
}

int isEmptyQueues(Queues wrapper) { return !wrapper.tot_size; }

Process_p dequeue(Queues *wrapper) {
  if (isEmptyQueues(*wrapper))
    return NULL;

  Process_p ret;
  if (!isEmptyQueue(wrapper->high_prio_q))
    ret = deleteLast(wrapper->high_prio_q);
  else if (!isEmptyQueue(wrapper->med_prio_q))
    ret = deleteLast(wrapper->med_prio_q);
  else if (!isEmptyQueue(wrapper->low_prio_q))
    ret = deleteLast(wrapper->low_prio_q);

  wrapper->tot_size--;
  return ret;
}

Process_p peek(Queues wrapper) {
  if (!isEmptyQueue(wrapper.high_prio_q))
    return peekFront(wrapper.high_prio_q);
  if (!isEmptyQueue(wrapper.med_prio_q))
    return peekFront(wrapper.med_prio_q);
  if (!isEmptyQueue(wrapper.low_prio_q))
    return peekFront(wrapper.low_prio_q);

  return NULL;
}
