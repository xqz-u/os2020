#include <stdio.h>
#include <stdlib.h>

#include "libDebug.h"
#include "libProcess.h"

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    throw_error(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

Process_p newEmptyProcess() { return safeMalloc(sizeof(Process)); }

Process_p newProcess(int start, int prio, int size) {
  Process_p ret = newEmptyProcess();
  *ret = (Process){start,
                   prio,
                   size,
                   .n_jobs = 0,
                   .jobs = safeMalloc(sizeof(int) * size),
                   .curr = 0,
                   .exec_time = 0,
                   .age = 0,
                   .served = 0};
  return ret;
}

void resizeProcess(Process_p *pp) {
  (*pp)->size *= 2;
  if (((*pp)->jobs =
           realloc((*pp)->jobs, sizeof *((*pp)->jobs) * (*pp)->size)) == NULL) {
    throw_error(stderr, "Failed memory reallocation of size %d!", (*pp)->size);
    exit(EXIT_FAILURE);
  }
}

void appendJob(Process_p *pp, int new_job) {
  if ((*pp)->n_jobs >= (*pp)->size)
    resizeProcess(pp);
  (*pp)->jobs[(*pp)->n_jobs++] = new_job;
}

int retrieveJob(Process_p p, int idx) {
  if (idx > p->n_jobs)
    throw_error(stderr, "idx too high! have %d, current tot is %d", idx,
                p->n_jobs);
  return p->jobs[idx];
}

void freeProcess(Process_p p) {
  free(p->jobs);
  free(p);
}

int *copyArr(int *og, int og_len) {
  int *ret = safeMalloc(sizeof *ret * og_len);
  for (int i = 0; i < og_len; i++)
    ret[i] = og[i];
  return ret;
}

void deepCopy(Process_p og, Process_p *ret) {
  *ret = newEmptyProcess();
  **ret = (Process){og->start,
                    og->prio,
                    og->size,
                    og->n_jobs,
                    copyArr(og->jobs, og->n_jobs),
                    og->curr,
                    og->exec_time,
                    og->age,
                    og->served};
}
