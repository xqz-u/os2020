#ifndef LIBSCHEDULING_H
#define LIBSCHEDULING_H

#include "libQueue.h"

typedef struct Queues {
  int tot_size;
  Queue *high_prio_q;
  Queue *med_prio_q;
  Queue *low_prio_q;
} Queues;

typedef void (*queueInspect)(int, Queues *, Queue *);

Queues newQueues(int size);
void freeQueues(Queues container);
int isEmptyQueues(Queues wrapper);
void enqueue(Queues *wrapper, Process_p p);
Process_p dequeue(Queues *wrapper);
Process_p peek(Queues wrapper);

#endif
