#include <stdio.h>
#include <stdlib.h>

#include "libPrinters.h"
#include "libScheduling.h"

#define MAX_PROCESS_NUM 10
#define MAX_JOBS_NUM 15

static inline void serveQueue(Queues *insertion_q, Queues *deletion_q) {
  Process_p tmp = dequeue(deletion_q);
  enqueue(insertion_q, tmp);
  freeProcess(tmp);
}

int getInput(Queues *q) {
  Process_p proc;
  int start, prio;
  int in, valid, cnt = 0;

  while (scanf("%d", &start) > 0) {
    scanf("%d", &prio);
    proc = newProcess(start, prio, MAX_JOBS_NUM);
    scanf("%d", &in);
    valid = 0;
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid) {
      enqueue(q, proc);
      cnt++;
    }
    freeProcess(proc);
  }

  return cnt;
}

void dummyModifyQueue(Queues *og) {
  Queues ret = newQueues(og->tot_size);

  while (!isEmptyQueues(*og))
    serveQueue(&ret, og);

  freeQueues(*og);
  *og = ret;
}

int main(void) {
  Queues input_queue = newQueues(MAX_PROCESS_NUM);

  printf("%d\n", getInput(&input_queue));

  printQueues(input_queue);

  dummyModifyQueue(&input_queue);

  printf("input q after modification in reassignment function:\n");
  printQueues(input_queue);

  freeQueues(input_queue);
  return 0;
}
