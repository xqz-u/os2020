#ifndef LIBPROCESS_H
#define LIBPROCESS_H

typedef struct Process {
  int start, prio;
  int size, n_jobs, *jobs, curr;
  int exec_time, age, served;
} Process, *Process_p;

Process_p newEmptyProcess();
Process_p newProcess(int start, int prio, int size);
void resizeProcess(Process_p *pp);
void appendJob(Process_p *pp, int job);
int retrieveJob(Process_p p, int idx);
void freeProcess(Process_p p);
int *copyArr(int *og, int og_len);
void deepCopy(Process_p og, Process_p *ret);

#endif
