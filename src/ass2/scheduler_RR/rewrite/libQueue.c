#include <assert.h>
#include <stdlib.h>

#include "libDebug.h"
#include "libQueue.h"

Queue *newQueue(int size) {
  Queue *ret = safeMalloc(sizeof(Queue));
  *ret = (Queue){.procs = safeMalloc(sizeof(Process_p) * size),
                 .front = 0,
                 .back = 0,
                 size};
  return ret;
}

int isEmptyQueue(Queue *qp) { return (qp->back == qp->front); }

void queueEmptyError() {
  throw_error(stderr, "queue empty! exit(%d)", EXIT_FAILURE);
  exit(EXIT_FAILURE);
}

void doubleQueueSize(Queue *qp) {
  int i, oldSize = qp->size;
  qp->size *= 2;
  qp->procs = realloc(qp->procs, sizeof *(qp->procs) * qp->size);
  assert(qp->procs != NULL);
  for (i = 0; i < qp->back; i++) /* eliminate the split configuration */
    qp->procs[oldSize + i] = qp->procs[i];
  qp->back += oldSize; /* update qp->back */
}

// NOTE this method allocates new memory, make sure to free the original
// process!
void addFront(Queue *qp, Process_p p) {
  Process_p new;
  deepCopy(p, &new);
  qp->procs[qp->back] = new;
  qp->back = (qp->back + 1) % qp->size;
  if (qp->back == qp->front)
    doubleQueueSize(qp);
}

// NOTE this method returns allocated memory to be freed!
Process_p deleteLast(Queue *qp) {
  if (isEmptyQueue(qp))
    queueEmptyError();
  Process_p ret;
  deepCopy(qp->procs[qp->front], &ret);
  freeProcess(qp->procs[qp->front]);
  qp->front = (qp->front + 1) % qp->size;
  return ret;
}

void freeQueue(Queue *qp) {
  while (!isEmptyQueue(qp))
    freeProcess(deleteLast(qp));
  free(qp->procs);
  free(qp);
}

Process_p peekFront(Queue *qp) {
  if (isEmptyQueue(qp))
    queueEmptyError();
  return qp->procs[qp->front];
}
