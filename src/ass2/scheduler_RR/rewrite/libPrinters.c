#include <stdio.h>

#include "libPrinters.h"

void printProc(Process_p p) {
  printf("\tstart: %d, priority: %d, size: %d\n", p->start, p->prio, p->size);
  printf("\tcurrent job: %d, execution: %d, age: %d\n", p->curr, p->exec_time,
         p->age);
  printf("\t%d: [", p->n_jobs);
  if (p->curr < p->n_jobs) {
    printf("%d", retrieveJob(p, p->curr));
    for (int i = p->curr + 1; i < p->n_jobs; i++)
      printf(",%d", retrieveJob(p, i));
  }
  printf("]\n");
}

void printQueue(Queue *qp) {
  printf("Queue size: %d, front: %d, back: %d\n", qp->size, qp->front,
         qp->back);
  for (int i = qp->front; i != qp->back; i = (i + 1) % qp->size) {
    printProc(qp->procs[i]);
    if ((i + 1) % qp->size != qp->back)
      printf("\n");
  }
}

void printQueues(Queues container) {
  printf("Tot processes: %d\n", container.tot_size);
  if (!isEmptyQueue(container.high_prio_q))
    printQueue(container.high_prio_q);
  if (!isEmptyQueue(container.med_prio_q))
    printQueue(container.med_prio_q);
  if (!isEmptyQueue(container.low_prio_q))
    printQueue(container.low_prio_q);
}
