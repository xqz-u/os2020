#include <stdio.h>
#include <stdlib.h>

#include "libDebug.h" // Debugging utility
#include "libScheduling.h" // Main module where all the data structures used in this exercise are defined

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
// General macros used when saving the input
#define MAX_PROCESS_NUM 10
#define MAX_JOBS_NUM 15
#define QUANTUM 10 // The assignment's RR quantum

// NOTE main assumption: there is only one CPU and one IO module, so processes
// cannot use either simultaneously
// NOTE all the function called by checkRequests rely on the input processes
// being sorted by start time
// NOTE the priority queue is implemented with the structure 'Queues' which
// contains three pointers to FIFO queues, used are priority classes as defined
// in the assignment

// Simple wrapper function to optimize the dequeue and enqueue of a process
// between a priority queue (first parameter) and a priority queue
static inline void serveQueue(Queues *insertion_q, Queue *deletion_q) {
  Process_p tmp = deleteLast(deletion_q);
  enqueue(insertion_q, tmp);
  freeProcess(tmp);
}

// Free the memory allocated to save the input and exit with an error if the
// input does not comply with the assignment's requirements
int wrongInputError(Queue *init_tmp_queue) {
  freeQueue(init_tmp_queue);
  throw_error(stderr, "Error! Empty initial list, return %d", EXIT_FAILURE);
  return EXIT_FAILURE;
}

// Save the input processes in a FIFO queue and return the number of valid
// processes read
int getInput(Queue *qp) {
  Process_p proc;
  int start, prio;
  int in, valid, cnt = 0;

  while (scanf("%d", &start) > 0) {
    scanf("%d", &prio);
    proc = newProcess(start, prio, MAX_JOBS_NUM);
    scanf("%d", &in);
    valid = 0;
    while (in != -1) {
      valid = 1;
      appendJob(&proc, in);
      scanf("%d", &in);
    }
    if (valid) {
      addFront(qp, proc);
      cnt++;
    }
    freeProcess(proc);
  }

  return cnt;
}

// Compute the average turnaround after all processes are executed
double averageTurnaround(Queue *final_list) {
  double turnaround = 0;
  int tot = 0;
  Process_p p;
  while (!isEmptyQueue(final_list)) {
    p = deleteLast(final_list);
    tot++;
    // Turnaround = time of process completion - start time of process
    // scheduling
    turnaround += (p->exec_time - p->start);
    freeProcess(p);
  }
  freeQueue(final_list);
  // Average turnaround = total turnaround / n of processes
  return turnaround / tot;
}

// Initialize the scheduler (priority queue, second parameter) with all those
// processes that made a request at the same time (the first parameter is
// actually useless and is only present to comply with the signature of the
// function pointer queueInspect).
void initReadyQueue(int _, Queues *ready_queue, Queue *arrival_list) {
  while (!isEmptyQueue(arrival_list) &&
         (isEmptyQueues(*ready_queue) ||
          peekFront(arrival_list)->start == peek(*ready_queue)->start))
    serveQueue(ready_queue, arrival_list);
}

// Schedule for execution all those processes that arrived during the given time
void addNewProcs(int time, Queues *ready_queue, Queue *new_requests) {
  while (!isEmptyQueue(new_requests) && peekFront(new_requests)->start <= time)
    serveQueue(ready_queue, new_requests);
  return;
}

// Schedule for execution all those processes that finished IO before the given
// time
void addIOProcs(int time, Queues *ready_queue, Queue *IO_requests) {
  while (!isEmptyQueue(IO_requests) &&
         peekFront(IO_requests)->exec_time <= time)
    serveQueue(ready_queue, IO_requests);
}

// Update the age of a process: if it was never run it equals the time elapsed
// between process' arrival and current CPU time, otherwise it depends on the
// time the process has been waiting before being scheduled again
int increaseAge(Process_p p, int cpu_t, int round_exec_t) {
  if (!p->served)
    return p->age = cpu_t - p->start;
  return p->age += MIN(round_exec_t, (cpu_t - p->exec_time));
}

// Increase the priority of a process if its age (idle time) surpasses 10 quanta
void updatePriority(Process_p p, int cpu_t, int round_exec_t) {
  if (increaseAge(p, cpu_t, round_exec_t) > 100) {
    p->prio--;
    p->age = 0;
  }
}

void dynamicPriorities(Queues *ready_procs, int cpu_t, int round_exec_t) {
  if (isEmptyQueues(*ready_procs)) // No processes in ready state yet
    return;

  Queues new_ready_procs = newQueues(ready_procs->tot_size);
  Process_p p;
  while (!isEmptyQueues(*ready_procs)) {
    p = dequeue(ready_procs);
    // Update the priority of processes in lower priority classes
    if (p->prio > 1)
      updatePriority(p, cpu_t, round_exec_t);
    enqueue(&new_ready_procs, p);
    freeProcess(p);
  }

  freeQueues(*ready_procs);
  // Change the original pointer to the priority queue
  *ready_procs = new_ready_procs;
}

void endProcessing(Process_p p, Queues *ready_queue, Queue *final,
                   int jobs_completed, int blocked_IO, int time) {
  if (jobs_completed) { // Process has finished execution, save its final state
    addFront(final, p);
    return;
  }
  if (!blocked_IO) // If a process is not busy doing IO and has not finished
                   // execution, schedule it as ready
    enqueue(ready_queue, p);
}

// Call the given procs_adder_func when the deletion queue still contains
// processes
void checkRequests(int time, Queues *insertion_q, Queue *deletion_q,
                   queueInspect procs_adder_func) {
  if (isEmptyQueue(deletion_q))
    return;
  procs_adder_func(time, insertion_q, deletion_q);
}

// This function is called when all processes are doing long IO operations hence
// no new process can be scheduled for CPU at the current time. When this is the
// case, force the insertion of either a new request or of the first process
// finishing IO in this order of priority. This function may be called many
// times in a row, if processes are busy with IO operations for long
void fillTimeGap(Queues *ready_queue, Queue *IO_requests, Queue *new_requests) {
  if (isEmptyQueue(IO_requests)) {
    if (!isEmptyQueue(new_requests))
      serveQueue(ready_queue, new_requests);
    return;
  }

  if (isEmptyQueue(new_requests)) {
    serveQueue(ready_queue, IO_requests);
    return;
  }

  if (peekFront(new_requests)->start <= peekFront(IO_requests)->exec_time) {
    serveQueue(ready_queue, new_requests);
    return;
  }

  serveQueue(ready_queue, IO_requests);
}

void checkReadyStates(int time, Queues *ready_queue, Queue *IO_requests,
                      Queue *new_requests) {
  // Add new processes to the scheduler (those that arrived before the given
  // 'time' - CPU time)
  checkRequests(time, ready_queue, new_requests, addNewProcs);
  // Same as above, but for processes that finished IO in the meantime
  checkRequests(time, ready_queue, IO_requests, addIOProcs);
}

// Process a IO job if present, update process' execution time and IO module's
// time
int IOjob(Process_p p, int *io_t) {
  if (p->curr >= p->n_jobs)
    return 1;
  *io_t = MAX(p->exec_time, *io_t);
  p->exec_time = *io_t + retrieveJob(p, p->curr++);
  *io_t = p->exec_time;
  return (p->curr >= p->n_jobs);
}

// Process a CPU job if present in RR style, update process' execution time and
// CPU module's time
int CPUjob(Process_p p, int *cpu_t, int *round_execution) {
  // Signal that the process used CPU for the first time
  p->served = MAX(1, p->served);
  p->age = 0;             // Reset process' age
  int last_job = p->curr; // Used to check whether a process finished CPU job
                          // and can now do IO
  *cpu_t = MAX(p->exec_time, *cpu_t);
  // Update a process' execution time to the amount of quantum needed
  *round_execution = MIN(QUANTUM, retrieveJob(p, last_job));
  p->jobs[last_job] -= *round_execution;
  p->exec_time = *cpu_t + *round_execution;
  *cpu_t = p->exec_time;
  if (!retrieveJob(p, last_job)) // If process finished current CPU job...
    p->curr++;
  // ...make the  schedule aware of it and perform IO
  return (p->curr > last_job);
}

double RRscheduler(Queues *ready_queue, Queue *blocked_queue, Queue *arrivals) {
  Queue *final = newQueue(arrivals->size); // Save processes final state
  // Two variables used to keep track of a module's (CPU/IO) time + another one
  // to save the amount of quantum used in a RR round signal the end of a
  // process' execution
  int cpu_time, io_time, age_time;
  // Flags to signal process' end of execution/performing IO
  int proc_done, blocked_state;
  Process_p p;

  cpu_time = io_time = 0;
  while (!isEmptyQueues(*ready_queue)) {
    p = dequeue(ready_queue); // Scheduler picks right priority class process
    blocked_state = proc_done = 0;
    if (CPUjob(p, &cpu_time, &age_time))
      // Perform IO if process finished CPU job during its time quantum
      if (!(proc_done = IOjob(p, &io_time))) {
        // Perform IO in a FCFS-like fashion if process did not finish execution
        blocked_state = 1;
        addFront(blocked_queue, p);
      }
    // Update ready processes' age with the amount of time quantum used by the
    // last run process (saved in 'age_time')
    dynamicPriorities(ready_queue, cpu_time, age_time);
    // Schedule new requests (new at all or processes finishing from IO)
    checkReadyStates(cpu_time, ready_queue, blocked_queue, arrivals);
    // When all processes are long gone doing IO and no new requests are still
    // present, force the scheduling of the first process that would make a
    // request
    if (isEmptyQueues(*ready_queue) && blocked_state)
      fillTimeGap(ready_queue, blocked_queue, arrivals);
    // Enqueue the process run in this RR round in the either the ready state
    // queue or the final one if it is not busy doing IO
    endProcessing(p, ready_queue, final, proc_done, blocked_state, cpu_time);
    freeProcess(p);
  }

  return averageTurnaround(final); // Compute the average turnaround time
}

int main(int argc, char *argv[]) {
  // FIFO queue used to save the input
  Queue *arrivals = newQueue(MAX_PROCESS_NUM);

  if (!getInput(arrivals)) // Exit if illegal input is given
    wrongInputError(arrivals);

  // Initialize the scheduler (priority queue) with the function initReadyQueue
  // and the input queue (the first parameter is just a placeholder for the
  // function pointer signature)
  Queues ready_state = newQueues(arrivals->size);
  checkRequests(-1, &ready_state, arrivals, initReadyQueue);

  // FIFO queue used to schedule IO execution
  Queue *blocked_state = newQueue(arrivals->size);

  printf("%.0f\n", RRscheduler(&ready_state, blocked_state, arrivals));

  // Free the memory used
  freeQueue(arrivals);
  freeQueue(blocked_state);
  freeQueues(ready_state);
  return 0;
}
