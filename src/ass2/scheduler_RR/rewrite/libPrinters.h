#ifndef LIBPRINTERS_H
#define LIBPRINTERS_H

#include "libScheduling.h"

void printProc(Process_p p);
void printQueue(Queue *qp);
void printQueues(Queues container);

#endif
