#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_PROCESSES 3
#define MAX_BUFFERS 2

// Semaphore names for mutual exclusion to shared memory
#define SEM_FILL_B0 "fill0"
#define SEM_EMPTY_B0 "empty0"
#define SEM_FILL_B1 "fill1"
#define SEM_EMPTY_B1 "empty1"

// Semaphore names for shared memory objects
#define BUFFER0 "b0"
#define BUFFER1 "b1"
#define B0 0
#define B1 1

#define SENTINEL -1

#define SEM_PERMS (S_IRUSR | S_IWUSR) // READ and WRITE permissions, owner

typedef struct SHM_Object{
    int shmfd;
    int* buffer;
}SHM_Object;

typedef struct BufferSems{
    sem_t* empty;
    sem_t* full;
}BufferSems;

void *safeMalloc(int sz)
{
    void *p = calloc(sz, 1);
    if (p == NULL) {
        fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
        exit(EXIT_FAILURE);
    }
    return p;
}

// Initializes processes
int init_processes()
{
    pid_t pid;
    int i;
    for (i = 1; i < MAX_PROCESSES; i++) {
        pid = fork();
        if (pid == -1) {
            perror("Forking!");
            exit(EXIT_FAILURE);
        }
        if (!pid) {
            return i;
        }
    }
    return 0;
}

// This function just waits for children to die
void waitChildren(int tot)
{
    int i, status;
    for (i = 0; i < tot; i++) {
        if (wait(&status) == -1) {
            perror("wait failed");
            exit(EXIT_FAILURE);
        }
    }
}

// Un-links shared memory objects semaphores
void unlink_shm()
{
    shm_unlink(BUFFER0);
    shm_unlink(BUFFER1);
}

// Un-links buffer semaphores
void unlink_semaphores()
{
    sem_unlink(SEM_EMPTY_B0);
    sem_unlink(SEM_FILL_B0);
    sem_unlink(SEM_EMPTY_B1);
    sem_unlink(SEM_FILL_B1);
}

// Throws error and un-links the previously opened semaphores
void throw_error_shm(char* error)
{
    perror(error);
    unlink_shm();
    exit(EXIT_FAILURE);
}

// Opens the semaphores to control access to buffer
BufferSems open_semaphores(char* empty, char* fill)
{
    BufferSems bs;
    bs.empty = sem_open(empty, O_CREAT, SEM_PERMS, 1); // Semaphore to signal when buffer is empty, initial value=1 (open)
    if (bs.empty == SEM_FAILED) {
        perror("sem_open() write failed");
        exit(EXIT_FAILURE);
    }
    bs.full = sem_open(fill, O_CREAT, SEM_PERMS, 0); // Semaphore to signal when buffer is full, initial value=0 (close)
    if (bs.full == SEM_FAILED) {
        perror("sem_open() write failed");
        exit(EXIT_FAILURE);
    }
    return bs;
}

// Closes opened semaphores
void close_semaphores(BufferSems* buffer_sems)
{
    int i = 0;
    for (i = 0; i < MAX_BUFFERS; i++) {
        sem_close(buffer_sems[i].empty);
        sem_close(buffer_sems[i].full);
    }
    unlink_semaphores();
}

// Initializes the shared memory semaphore and corresponding buffer
SHM_Object init_shm_object(char* buffer_name)
{
    SHM_Object shm_obj;
    int SHM_SIZE = sysconf(_SC_PAGE_SIZE); // Size of a page in bytes

    if ((shm_obj.shmfd = shm_open(buffer_name, O_CREAT | O_RDWR | O_TRUNC, SEM_PERMS)) == -1) {
        throw_error_shm("shm_open() failure");
    }
    if (ftruncate(shm_obj.shmfd, SHM_SIZE) == -1) { // Used to truncate memory to given size
        throw_error_shm("ftruncate failure");
    }
    if ((shm_obj.buffer = mmap(0, SHM_SIZE, PROT_WRITE | PROT_READ, MAP_SHARED, shm_obj.shmfd, 0)) == (int*) -1) {
        throw_error_shm("mmap failure");
    }
    return shm_obj;
}

// Closes semaphores and removes corresponding shared memory objects
void remove_shm_objects(SHM_Object *shm_object)
{
    int SHM_SIZE = sysconf(_SC_PAGE_SIZE); // Size of a page in bytes
    munmap(BUFFER0, SHM_SIZE); // Removes buffers from memory
    munmap(BUFFER1, SHM_SIZE);

    close(shm_object[B0].shmfd); // closes the semaphores
    close(shm_object[B1].shmfd);

    unlink_shm(); // unlinks the semaphores
}

/**
 * 1. Wait for buffer to be empty.
 * 2. Write value into the buffer.
 * 3. Signal that buffer is now filled.
 * @param sem
 * @param buffer (shared memory object)
 * @param value (value to be written into buffer)
 **/
void write_buffer(BufferSems sem, int* buffer, int value)
{
    sem_wait(sem.empty);
    buffer[0] = value;
    sem_post(sem.full);
}

/**
 * 1. Wait for buffer to be filled.
 * 2. Read value from buffer.
 * 3. Signal that buffer is now empty.
 * @param sem
 * @param buffer (shared memory object)
 * @return value (read from buffer)
 */
int read_buffer(BufferSems sem, const int* buffer)
{
    sem_wait(sem.full);
    int value = buffer[0];
    sem_post(sem.empty);

    return value;
}

/**
 * Each process has a different job
 * P0: Write value from 1 to n into buffer 0
 * P1: Read value from B0, Write value into B1 if (value%2 != 0)
 * P2: Read value from B1, Write to stdout if (value%3 != 0)
 * --------------------------------------------------------------
 * @param proc (each process executes a different job)
 * @param n (maximum value to write into buffer 0)
 * @param shm_object (shared memory objects)
 * @param sem_b0 (semaphores for buffer 0)
 * @param sem_b1 (semaphores for buffer 1)
 */
void execute(int proc_number, int n, SHM_Object* shm_object, BufferSems* buffer_sems)
{
    int value = 0;
    switch (proc_number) {
        case 0: // PROCESS 0
            while (1) {
                if (value <= n) {
                    write_buffer(buffer_sems[B0], shm_object[B0].buffer, value);
                    value++;
                } else {
                    write_buffer(buffer_sems[B0], shm_object[B0].buffer, SENTINEL);
                    return;
                }
            }
        case 1: // PROCESS 1
            while (1) {
                value = read_buffer(buffer_sems[B0], shm_object[B0].buffer);
                if(value % 2 != 0 || value == SENTINEL){
                    write_buffer(buffer_sems[B1], shm_object[B1].buffer, value);
                }
                if (value == SENTINEL) {
                    return;
                }
            }
        case 2: // PROCESS 2
            while (1) {
                value = read_buffer(buffer_sems[B1], shm_object[B1].buffer);
                if(value % 3 != 0 && value != SENTINEL){
                    printf("%d\n", value);
                }
                if (value == SENTINEL) {
                    return;
                }
            }
    }
}

int main()
{
    int n;
    scanf("%d", &n);
    setbuf(stdout, NULL);

    SHM_Object *shm_object = safeMalloc(MAX_BUFFERS * sizeof(SHM_Object)); // Shared memory object for B0 and B1
    shm_object[B0] = init_shm_object(BUFFER0);
    shm_object[B1] = init_shm_object(BUFFER1);

    BufferSems *buffer_sems = safeMalloc(MAX_BUFFERS * sizeof(BufferSems)); // Semaphores empty/fill for both B0 and B1
    buffer_sems[B0] = open_semaphores(SEM_EMPTY_B0, SEM_FILL_B0);
    buffer_sems[B1] = open_semaphores(SEM_EMPTY_B1, SEM_FILL_B1);

    int proc_number = init_processes(); // Initialize processes with their own process number
    execute(proc_number, n, shm_object, buffer_sems); // Execute work for each individual process

    if(proc_number == 0)
        waitChildren(MAX_PROCESSES-1);

    close_semaphores(buffer_sems);
    unlink_shm();
    free(buffer_sems);

    remove_shm_objects(shm_object);
    free(shm_object);

    return 0;
}
