#include <stdio.h>
#include <stdlib.h>

#include "libProgram.h"
#include "libUtils.h"

inline program_list newProgram(void) {
    program_list ret = safeMalloc(sizeof *ret);
    *ret = (program){.next = NULL,
                     .pid = -1,
                     .exit_status = -1,
                     .prog_args = newStrings(DEFAULT_PROG_ARGS)};
    return ret;
}

inline void freeProgram(program prog) { freeStrings(prog.prog_args); }

void freeProgramList(program_list progs) {
    if (!progs)
        return;
    freeProgramList(progs->next);
    freeProgram(*progs);
    free(progs);
}

void printProgram(program prog) {
    printf("\tpid: %ld, exit_status: %d\n", (long)prog.pid, prog.exit_status);
    printStrings(prog.prog_args);
}

void printProgramList(program_list progs) {
    if (!progs)
        return;
    printProgram(*progs);
    printProgramList(progs->next);
}

inline void resetProgramList(program_list *head) {
    freeProgramList(*head);
    if (*head)
        *head = NULL;
}

program_list parseProgram(strings *token_list) {
    char *tok = retrieveString(*token_list, token_list->cursor);
    if (!tok || isProtected(*tok))
        return NULL;

    program_list prog = newProgram();

    while (tok && !isProtected(*tok)) {
        appendString(&prog->prog_args, tok);
        tok = retrieveString(*token_list, ++token_list->cursor);
    }

    appendString(&prog->prog_args, NULL);
    return prog;
}

program_list parseProgramList(strings *token_list, program_list *head) {
    if (!(*head = parseProgram(token_list))) {
        printf("Error: invalid syntax!\n");
        return *head;
    }

    char *prog_delim = retrieveString(*token_list, token_list->cursor);

    if (prog_delim && isPipe(*prog_delim)) {
        token_list->cursor++;
        (*head)->next = parseProgramList(token_list, &(*head)->next);
        if (!(*head)->next)
            resetProgramList(head);
    }

    return *head;
}

inline program_list findProgramPid(program_list programs_head, pid_t seek_pid) {
    program_list p = NULL;
    for (p = programs_head; p; p = p->next)
        if (p->pid == seek_pid)
            break;
    return p;
}
