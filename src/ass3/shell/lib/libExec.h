#ifndef LIBEXEC_H
#define LIBEXEC_H

#include <sys/types.h>

#include "libCommands.h"
#include "libPipe.h"
#include "libProgram.h"

/* int isPipelineDone(program_list progs, int ret); */
/* void waitFgChildren(program_list programs); */
extern void inspectChildState(pid_t pid, int status);
extern void waitChild(program_list child_prog);
extern int openFile(char *filename, int flags, mode_t mod);
extern void initChannel(Pipe channel, program_list curr_prog, char *outfile,
                        int *out_fd);
extern void run(char *filename, char **args);
void runJobs(command_list commands);
void runPipeline(program_list programs, char *infile, char *outfile,
                 int is_background);

#endif
