#ifndef LIBPIPE_H
#define LIBPIPE_H

#define READ 0
#define WRITE 1
#define STDIN STDIN_FILENO
#define STDOUT STDOUT_FILENO

typedef int *Pipe;

extern Pipe newEmptyPipe(void);
extern void freePipe(Pipe channel);
extern void closeStream(int fd);
extern void cleanupStream(int input, int output);
extern void dupStream(int old_fd, int new_fd);
extern void redirectStreams(int input, int output);
extern void initPipe(Pipe channel);
extern void printPipe(Pipe channel);

#endif
