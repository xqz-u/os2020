#include <stdio.h>
#include <stdlib.h>

#include "libDebug.h"
#include "libUtils.h"

inline void *safeMalloc(int sz) {
    void *p = calloc(sz, 1);
    if (p == NULL) {
        fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
        exit(EXIT_FAILURE);
    }
    return p;
}

inline void resize(void **store, int *nmemb, size_t size) {
    *nmemb *= 2;
    if (!(*store = realloc(*store, size * *nmemb)))
        throw_error(stdout, "Failed reallocation of size %d!", *nmemb);
}

inline void checkResize(void **store, int *nmemb, size_t size, int n_filled) {
    if (n_filled < *nmemb)
        return;
    resize(store, nmemb, size);
}

inline void printCharArr(char **args, int args_sz) {
    printf("\t%d strings: [", args_sz);
    if (args_sz) {
        printf("\"%s\"", args[0]);
        for (int i = 1; i < args_sz; i++)
            printf(",\"%s\"", args[i]);
    }
    printf("]\n");
}

inline void freeCharArr(char **args, int args_sz) {
    for (int i = 0; i < args_sz; i++)
        free(args[i]);
    free(args);
}
