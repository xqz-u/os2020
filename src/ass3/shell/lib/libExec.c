#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
/* #include <signal.h> */

#include "libDebug.h"
#include "libExec.h"
#include "libStrings.h"
#include "libUtils.h"

#define DEBUG 0

/* extern volatile sig_atomic_t n_bg_process; */

/* int isPipelineDone(program_list progs, int ret) { */
/*     if (!(progs && ret)) */
/*         return ret; */
/*     return isPipelineDone(progs->next, progs->pid != -1); */
/* } */

/* void waitFgChildren(program_list programs) { */
/*     if (isPipelineDone(programs, -1)) { */
/*         printf("done waits FG! return\n"); */
/*         return; */
/*     } */

/*     int status; */
/*     pid_t cpid; */
/*     if ((cpid = waitpid(-1, &status, 0)) == -1) */
/*         exitError("waitChild() -> waitpid()"); */
/* #if DEBUG */
/*     inspectChildState(cpid, status); */
/* #endif */
/*     program_list done_prog = findProgramPid(programs, cpid); */
/*     if (done_prog) */
/*         done_prog->exit_status = status; */
/*     else { */
/*         n_bg_process--; */
/*         printf("bg rpc exited in fg waits! %d\n", n_bg_process); */
/*     } */
/*     waitFgChildren(programs); */
/* } */

inline void inspectChildState(pid_t pid, int status) {
    printf("Process %ld: exit(%d) -> %s\n", (long)pid, WEXITSTATUS(status),
           strerror(WEXITSTATUS(status)));
    if (WIFSTOPPED(status))
        debugErr("Warning! child %d was stopped by signal %d -> %s", (long)pid,
                 WSTOPSIG(status), strerror(WSTOPSIG(status)));
    if (WIFSIGNALED(status))
        debugErr("Warning! child %ld was terminated by signal %d -> %s\n",
                 (long)pid, WTERMSIG(status), strerror(WTERMSIG(status)));
}

inline void waitChild(program_list child_prog) {
#if DEBUG
    debugErr("waiting on proc %ld", (long)child_prog->pid);
#endif
    if (waitpid(child_prog->pid, &child_prog->exit_status, 0) == -1)
        exitError("waitChild() -> waitpid()");
#if DEBUG
    inspectChildState(child_prog->pid, child_prog->exit_status);
#endif
}

inline int openFile(char *filename, int flags, mode_t mod) {
    int new_fd = mod ? open(filename, flags, mod) : open(filename, flags);
    if (new_fd == -1)
        exitError("redirectOutput() -> open()");
    return new_fd;
}

inline void initChannel(Pipe channel, program_list curr_prog, char *outfile,
                        int *out_fd) {
    if (!curr_prog->next) {
        *out_fd = outfile
                      ? openFile(outfile, O_CREAT | O_WRONLY | O_TRUNC, 0666)
                      : STDOUT;
        return;
    }
    initPipe(channel);
    *out_fd = channel[WRITE];
}

inline void run(char *filename, char **args) {
#if DEBUG
    debugErr("Process %ld executes prog '%s'", (long)getpid(), filename);
#endif
    char *unquot_args[] = {filename, NULL, NULL};
    char *quoted_arg = args[1];
    if (quoted_arg && isQuotes(*quoted_arg) &&
        isQuotes(quoted_arg[strlen(quoted_arg) - 1]))
        unquot_args[1] = extractSubstr(quoted_arg, 1, strlen(quoted_arg) - 1);
    /* debugErr("found param enclosed in quotes! it was <%s>, now it is ->
     * <%s>\n", */
    /*          quoted_arg, *unquot_args); */
    /* printCharArr(unquot_args, 3); */
    int ret = execvp(filename, unquot_args[1] ? unquot_args : args);
    if (ret == -1 && errno == ENOENT)
        fprintf(stdout, "Error: command not found!\n");
}

void runPipeline(program_list programs, char *infile, char *outfile,
                 int is_background) {
    Pipe channel = newEmptyPipe();
    int in_fd, out_fd;
    pid_t pid_ref;
    program_list prog;

    in_fd = infile ? openFile(infile, O_RDONLY, 0) : STDIN;

    for (prog = programs; prog; prog = prog->next) {
        initChannel(channel, prog, outfile, &out_fd);
        switch (pid_ref = fork()) {
        case -1:
            exitError("runPipeline() -> fork()");
        case 0:
            redirectStreams(in_fd, out_fd);
            run(prog->prog_args.args[0], prog->prog_args.args);
            exit(errno);
        default:
            cleanupStream(in_fd, out_fd);
            in_fd = channel[READ];
            prog->pid = pid_ref;
            if (!is_background)
                waitChild(prog);
        }
    }
    freePipe(channel);

    /* if (!is_background) */
    /*     waitFgChildren(programs); */
}

void runJobs(command_list commands) {
    if (!commands)
        return;
    runPipeline(commands->programs_head, commands->infile, commands->outfile,
                commands->background);
    runJobs(commands->next);
}
