#ifndef LIBPROGRAM_H
#define LIBPROGRAM_H

#include <sys/types.h>

#include "libStrings.h"

typedef struct program *program_list;

typedef struct program {
    program_list next;
    pid_t pid;
    int exit_status;
    strings prog_args;
} program;

extern program_list newProgram(void);
extern void freeProgram(program prog);
void freeProgramList(program_list progs);
void printProgram(program prog);
void printProgramList(program_list progs);
extern void resetProgramList(program_list *head);
program_list parseProgram(strings *token_list);
program_list parseProgramList(strings *token_list, program_list *head);
extern program_list findProgramPid(program_list programs_head, pid_t pid);

#endif
