#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "lib/libExec.h"
#include "lib/libUtils.h"

#define DEBUG 0

/* switch (info_struct->si_code) { */
/* case CLD_EXITED: */
/*     sig_code = "child exited!"; */
/*     break; */
/* case CLD_TRAPPED: */
/*     sig_code = "child trapped!"; */
/*     break; */
/* case CLD_STOPPED: */
/*     sig_code = "child stopped!"; */
/*     break; */
/* case CLD_CONTINUED: */
/*     sig_code = "child continued!"; */
/*     break; */
/* default: */
/*     sig_code = "not inspected reason..."; */
/* } */

volatile sig_atomic_t n_bg_process;

static inline void refreshInput(command_list *cmds, strings tokens,
                                char *input_line) {
    resetCommandList(cmds);
    freeStrings(tokens);
    free(input_line);
}

void bgChildHandler(int sig, siginfo_t *info_struct, void *ucontext) {
#if DEBUG
    printf("Caught SIGCHILD of %ld with code %d -> %s\n",
           (long)info_struct->si_pid, info_struct->si_code,
           info_struct->si_code == CLD_EXITED ? "child exited!"
                                              : "unknonwn...");
#endif
    int status;
    pid_t exit_pid;
    while ((exit_pid = waitpid(-1, &status, WNOHANG)) > 0) {
        n_bg_process--;
#if DEBUG
        printf("handler::exit pid %ld, status %d -> %s\n", (long)exit_pid,
               WEXITSTATUS(status), strerror(WEXITSTATUS(status)));
#endif
    }
}

static inline int checkSafeExit(command_list *cmds, strings token_list,
                                char *raw_input) {
    if (strcmp(raw_input, "exit"))
        return -1;

    if (n_bg_process)
        printf("Error: there are still background processes running!\n");

    refreshInput(cmds, token_list, raw_input);
    return !n_bg_process;
}

int main(int argc, char *argv[]) {
    char *raw_input;
    strings token_list;
    command_list cmd_head = NULL;
    int exit_status;

    setvbuf(stdout, NULL, _IONBF, 0);

    struct sigaction bg_programs_act = {.sa_flags = SA_SIGINFO | SA_RESTART,
                                        .sa_sigaction = bgChildHandler,
                                        .sa_mask = {{0}}};

    if (sigaction(SIGCHLD, &bg_programs_act, NULL) == -1) {
        printf("Error in signal handler! %d -> %s\n", errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    n_bg_process = 0;

    while ((raw_input = readline())) {
        token_list = tokenize(raw_input);
        printStrings(token_list);
        cmd_head = parseCommandList(&token_list, &cmd_head);
        /* printCommandList(cmd_head); */
        n_bg_process += countBgPrograms(cmd_head);
        /* printf("there are %d tot bg process!\n", n_bg_process); */
        exit_status = checkSafeExit(&cmd_head, token_list, raw_input);
        if (!exit_status)
            continue;
        else if (exit_status == 1)
            break;
        /* runJobs(cmd_head); */
        refreshInput(&cmd_head, token_list, raw_input);
        /* printf("-----------------------------\n"); */
    }

    return 0;
}
