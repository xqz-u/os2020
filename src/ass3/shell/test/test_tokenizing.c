#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

static inline void sstrcp(char *dest, char *src, char *end) {
    /* char *t = "t"; */
    /* printf("%p %c\n", src, *src); */
    /* printf("%p %c\n", t, *t); */
    /* *src += 1; // == (*src)++ */
    /* printf("%c\n", *src); */
    /* *src++ = *t++; */
    /* printf("%p %c\n", src, *src); */
    /* printf("%p %c\n", t, *t); */
    /* printf("------\n"); */
    char *start_ref = src;

    while (src < end) {
        printf("before %p %p\n", dest, src);
        *dest++ = *src++;
        printf("src %p %c\n", src, *src);
        printf("dest %p %c\n", dest, *dest);
    }
    *dest = 0;
    src = start_ref;
}

int main(void) {
    char *example = malloc(4 + 1);
    *example = 'c';
    example[1] = 'i';
    example[2] = 'a';
    example[3] = 'o';
    example[4] = 0;

    printf("%s\n", example);

    char *cp = calloc(strlen(example) + 1, sizeof *cp);
    printf("og cp %p\n", cp);
    sstrcp(cp, example, cp + strlen(example));
    printf("copied <%s>\n", cp);

    free(cp);
    free(example);
    return 0;
}
