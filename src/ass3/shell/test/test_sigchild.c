#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/signalfd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

typedef struct program {
    char **args;
    int bg;
} program;

int TOT_BG = 0;

void childHandler(int sig) {
    int status, flag = 0;
    pid_t exit_pid;
    while ((exit_pid = waitpid(-1, &status, WNOHANG)) > 0) {
        flag = 1;
        TOT_BG--;
        printf("exit pid %ld, sig sent is %s, status %d -> %s. Me I'm %ld, tot "
               "bg is %d!\n",
               (long)exit_pid, (sig == SIGCHLD ? "SIGCHLD" : "NOT SIGCHLD"),
               WEXITSTATUS(status), strerror(WEXITSTATUS(status)),
               (long)getpid(), TOT_BG);
    }
    printf("pid %ld %d\n", (long)getpid(), flag);
}

void foregroundWait(pid_t child, char *prog_filename) {
    int status;
    pid_t exit_pid = waitpid(child, &status, 0);
    if (exit_pid == -1)
        perror("foreground waitpid() ->");
    printf(
        "exit pid %d when child is %d, status %d -> %s. executed prog \"%s\"\n",
        exit_pid, child, WEXITSTATUS(status), strerror(WEXITSTATUS(status)),
        prog_filename);
}

void execute(int n_prog, program progs[n_prog]) {
    program curr;
    pid_t child_pid;

    for (int i = 0; i < n_prog; i++) {
        curr = progs[i];
        switch (child_pid = fork()) {
        case -1:
            perror("fork");
            exit(-1);
        case 0:
            printf("Child %ld executes \"%s\" (bg ? %d)\n", (long)getpid(),
                   curr.args[0], curr.bg);
            execvp(curr.args[0], curr.args);
            printf("error %d -> %s\n", errno, strerror(errno));
        default:
            printf("Parent %ld\n", (long)getpid());
            if (!curr.bg)
                foregroundWait(child_pid, curr.args[0]);
            else
                printf("found bg proc, tot is %d\n", ++TOT_BG);
        }
    }
}

static inline int safeExit(char *input) {
    if (!TOT_BG) {
        free(input);
        return 1;
    }
    printf("Error: background proc is still executing!\n");
    return 0;
}

static inline void resetInput(char **str, size_t *str_sz) {
    free(*str);
    if (*str)
        *str = NULL;
    *str_sz = 0;
}

static inline int readInput(char **line, size_t *line_size) {
    size_t bytes_read = getline(line, line_size, stdin);
    printf("read %zu bytes, line -> %s", bytes_read, *line);
    if (!strcmp(*line, "exit\n"))
        return safeExit(*line);
    return 0;
}

int __main(int argc, char *argv[]) {
    setbuf(stdout, NULL);

    char *prog_a[] = {"sleep", "5", NULL};
    char *prog_b[] = {"echo", "command echo!", NULL};

    program progs[] = {(program){prog_a, 1}, (program){prog_b, 0}};

    struct sigaction act = {NULL};
    act.sa_handler = childHandler;
    sigaction(SIGCHLD, &act, NULL);

    char *line = NULL;
    size_t line_size = 0;
    int guard = 0;

    do {
        guard = readInput(&line, &line_size);
        if (guard)
            break;
        execute(sizeof progs / sizeof *progs, progs);
        resetInput(&line, &line_size);
        printf("-------------------\n");
    } while (!guard);

    return 0;
}

/* ------------------------------------------------------------------- */

/* Problem: when multiple signals of the same istance are delievered at the same
 * time, they are conflated in only one signal! Cannot check the pids one by one
 */

#define READ 0
#define WRITE 1

int self_pipe[2];

volatile sig_atomic_t term_pid;

void launchChildren(void) {
    pid_t pid;

    for (int i = 0; i < 3; i++) {
        switch (pid = fork()) {
        case -1:
            perror("forking");
            exit(1);
        case 0:
            printf("In child! pid is %ld at iteration %d, will exit now\n",
                   (long)getpid(), i);
            exit(0);
        default:
            printf("This is father %ld, iteration %d!\n", (long)getpid(), i);
        }
    }
    printf("Father %ld outside forking loop!\n", (long)getpid());
}

void childHandler2(int signo, siginfo_t *info_struct, void *ucontext) {
    term_pid = info_struct->si_pid;
    /* printf("child %ld exited!\n", (long)info_struct->si_pid); */

    /* int status; */
    /* pid_t exit_pid; */
    /* while ((exit_pid = waitpid(-1, &status, WNOHANG)) > 0) */
    /*     printf("%ld exited(%d)\n", (long)exit_pid, WEXITSTATUS(status)); */
}

int ___main(void) {
    setbuf(stdout, NULL);

    struct sigaction sa_child; // init sigaction specs
    sa_child.sa_flags = SA_SIGINFO;
    sa_child.sa_sigaction = childHandler2;
    sigemptyset(&sa_child.sa_mask);
    sigaction(SIGCHLD, &sa_child, NULL); // install handler on parent proc

    /* struct pollfd self_channel[1]; */

    launchChildren();

    getchar();

    return 0;
}

/* ------------------------------------------------------------------------ */

void printSigInfo(struct signalfd_siginfo sig_spec) {
    printf("signal number %d -> %s\n", sig_spec.ssi_signo,
           strerror(sig_spec.ssi_signo));
    printf("sender pid: %ld, status: %d -> %s\n", (long)sig_spec.ssi_pid,
           WEXITSTATUS(sig_spec.ssi_status),
           strerror(WEXITSTATUS(sig_spec.ssi_status)));
}

void setReader(int sig_fd) {
    struct signalfd_siginfo sig_spec;
    int status;
    pid_t child;

    while (read(sig_fd, &sig_spec, sizeof sig_spec) > 0) {
        printf("Read from the fd!\n");
        printSigInfo(sig_spec);
        if ((child = waitpid(-1, &status, WNOHANG)) == -1)
            perror("waiting!");
        printf("Just reaped child %ld with status %d -> %s\n", (long)child,
               WEXITSTATUS(status), strerror(WEXITSTATUS(status)));
    }
    printf("No more messages!? errno %d -> %s == EAGAIN? %d\n", errno,
           strerror(errno), errno == EAGAIN);
}

int ____main(void) {
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);

    int sig_fd = signalfd(-1, &mask, SFD_NONBLOCK | SFD_CLOEXEC);
    if (sig_fd == -1) {
        perror("creating signal fd!");
        exit(1);
    }

    /* Need to find a way such that the caller (parent proc) is notified of an
     * event on the fd (exit of a child) and can thus wait on it. Maybe use
     * select/poll? */
    setReader(sig_fd);
    launchChildren();
    printf("close the signal fd...\n");
    if (close(sig_fd) == -1) {
        perror("closing signal fd ->");
        exit(1);
    }

    /* getchar(); */

    return 0;
}

/* -------------------------------------------------------------------------- */

void testChild(void) {
    switch (fork()) {
    case -1:
        perror("forking");
        exit(1);
    case 0:
        printf("child %ld created! sleep 3 secs and terminate...\n",
               (long)getpid());
        sleep(3);
        exit(0);
    default:
        printf("Parent %ld!\n", (long)getpid());
    }
}

void signalEventLoop(struct pollfd signal_poller[]) {
    struct signalfd_siginfo signalinfo;
    int ret;

    while (1) { // infinite loop!
        if ((ret = poll(signal_poller, 1, 500)) == -1) {
            perror("poll()");
            exit(errno);
        }
        if (!ret || ret == EAGAIN) {
            printf("%s\n",
                   ret ? "nothing was ready but could be at next poll (EAGAIN!)"
                       : "timeout occurred before any event happened!");
            continue;
        }
        printf("%d fds were selected!\n", ret);
        if (signal_poller[0].revents & POLLIN) {
            printf("it should be possible to read on the signal fd! let's "
                   "try...\n");
            if (read(signal_poller[0].fd, &signalinfo, sizeof signalinfo) ==
                -1) {
                perror("read failed!");
                exit(errno);
            }
            printSigInfo(signalinfo);
        }
    }
}

int openSignalFD(sigset_t *sig_mask) {
    int sig_fd = signalfd(-1, sig_mask, SFD_NONBLOCK | SFD_CLOEXEC);
    if (sig_fd == -1) {
        perror("creating signal fd!");
        exit(errno);
    }
    return sig_fd;
}

void initSignalMask(sigset_t *sig_mask, int signo) {
    sigemptyset(sig_mask);
    sigaddset(sig_mask, signo);
}

void setSinglePollFD(struct pollfd sig_pollers[], int signalfd,
                     short events_t) {
    sig_pollers[0].fd = signalfd;
    sig_pollers[0].events = events_t;
    sig_pollers[0].revents = 0;
}

// Problem: need to put the event loop in a separate process/thread probably!
// how is it possible to install it here (parent) in a loop before forking, if
// the loop never ends?!
int _____main(void) {
    setbuf(stdout, NULL); // disable output buffering

    // the signal fd uses the following mask, i.e. it accepts SIGCHLD
    sigset_t mask;
    initSignalMask(&mask, SIGCHLD);

    // set up new file descriptor where parent will read messages from
    // children
    int signalfd = openSignalFD(&mask);

    // listen for a ready state on the signal fd
    struct pollfd sigfd_wrap[1];
    setSinglePollFD(sigfd_wrap, signalfd, POLLIN);

    // ... in an event loop, and print signal info!
    signalEventLoop(sigfd_wrap);

    // after this structures are installed in the parent, can now fork children!
    testChild();

    if (close(signalfd) == -1)
        perror("closing signalfd!");

    return 0;
}

/* ------------------------------------------------------------------------- */
static inline void inspectChildState(pid_t pid, int status) {
    printf("Process %ld: exit(%d) -> %s\n", (long)pid, WEXITSTATUS(status),
           strerror(WEXITSTATUS(status)));
    if (WIFSTOPPED(status))
        printf("Warning! child %ld was stopped by signal %d -> %s", (long)pid,
               WSTOPSIG(status), strerror(WSTOPSIG(status)));
    if (WIFSIGNALED(status))
        printf("Warning! child %ld was terminated by signal %d -> %s\n",
               (long)pid, WTERMSIG(status), strerror(WTERMSIG(status)));
}

volatile sig_atomic_t child_stat = 1;
volatile sig_atomic_t child_pid = 1;
volatile sig_atomic_t child_code = -100;
volatile sig_atomic_t signo_g = -100;

void bgHandler(int signo, siginfo_t *infos, void *ucontext) {
    char *log_msg = "ENTERED!\n";
    write(STDOUT_FILENO, log_msg, sizeof log_msg);

    child_code = infos->si_code;
    signo_g = signo;

    while ((child_pid = waitpid(-1, (int *)&child_stat, WNOHANG)) > 0)
        ;

    if (child_pid < 1)
        log_msg = !child_pid ? "child did not change state!\n"
                             : errno == ECHILD ? "ECHILD\n" : "unknown error\n";
    write(STDOUT_FILENO, log_msg, sizeof log_msg);
}

static inline char *sigCodeChecker(int sig_code) {
    switch (sig_code) {
    case CLD_EXITED:
        return "child exited!";
    case CLD_STOPPED:
        return "child stopped!";
    case CLD_CONTINUED:
        return "child continued!";
    default:
        return "non inspected code!";
    }
}

int main(void) {
    setbuf(stdout, NULL);

    struct sigaction bg_act = {
        .sa_flags = SA_SIGINFO, .sa_sigaction = bgHandler, .sa_mask = {{0}}};
    sigaction(SIGCHLD, &bg_act, NULL);

    int status;
    pid_t exit_pid, cpid = fork();
    switch (cpid) {
    case -1:
        perror("fork");
        exit(errno);
    case 0:
        printf("child %ld! sleep 3 sec...\n", (long)getpid());
        sleep(3);
        return 0;
    default:
        printf("Parent %ld! now block on wait for child...\n", (long)getpid());
        if ((exit_pid = waitpid(cpid, &status, 0)) == -1) {
            perror("waitpid()");
            exit(errno);
        }
        inspectChildState(exit_pid, status);
    }

    printf("signal %d -> %s\n[%ld]::code %d -> %s\n%4s::exit status %d -> %s\n",
           signo_g, signo_g == SIGCHLD ? "SIGCHLD" : "unknown signal",
           (long)child_pid, child_code, sigCodeChecker(child_code), " ",
           child_stat, strerror(child_stat));

    while (1)
        getchar();

    return 0;
}
