#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

#define READ 0
#define WRITE 1
#define N_PROC 2

void test_2_pipes(void) {
    char *prog_a[] = {"ls", "-a", NULL};
    char *prog_b[] = {"grep", "test", NULL};
    pid_t pid;

    int channel[2];
    pipe(channel);

    pid = fork();
    if (!pid) {
        printf("First fork, child pid %d\n", getpid());
        close(channel[READ]); // optional (do anyways)
        close(1);             // optional
        dup2(channel[WRITE], 1);
        execvp(prog_a[0], prog_a);
        fprintf(stderr, "Cannot run prog %s\n", prog_a[0]);
    }

    pid = fork();
    if (!pid) {
        printf("Second fork, child pid %d\n", getpid());
        close(channel[WRITE]); // optional (do anyways)
        close(0);              // optional
        dup2(channel[READ], 0);
        execvp(prog_b[0], prog_b);
        fprintf(stderr, "Cannot run prog %s\n", prog_a[0]);
    }

    close(channel[WRITE]); // widow the parent or pipeline hangs!

    for (int i = 0; i < N_PROC; i++)
        waitpid(-1, NULL, 0);
}
