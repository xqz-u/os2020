#include <dirent.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define DEBUG 0
#define READ 0
#define WRITE 1
#define STDIN STDIN_FILENO
#define STDOUT STDOUT_FILENO

typedef struct program {
    char **args;
    char *infile, *outfile;
} program;

static inline void exitError(const char *error_string) {
    perror(error_string);
    exit(EXIT_FAILURE);
}

static inline void consoleIPC(FILE *fp, const char *format_string,
                              va_list args) {
    if (vfprintf(fp, format_string, args) < 0)
        exitError("Error while printing debug string!");
    fprintf(fp, "\n");
    fflush(fp);
}

void debugErr(const char *format_string, ...) {
    va_list args;
    va_start(args, format_string);
    consoleIPC(stderr, format_string, args);
    va_end(args);
}

void printOpenFD(void) {
    DIR *dir;
    struct dirent *ent;

    if (!(dir = opendir("/proc/self/fd")))
        exitError("opening directory");

    debugErr("-----------------PROC %ld------------", (long)getpid());
    while ((ent = readdir(dir)))
        debugErr("%s", ent->d_name);
    debugErr("-------------------------------------");

    closedir(dir);
    return;
}

static inline void *safeMalloc(int sz) {
    void *p;
    if (!(p = calloc(sz, 1)))
        exitError("Fatal error: safeSafeMalloc failed.");
    return p;
}

typedef int *Pipe;

static inline Pipe newEmptyPipe(void) {
    return (Pipe){safeMalloc(2 * sizeof(int))};
}

static inline void freePipe(Pipe channel) { free(channel); }

static inline void closeStream(int fd) {
    if (fd == STDIN || fd == STDOUT) {
#if DEBUG
        debugErr("proc %ld error! won't close fd %d!!!!", (long)getpid(), fd);
#endif
        return;
    }
    if (close(fd) == -1)
        exitError("closeStream() -> close()");
}

static inline void cleanupStream(int input, int output) {
    closeStream(input);
    closeStream(output);
}

// NOTE useless?
static inline int assignStream(int old_fd) {
    closeStream(old_fd);
    int new_fd;
    if ((new_fd = dup(old_fd)) == -1)
        exitError("assignStream() -> dup()");
    return new_fd;
}

static inline void dupStream(int old_fd, int new_fd) {
    if (dup2(old_fd, new_fd) == -1)
        exitError("dupStream() -> dup2()\n");
    if (old_fd == new_fd) {
#if DEBUG
        debugErr(
            "process %ld trying to close old fd %d == new fd %d after dup2()!",
            (long)getpid(), old_fd, new_fd);
#endif
        return;
    }
    closeStream(old_fd);
}

static inline void initNewPipe(Pipe channel) {
    if (pipe(channel) == -1)
        exitError("initNewPipe() -> pipe()");
}

// NOTE useless overloading?
void initPipe(Pipe channel, int fd_read, int fd_write) {
    if (fd_read < 0 && fd_write < 0) {
        initNewPipe(channel);
        return;
    }
    if (fd_read >= 0)
        channel[READ] = assignStream(fd_read);
    if (fd_write >= 0)
        channel[WRITE] = assignStream(fd_write);
}

static inline void printPipe(Pipe channel) {
    debugErr("READ -> %d WRITE %d", channel[READ], channel[WRITE]);
}

static inline void inspectChildState(pid_t pid, int status) {
#if DEBUG
    if (WIFEXITED(status))
        debugErr("Process %ld exited with code %d", (long)pid,
                 WEXITSTATUS(status));
#endif
    if (WIFSTOPPED(status))
        debugErr("Warning! child %d was stopped with signal %d", (long)pid,
                 WSTOPSIG(status));
    if (WIFSIGNALED(status))
        debugErr("Warning! child %ld was terminated by signal %d", (long)pid,
                 WTERMSIG(status));
}

void waitChildren(int n_children) {
    int status, i;
    pid_t exit_pid;
    for (i = 0; i < n_children; i++) {
        if ((exit_pid = waitpid(-1, &status, WUNTRACED)) == -1)
            exitError("waitChildren() -> waitpid()");
        inspectChildState(exit_pid, status);
    }
}

static inline void redirectStreams(int input, int output) {
    if (input != STDIN)
        dupStream(input, STDIN);
    if (output != STDOUT)
        dupStream(output, STDOUT);
}

static inline int redirectInput(char *in_filename) {
    int new_in_fd = open(in_filename, O_RDONLY);
    if (new_in_fd == -1)
        exitError("redirectInput() -> open()");
    return new_in_fd;
}

static inline int redirectOutput(char *out_filename) {
    int new_out_fd = open(out_filename, O_WRONLY | O_TRUNC | O_CREAT);
    if (new_out_fd == -1)
        exitError("redirectOutput() -> open()");
    return new_out_fd;
}

static inline void initChannel(Pipe channel, int curr_prog, int tot_prog,
                               int *in_fd, int *out_fd, program prog) {
    if (curr_prog == 0)
        *in_fd = prog.infile ? redirectInput(prog.infile) : STDIN;

    if (curr_prog + 1 == tot_prog) {
        *out_fd = prog.outfile ? redirectOutput(prog.outfile) : STDOUT;
        return;
    }

    initPipe(channel, -1, -1);
    *out_fd = channel[WRITE];
}

static inline void run(char *filename, char **args) {
    execvp(filename, args);
    debugErr("Could not execute program \"%s\"", filename);
    exitError("run() -> execvp()");
}

void runPipeline(int n_proc, program prog) {
    Pipe channel = newEmptyPipe();
    int in_fd, out_fd;

    for (int i = 0; i < n_proc; i++) {
        initChannel(channel, i, n_proc, &in_fd, &out_fd, prog);
        switch (fork()) {
        case 0:
            redirectStreams(in_fd, out_fd);
            run(prog.args[0], prog.args);
        case -1:
            exitError("runPipeline() -> fork()");
        default:
            cleanupStream(in_fd, out_fd);
            in_fd = channel[READ];
        }
    }

    freePipe(channel);
    waitChildren(n_proc);
}

void test_no_pipe(void) {
    int n_proc = 1;
    char *prog_a[] = {"cat", NULL};
    program prog = (program){prog_a, "input", NULL};

    runPipeline(n_proc, prog);
}

void test_2_pipes(void) {
    int n_proc = 2;
    char *prog_a[] = {"cat", NULL};
    char *prog_b[] = {"xargs", "ls", NULL};

    program programs[] = {(program){prog_a, "input"},
                          (program){prog_b, "output"}};
    runPipeline(n_proc, programs[0]);
}

/* void test_3_pipes(void) { */
/*     int n_proc = 3; */
/*     char *prog_a[] = {"ls", "-a", NULL}; */
/*     char *prog_b[] = {"grep", "test", NULL}; */
/*     char *prog_c[] = {"cut", "-c1-4", NULL}; */

/*     char **progs[] = {prog_a, prog_b, prog_c}; */
/*     runPipeline(n_proc, progs); */
/* } */

/* void test_many_pipes(void) { */
/*     int n_proc = 6; */
/*     char *a[] = {"ls", "-l", "/home/pasta/", NULL}; */
/*     char *b[] = {"tail", "-n", "+2", NULL}; */
/*     // next program won't work, used just to demonstrate pipeline */
/*     char *c[] = {"sed", "'s/\s\s*\/ /g'", NULL}; */
/*     char *d[] = {"cut", "-d", "' '", "-f", "3", NULL}; */
/*     char *e[] = {"sort", NULL}; */
/*     char *f[] = {"uniq", "-c", NULL}; */

/*     char **progs[] = {a, b, c, d, e, f}; */
/*     runPipeline(n_proc, progs); */
/* } */

int main(void) {
    /* test_no_pipe(); */
    test_2_pipes();
    /* test_3_pipes(); */
    /* test_many_pipes(); */
    return 0;
}
