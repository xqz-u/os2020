#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libCommands.h"
#include "libUtils.h"

#define IN 0
#define OUT 1
#define DEBUG 0

inline command_list newCommand(program_list head) {
    command_list ret = safeMalloc(sizeof *ret);
    *ret = (command){.next = NULL,
                     .programs_head = head,
                     .infile = NULL,
                     .outfile = NULL,
                     .background = 0};
    return ret;
}

inline void freeCommand(command cmd) {
    free(cmd.infile);
    free(cmd.outfile);
    freeProgramList(cmd.programs_head);
}

void freeCommandList(command_list cmds) {
    if (!cmds)
        return;
    freeCommandList(cmds->next);
    freeCommand(*cmds);
    free(cmds);
}

inline void resetCommandList(command_list *cmd_head) {
    freeCommandList(*cmd_head);
    if (*cmd_head)
        *cmd_head = NULL;
}

void printCommand(command cmd) {
    printf("infile: %s, outfile: %s\nis background? %s\n", cmd.infile,
           cmd.outfile, (cmd.background ? "TRUE" : "FALSE"));
    printProgramList(cmd.programs_head);
}

void printCommandList(command_list cmds) {
    if (!cmds)
        return;
    printCommand(*cmds);
    printCommandList(cmds->next);
}

inline int acceptFilename(strings *token_list, char **filename_ref) {
    char *filename = retrieveString(*token_list, token_list->cursor);
    if (!filename || (isProtected(*filename) && !isBg(*filename)))
        return 0;
    *filename_ref = strdup(filename);
    token_list->cursor++;
    return 1;
}

int acceptRedirection(strings *token_list, command_list cmd,
                      int next_redirection, int is_in) {
    if (next_redirection == (is_in ? OUT : IN))
        return 0;

    char *tok = retrieveString(*token_list, token_list->cursor);

    if (!tok || !(is_in ? isInfile(*tok) : isOutfile(*tok)))
        return 0;

    token_list->cursor++;
    return acceptFilename(token_list, (is_in ? &cmd->infile : &cmd->outfile));
}

int setIOFilenames(strings *token_list, command_list cmd, int next_is) {
    if (!retrieveString(*token_list, token_list->cursor))
        return 1; // empty case

    int status = 0;
    if (acceptRedirection(token_list, cmd, next_is, 1)) {
        next_is = OUT;
        status = setIOFilenames(token_list, cmd, next_is);
    } else if (acceptRedirection(token_list, cmd, next_is, 0)) {
        next_is = IN;
        status = setIOFilenames(token_list, cmd, next_is);
    }

    return status;
}

void parseIO(strings *token_list, command_list *cmd) {
    int valid_IO = setIOFilenames(token_list, *cmd, -1);

    if ((*cmd)->infile && (*cmd)->outfile &&
        !strcmp((*cmd)->infile, (*cmd)->outfile)) {
        printf("Error: input and output files cannot be equal!\n");
        resetCommandList(cmd);
        return;
    }

    if (valid_IO)
        return;

    char *allow_bg = retrieveString(*token_list, token_list->cursor);

    if (!(allow_bg && isBg(*allow_bg))) {
        printf("Error: invalid syntax!\n");
        resetCommandList(cmd);
    }
}

command_list parseCommand(strings *token_list) {
    if (!retrieveString(*token_list, token_list->cursor))
        return NULL; // empty command case

    program_list progs_head = NULL;
    command_list cmd = newCommand(parseProgramList(token_list, &progs_head));

    if (!cmd->programs_head) {
        resetCommandList(&cmd);
        return cmd; // invalid syntax error occurred during program parsing
    }

    parseIO(token_list, &cmd);
    return cmd;
}

command_list parseCommandList(strings *token_list, command_list *head) {
    if (!(*head = parseCommand(token_list)))
        return *head;

    char *cmd_delim = retrieveString(*token_list, token_list->cursor);

    if (cmd_delim && isBg(*cmd_delim)) {
        (*head)->background = 1;
        token_list->cursor++;
        (*head)->next = parseCommandList(token_list, &(*head)->next);
        if (!(*head)->next && retrieveString(*token_list, token_list->cursor))
            resetCommandList(head);
    }

    return *head;
}

int countBgPrograms(command_list cmds) {
    command_list c;
    program_list p;
    int pipeline_bg = 0;
    for (c = cmds; c && c->background; c = c->next)
        for (p = c->programs_head; p; p = p->next)
            pipeline_bg++;
#if DEBUG
    printf("new input line contains %d BG processes\n", pipeline_bg);
#endif
    return pipeline_bg;
}
