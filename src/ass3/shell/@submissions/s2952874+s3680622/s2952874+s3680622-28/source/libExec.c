#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "libDebug.h"
#include "libExec.h"
#include "libStrings.h"
#include "libUtils.h"

#define DEBUG 0

inline void inspectChildState(pid_t pid, int status) {
    printf("Process %ld: exit(%d) -> %s\n", (long)pid, WEXITSTATUS(status),
           strerror(WEXITSTATUS(status)));
    if (WIFSTOPPED(status))
        debugErr("Warning! child %d was stopped by signal %d -> %s", (long)pid,
                 WSTOPSIG(status), strerror(WSTOPSIG(status)));
    if (WIFSIGNALED(status))
        debugErr("Warning! child %ld was terminated by signal %d -> %s\n",
                 (long)pid, WTERMSIG(status), strerror(WTERMSIG(status)));
}

/* Wait blocking on single process */
inline void waitChild(program_list child_prog) {
#if DEBUG
    debugErr("waiting on proc %ld", (long)child_prog->pid);
#endif
    if (waitpid(child_prog->pid, &child_prog->exit_status, 0) == -1)
        exitError("waitChild() -> waitpid()");
#if DEBUG
    inspectChildState(child_prog->pid, child_prog->exit_status);
#endif
}

/* Wrapper for the open syscall */
inline int openFile(char *filename, int flags, mode_t mod) {
    int new_fd = mod ? open(filename, flags, mod) : open(filename, flags);
    if (new_fd == -1)
        exitError("redirectOutput() -> open()");
    return new_fd;
}

inline void initChannel(Pipe channel, program_list curr_prog, char *outfile,
                        int *out_fd) {
    // route last output stream, either other file if any or to stdout
    if (!curr_prog->next) {
        *out_fd = outfile
                      ? openFile(outfile, O_CREAT | O_WRONLY | O_TRUNC, 0666)
                      : STDOUT;
        return;
    }
    initPipe(channel); // create a new pipe!
    // in all cases except the the last program, output will be routed to the
    // write end of the pipe
    *out_fd = channel[WRITE];
}

/* Wrapper for the execvp syscall! */
inline void run(char *filename, char **args) {
#if DEBUG
    debugErr("Process %ld executes prog '%s'", (long)getpid(), filename);
#endif
    // Strip quotes used to keep quoted args together in one single parameter!
    char *unquot_args[] = {filename, NULL, NULL};
    char *quoted_arg = args[1];
    if (quoted_arg && isQuotes(*quoted_arg) &&
        isQuotes(quoted_arg[strlen(quoted_arg) - 1]))
        unquot_args[1] = extractSubstr(quoted_arg, 1, strlen(quoted_arg) - 1);
    int ret = execvp(filename, unquot_args[1] ? unquot_args : args);
    if (ret == -1 && errno == ENOENT)
        fprintf(stdout, "Error: command not found!\n");
}

/* This function forks the children processes needed to execute a pipeline. The
 * parent initializes the pipe, forks the children, handles IO routing and waits
 * for foreground processes, whreas the children duplicate IO streams inherited
 * from the parent and run an executable. */
void runPipeline(program_list programs, char *infile, char *outfile,
                 int is_background) {
    Pipe channel = newEmptyPipe(); // create pipe (alloc'd just to pass it
                                   // between functions)
    int in_fd, out_fd;
    pid_t pid_ref;
    program_list prog;

    // the first input fd is either stdin or a file input redirection!
    in_fd = infile ? openFile(infile, O_RDONLY, 0) : STDIN;

    // fork child processes which will execute all programs in a pipeline!
    for (prog = programs; prog; prog = prog->next) {
        initChannel(channel, prog, outfile, &out_fd);
        switch (pid_ref = fork()) {
        case -1:
            exitError("runPipeline() -> fork()");
        case 0:
            // duplicate fd for stdin and stdout to pipe ends set in the parent
            redirectStreams(in_fd, out_fd);
            run(prog->prog_args.args[0], prog->prog_args.args);
            exit(errno);
        default:
            cleanupStream(in_fd, out_fd); // widow the pipes from the parent or
                                          // child will hang waiting from EOF
                                          // because of exec not closing them...
            in_fd = channel[READ];        // where next child will read from
            prog->pid = pid_ref;
            if (!is_background) // wait sequentially for foreground processes
                waitChild(prog);
        }
    }
    freePipe(channel);
}

/* This function recursively runs a whole input line entered by the user*/
void runJobs(command_list commands) {
    if (!commands)
        return;
    runPipeline(commands->programs_head, commands->infile, commands->outfile,
                commands->background);
    runJobs(commands->next);
}
