#ifndef LIBUTILS_H
#define LIBUTILS_H

/* #define str_terminate(len) ((len) + 1) */
/* #define isNewline(c) ((c) == '\n') */
/* #define isStrTerm(c) ((c) == '\0') */
/* #define isEOF(c) ((c) == EOF) */
/* #define isTerminator(c) ((isNewline(c)) || (isStrTerm(c)) || (isEOF(c))) */

#include <sys/types.h>

extern void *safeMalloc(int sz);
extern void resize(void **store, int *nmemb, size_t size);
extern void checkResize(void **store, int *nmemb, size_t size, int n_filled);
extern void printCharArr(char **args, int args_sz);
extern void freeCharArr(char **args, int args_sz);

#endif
