#define _GNU_SOURCE
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>

#include "libDebug.h"
#include "libStrings.h"
#include "libUtils.h"

inline int isTerminator(char c) { return (c == '\n' || c == '\0'); }

inline int isQuotes(char c) { return (c == '\'' || c == '\"'); }

inline int isPipe(char c) { return c == '|'; }

inline int isBg(char c) { return c == '&'; }

inline int isInfile(char c) { return c == '<'; }

inline int isOutfile(char c) { return c == '>'; }

inline int isProtected(char c) {
    return (isPipe(c) || isBg(c) || isInfile(c) || isOutfile(c));
}

inline strings newStrings(int size) {
    return (strings){.args = safeMalloc(size * sizeof(char *)),
                     .tot = 0,
                     .cursor = 0,
                     .n_args = size};
}

inline void freeStrings(strings s_store) {
    freeCharArr(s_store.args, s_store.tot);
}

inline void printStrings(strings s_store) {
    printf("\tcursor is %d -> %s (tot size: %d)\n", s_store.cursor,
           s_store.args[s_store.cursor], s_store.n_args);
    printCharArr(s_store.args, s_store.tot);
    printf("\n");
}

void appendString(strings *s_store, char *s) {
    if (!s)
        s_store->args[s_store->tot++] = NULL;
    else {
        if (!(s_store->args[s_store->tot++] = strdup(s)))
            exitError("appendString() -> strdup()");
    }
    checkResize((void **)&s_store->args, &s_store->n_args,
                sizeof *s_store->args, s_store->tot);
}

inline char *retrieveString(strings s_store, int idx) {
    if (idx >= s_store.tot) {
        debugOut("tot is %d and you asked for string #%d!", s_store.tot, idx);
        exitError("retrieveString()");
    }
    return s_store.args[idx];
}

char *extractSubstr(char *og_str, int start, int end) {
    if (end == start)
        return NULL;
    char *ret_w = safeMalloc(end - start + 1);
    ssize_t i;
    for (i = start; i < end; i++)
        ret_w[i - start] = og_str[i];
    ret_w[i - start] = 0; // null terminate
    return ret_w;
}

inline void completeQuotes(char *og_str, ssize_t *cursor, char *ccur) {
    char terminal = *ccur == '\"' ? '\"' : '\'';
    *ccur = og_str[(*cursor)++];
    while (!(*ccur == terminal || isTerminator(*ccur)))
        *ccur = og_str[(*cursor)++];
    *ccur = og_str[*cursor <= strlen(og_str) ? (*cursor)++ : *cursor - 1];
}

strings tokenize(char *input_str) {
    strings ret = newStrings(DEFAULT_PROG_ARGS);
    char *new_w, ccur;
    ssize_t start_w, end_w;
    int enable_quote, exit;

    start_w = end_w = enable_quote = exit = 0;
    while (!exit) {
        ccur = input_str[end_w++];
        if (isQuotes(ccur)) {
            completeQuotes(input_str, &end_w, &ccur);
            enable_quote = 1;
        }
        exit = isTerminator(ccur);
        if (ccur == ' ' || enable_quote || exit) {
            new_w = extractSubstr(input_str, start_w, end_w - 1);
            start_w = end_w;
            if (new_w) {
                appendString(&ret, new_w);
                free(new_w);
            }
        }
        enable_quote = 0;
    }

    appendString(&ret, NULL);
    return ret;
}

char *readline(void) {
    int input_len = MAX_INPUT, i = 0;
    char c, *input = safeMalloc(input_len);

    /* printf(">> "); */

    c = fgetc(stdin);
    while (!(c == EOF || c == '\n')) {
        input[i++] = c;
        checkResize((void **)&input, &input_len, sizeof *input, i);
        c = fgetc(stdin);
    }
    input[i] = 0; // Add null terminator

    return input;
}
