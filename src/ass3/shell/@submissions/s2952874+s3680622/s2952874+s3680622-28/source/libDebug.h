#ifndef LIBDEBUG_H
#define LIBDEBUG_H

#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>

void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...);

#define throw_error(fd, formatter, ...)                                        \
    errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

extern void exitError(const char *error_string);
extern void console(FILE *fp, const char *format_string, va_list args);
extern void debugFile(FILE *fp, const char *format_string, ...);
extern void debugErr(const char *format_string, ...);
extern void debugOut(const char *format_string, ...);
void printProcessOpenFD(FILE *fp, pid_t pid);

#endif
