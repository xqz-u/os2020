#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "libDebug.h"
#include "libPipe.h"
#include "libUtils.h"

#define DEBUG 0

inline Pipe newEmptyPipe(void) { return (Pipe){safeMalloc(2 * sizeof(int))}; }

inline void freePipe(Pipe channel) { free(channel); }

inline void closeStream(int fd) {
    if (fd == STDIN || fd == STDOUT) {
#if DEBUG
        debugErr("proc %ld error! won't close fd %d, it's already std!",
                 (long)getpid(), fd);
#endif
        return;
    }
    if (close(fd) == -1)
        exitError("closeStream() -> close()");
}

inline void cleanupStream(int input, int output) {
    closeStream(input);
    closeStream(output);
}

inline void dupStream(int old_fd, int new_fd) {
    if (dup2(old_fd, new_fd) == -1)
        exitError("dupStream() -> dup2()\n");
    if (old_fd == new_fd) {
#if DEBUG
        debugErr(
            "process %ld trying to close old fd %d == new fd %d after dup2()!",
            (long)getpid(), old_fd, new_fd);
#endif
        return;
    }
    closeStream(old_fd);
}

inline void redirectStreams(int input, int output) {
    if (input != STDIN)
        dupStream(input, STDIN);
    if (output != STDOUT)
        dupStream(output, STDOUT);
}

inline void initPipe(Pipe channel) {
    if (pipe(channel) == -1)
        exitError("initNewPipe() -> pipe()");
}

inline void printPipe(Pipe channel) {
    debugErr("READ -> %d WRITE %d", channel[READ], channel[WRITE]);
}
