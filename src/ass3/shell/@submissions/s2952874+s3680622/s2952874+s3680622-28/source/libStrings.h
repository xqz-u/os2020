#ifndef LIBSTRINGS_H
#define LIBSTRINGS_H

#include <sys/types.h>

typedef struct strings {
    char **args;
    int tot, cursor, n_args;
} strings;

#define DEFAULT_PROG_ARGS 10

extern int isTerminator(char c);
extern int isQuotes(char c);
extern int isPipe(char c);
extern int isBg(char c);
extern int isInfile(char c);
extern int isOutfile(char c);
extern int isProtected(char c);
extern strings newStrings(int size);
extern void freeStrings(strings s_store);
extern void printStrings(strings s_store);
void appendString(strings *s_store, char *s);
extern char *retrieveString(strings s_store, int idx);
char *extractSubstr(char *og_str, int start, int end);
extern void completeQuotes(char *og_str, ssize_t *cursor, char *ccur);
strings tokenize(char *input_str);
char *readline(void);

#endif
