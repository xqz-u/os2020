#ifndef LIBCOMMANDS_H
#define LIBCOMMANDS_H

#include "libProgram.h"

typedef struct command *command_list;

typedef struct command {
    command_list next;
    program_list programs_head;
    char *infile, *outfile;
    int background;
} command;

extern command_list newCommand(program_list head);
extern void freeCommand(command cmd);
void freeCommandList(command_list cmds);
extern void resetCommandList(command_list *cmd_head);
void printCommand(command cmd);
void printCommandList(command_list cmds);
extern int acceptFilename(strings *token_list, char **filename_ref);
int acceptRedirection(strings *token_list, command_list cmd,
                      int next_redirection, int is_in);
int setIOFilenames(strings *token_list, command_list cmd, int next_is);
int redirectIO(strings *token_list, command_list cmd, int redirect_in);
int setIOFilenames(strings *token_list, command_list cmd, int next_is);
void parseIO(strings *token_list, command_list *cmd);
command_list parseCommand(strings *token_list);
command_list parseCommandList(strings *token_list, command_list *head);
int countBgPrograms(command_list cmds);

#endif
