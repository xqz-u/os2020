#define _GNU_SOURCE
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libDebug.h"

// Error handling
void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...) {
    fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
    va_list err_args;
    va_start(err_args, cformat);
    if (vfprintf(err_dest, cformat, err_args) <= 0)
        fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
    fprintf(err_dest, "\n");
    va_end(err_args);
}
#define throw_error(fd, formatter, ...)                                        \
    errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

inline void exitError(const char *error_string) {
    perror(error_string);
    exit(EXIT_FAILURE);
}

inline void console(FILE *fp, const char *format_string, va_list args) {
    if (vfprintf(fp, format_string, args) < 0)
        exitError("Error while printing debug string!");
    fflush(fp);
    fprintf(fp, "\n");
    fflush(fp);
}

// NOTE fp must be an open valid file stream!
inline void debugFile(FILE *fp, const char *format_string, ...) {
    va_list args;
    va_start(args, format_string);
    console(fp, format_string, args);
    va_end(args);
}

inline void debugErr(const char *format_string, ...) {
    va_list args;
    va_start(args, format_string);
    console(stderr, format_string, args);
    va_end(args);
}

inline void debugOut(const char *format_string, ...) {
    va_list args;
    va_start(args, format_string);
    console(stdout, format_string, args);
    va_end(args);
}

void printProcessOpenFD(FILE *fp, pid_t pid) {
    DIR *dir;
    struct dirent *ent;

    char *proc_path;
    if ((long)pid == -1) {
        if (asprintf(&proc_path, "/proc/self/fd") == -1)
            exitError("printProcessOpenFD -> asprintf()");
    } else {
        if (asprintf(&proc_path, "/proc/%ld/fd", (long)pid) == -1)
            exitError("printProcessOpenFD -> asprintf()");
    }

    if (!(dir = opendir(proc_path)))
        exitError("opening directory");

    debugFile(fp, "-----------------PROC %ld------------", (long)getpid());
    while ((ent = readdir(dir)))
        debugFile(fp, "%s", ent->d_name);
    debugFile(fp, "-------------------------------------");

    closedir(dir);
    free(proc_path);
    return;
}
