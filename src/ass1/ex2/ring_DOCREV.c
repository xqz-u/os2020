#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// This struct holds a pipe to ease (bidirectional) communication
typedef struct Pipe {
  int r_end, w_end;
} Pipe;

// Struct that defines a process which can communicate through pipes
typedef struct Process {
  int parent;
  int gen;
  pid_t pid;
  Pipe read, write;
} Process;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

Pipe newPipe() {
  int fd[2];
  /* Pipe p; */
  if (pipe(fd) == -1)
    perror("Piping!");
  /* p.r_end = fd[0]; */ /* for style purpose u can use literal struct
                      /* instead of assigning each element to the
                      /* corresponding fields*/
  /* p.w_end = fd[1]; */
  /* return p; */
  /*
    return (Pipe){fd[0],fd[1]}
  */
  return (Pipe){.r_end = fd[0], .w_end = fd[1]};
}

Pipe *makePipes(int len) {
  Pipe *store = safeMalloc(len * sizeof(Pipe));
  for (int i = 0; i < len; i++)
    store[i] = newPipe();
  return store;
}

Process newProcess(pid_t proc_id, int generation, int master) {
  return (Process){.parent = master, .gen = generation, .pid = proc_id};
  /* Process proc; */
  /* proc.gen = generation; */
  /* proc.pid = proc_id; */
  /* proc.parent = master; */
  /* return proc; */ /* use literal it's a oneliner */
  /*
    return (Process){master,generation,proc_id}
  */
  /*
    however remember that literal are order based by default but u can
    declare also literal which do not follow the normal order
    Ex (Process) {.parent = master, .gen = generation, .pid = proc_id}
  */
}

// Each time a process has to read, it closes its write side of the pipe
// before
void readPipe(Pipe rp, int *value, int single) {
  if (!single)
    close(rp.w_end);
  if (read(rp.r_end, value, sizeof(int)) == -1) {
    printf("Reading from %d\n", rp.r_end);
    perror("Error");
    exit(EXIT_FAILURE);
  }
  /*
    here maybe u can switch the if statement like

    if (read(rp.r_end, value, sizeof(int)) != -1) return;

    printf("Reading from %d\n", rp.r_end);
    perror("Error");
    exit(EXIT_FAILURE);
  */
}

// Each time a process has to write, it closes its read side of the pipe before
void writePipe(Pipe wp, int *value, int single) {
  if (!single)
    close(wp.r_end);
  if ((write(wp.w_end, value, sizeof(int))) == -1) {
    printf("Writing to %d\n", wp.w_end);
    perror("Error");
    exit(EXIT_FAILURE);
  }
  /* same here but it's a style stuff so maybe u prefer like this so
     /* it's up to u*/
}

// This function prints the desired output, and it takes care of terminating the
// children if the target has been reached by passing a negative argument
// message regardless of the process being a parent or not
void ringTalk(Process proc, int target, int n_proc) {
  int count;
  int multiple = n_proc > 1 ? 0 : 1; // If there is only one process, the other
                                     // end of the pipe must not be closed!
  while (1) {
    readPipe(proc.read, &count, multiple);
    if (count < 0 || count >= target) {
      if (count >= target) {
        printf("relative pid=%d: %d\n", proc.gen, count);
        fflush(stdout);
      }
      count = -1;
      writePipe(proc.write, &count, multiple);
      break;
    }
    printf("relative pid=%d: %d\n", proc.gen, count++);
    fflush(stdout);
    writePipe(proc.write, &count, multiple);
  }
  if (!proc.parent)
    return;
  /*
    maybe you can rewrite this function !! because in general try to
    avoid while(1) if u can because can be sometime confusing and also
    u put ur self in possible non terminating situation An idea could
    be to move the first if condition inside the while() so something
    like while (count<0 || count >= target) {....}
  */
}

// This function just waits for children to die
void waitChildren(int tot) {
  int status, exit_pid;
  for (int i = 0; i < tot; i++) {
    exit_pid = wait(&status);
    if (!WIFEXITED(status))
      printf("Warning! child %d exited with code %d\n", exit_pid, status);
    if (WIFSIGNALED(status))
      printf("Warning! child %d was terminated by signal %d\n", exit_pid,
             WTERMSIG(status));
  }
  /*
    usually in concurrent/parallel the way to wait others
    process/thread/stuff is to create an exit channel for each
    process/thread in which he will write when he is done but is not a
    big concern here but remember that u can always avoid using wait
    or stuff like. U can for example use this exit channel/pipe to
    sync on the exit of each process
  */
}

// Spawn given number of processes by forking the program and return them with
// their 'age' and 'parenthood' information
Process spawnProcess(int tot) {
  pid_t pid;
  Process p;

  for (int i = 1; i < tot; i++) {
    pid = fork();
    if (pid == -1) {
      perror("Forking!");
      exit(EXIT_FAILURE);
    }
    if (!pid) {
      p = newProcess(getpid(), i, 0); /* why ?? u can just return the
                                       * call like return
                                       * newProcess(...);*/
      return p;
    }
  }
  // Here, the process must be the parent, which is returned at last
  p = newProcess(getpid(), 0, 1);
  return p; /* also there why ? return the call !!! it's more /*
               efficient and maybe more clear*/
  /* Ex : */
  /* Process spawnProcess(int tot) */
  /* { */
  /*   pid_t pid; */

  /*   for (int i = 1; i < tot; i++){ */
  /*     if ((pid = fork()) == -1) { */
  /*       perror("Forking!"); */
  /*       exit(EXIT_FAILURE); */
  /*     } */
  /*     if (!pid) break ; */
  /*   } */
  /*   return  !pid ? */
  /*     newProcess(getpid(), i, 0) */
  /*     : */
  /*     newProcess(getpid(),0, 1); */
  /* } */
}

// Give a process the communication channels suiting the problem's logic
void linkProcess(Process *p, Pipe read_end, Pipe write_end) {
  p->read = read_end;
  p->write = write_end;
}

int main(int argc, char *argv[]) {
  setbuf(stdout, NULL); // For Themis

  int n_proc;
  scanf("%d", &n_proc);

  Pipe *pipe_store = makePipes(n_proc);

  Process proc = spawnProcess(n_proc);

  // Each process reads from the process to its left and writes to the process
  // on its right
  if (proc.parent)
    linkProcess(&proc, pipe_store[n_proc - 1], pipe_store[0]);
  if (!proc.parent)
    linkProcess(&proc, pipe_store[proc.gen - 1], pipe_store[proc.gen]);

  if (proc.parent > 0) { // When process is parent...
    int count = 0;
    printf("relative pid=%d: %d\n", proc.gen,
           count++); // the parent starts the ring!
    writePipe(proc.write, &count, (n_proc > 1 ? 0 : 1));
  }

  // Children just enter the ring and continue the communication
  ringTalk(proc, 50, n_proc);

  // When the communication is finished, the parent waits for the children to
  // exit (if they exist...)
  if (proc.parent && n_proc > 1)
    waitChildren(n_proc);

  free(pipe_store);
  return 0;
}
