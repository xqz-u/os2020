#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define READ_END 0
#define WRITE_END 1

void parentMassSuicide(int n_proc, int ball, int channels[n_proc][2]) {
  printf("BALL IS 10, EXITTTT! should notify children to dieee\n");
  ball = -1;
  write(channels[1][READ_END], &ball, sizeof(ball));
  close(channels[1][READ_END]);
  printf("Parent had 10! Now sending death msg to first child and waiting "
         "for all to die\n");
  pid_t exit_pid;
  int status;
  for (int j = 1; j < n_proc; j++) {
    exit_pid = waitpid(-1, &status, 0);
    printf("Child %d just exited with code %d!\n", exit_pid, status);
  }
}

void parentWaitsSuicide(int remaining_proc, int ball, int channels[][2]) {
  printf("Parent will wait for the remaining %d children to die...\n",
         remaining_proc);
  ball -= 1;
  write(channels[1][READ_END], &ball, sizeof(ball));
  close(channels[1][READ_END]);
  pid_t exit_pid;
  int status;
  for (int j = remaining_proc; j > 0; j--) {
    exit_pid = waitpid(-1, &status, 0);
    printf("Child %d just exited with code %d!\n", exit_pid, status);
  }
}

void talk(int n_proc, int child_id, int read_prev, int write_next,
          int my_pipe[2], int ball) {
  printf("\tChild %d (i=%d) ready to talk! READ on %d and WRITE on %d\n",
         getpid(), child_id, read_prev, write_next);
  while (read(read_prev, &ball, sizeof(ball)))
    ;
  close(read_prev);
  if (ball < 0) {
    printf("\tChild %d passes on death signal %d + 1 to %d and die!\n",
           getpid(), write_next, ball);
    ball -= 1;
    write(write_next, &ball, sizeof(ball));
    close(write_next);
    exit(0);
  }
  printf("\tChild relative pid=%d ball=%d\n", child_id, ball);
  if (ball == 10) {
    ball = -1;
    printf("\t10 reachced! Child %d will write %d to %d and die!\n", getpid(),
           ball, write_next);
    write(write_next, &ball, sizeof(ball));
    close(write_next);
    exit(0);
  }
  ball++;
  write(write_next, &ball, sizeof(ball));
  close(write_next);
}

void ringCommunication(int n_proc) {
  int channels[n_proc][2];
  int ball = 0, i;
  pid_t pid;

  for (i = 0; i < n_proc; i++) {
    if (pipe(channels[i]) == -1)
      perror("Error piping!");
  }

  printf("Available FD for communication:\n");
  for (i = 0; i < n_proc; i++)
    printf("i %d, READ %d WRITE %d\n", i, channels[i][0], channels[i][1]);

  for (i = 1; i < n_proc; i++) {
    pid = fork();
    if (pid > 0) { // Parent
      continue;
    } else if (pid == 0) { // Children
      break;
    } else {
      perror("Error forking!");
      exit(EXIT_FAILURE);
    }
  }

  if (pid > 0) {
    printf(
        "All children should be spawned, parent is %d. START RING, ball=%d!\n",
        getpid(), ball++);
    while (1) {
      close(channels[0][READ_END]);
      close(channels[1][WRITE_END]);
      printf("Parent is writing message %d on fd %d!\n", ball,
             channels[1][READ_END]);
      write(channels[1][READ_END], &ball, sizeof(ball));
      printf("Write from parent worked!\n");
      close(channels[0][WRITE_END]);
      close(channels[1][READ_END]);
      while (read(channels[n_proc - 1][WRITE_END], &ball, sizeof(ball)))
        ;
      printf("Parent received ball=%d and closed fd %d!\n", ball,
             channels[n_proc - 1][WRITE_END]);
      if (ball == 10) {
        parentMassSuicide(n_proc, ball, channels);
        return;
      } else if (ball < 0) {
        parentWaitsSuicide(n_proc - 1 + ball, ball, channels);
        return;
      }
      ball++;
    }
  }

  if (pid == 0) {
    // My reasoning is that each process will read on the previous index's write
    // end, and write to the next index's write end, and that they should block
    // on the read and write calls thus creating a forced "natural" order of
    // communicaton...
    while (1) {
      talk(n_proc, i, channels[(i - 1) % n_proc][WRITE_END],
           channels[(i + 1) % n_proc][READ_END], channels[i], ball);
    }
  }
}

int main(int argc, char *argv[]) {
  int num_proc;

  scanf("%d", &num_proc);
  ringCommunication(num_proc);
  printf("All children returned, parent %d can die. Goodbye!\n", getpid());

  return 0;
}
