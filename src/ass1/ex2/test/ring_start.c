#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// Error handling
void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  if (vfprintf(err_dest, cformat, err_args) <= 0)
    fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throw_error(fd, formatter, ...)                                        \
  errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

int main(int argc, char *argv[]) {
  /* setbuf(stdout, NULL); */
  pid_t pid;
  int status;
  int n_proc, i;

  scanf("%d", &n_proc);
  pid_t *proc_store = malloc(n_proc * sizeof(pid_t));

  for (i = 0; i < n_proc; i++) {
    pid = fork();
    if (pid > 0) {
      /* proc_store[i] = pid; */
      printf("Parent: pid at proc[%d] = %d\n", i, proc_store[i]);
      waitpid(pid, &status, 0);
    } else if (pid == 0) {
      proc_store[i] = getpid();
      printf("Child: getpid() %d pid at proc[%d] = %d\n", getpid(), i,
             proc_store[i]);
      exit(0);
    } else {
      perror("forking");
    }
  }

  for (i = 0; i < n_proc; i++)
    printf("pid %d\n", proc_store[i]);

  free(proc_store);
  return 0;
}
