#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_MALLOC 256
#define DEBUG 0
#define READ_END 0
#define WRITE_END 1
#define PARENT 0
#define CHILD 1
#define NUM_PROC 2

#define read_pipe(PROC, read_buff)                                             \
  close(fd[PROC][WRITE_END]);                                                  \
  read(fd[PROC][READ_END], read_buff, sizeof(read_buff));

#define write_pipe(PROC, write_buff)                                           \
  close(fd[PROC][READ_END]);                                                   \
  write(fd[PROC][WRITE_END], write_buff, sizeof(write_buff));

void printPipes(int fd[NUM_PROC][2]) {
  for (int i = 0; i < NUM_PROC; i++)
    printf("Pipe: %d -> %d :: READ %d WRITE %d\n", i, (i + 1) % NUM_PROC,
           fd[i][READ_END], fd[i][WRITE_END]);
  printf("\n");
}

void initPipes(int fd[NUM_PROC][2]) {
  for (int i = 0; i < NUM_PROC; i++) {
    if (pipe(fd[i]) == -1)
      perror("Piping!");
  }
#if DEBUG
  printPipes(fd);
#endif
}

void sendMsg(int fd[NUM_PROC][2]) {
  int father = 189;
  int status;

  write_pipe(PARENT, &father);

  read_pipe(CHILD, &father);

  fprintf(stdout, "Parent received : %d\n", father);
  fflush(stdout);

  waitpid(-1, &status, 0);
  fprintf(stdout, "Child died with code %d\n", status);
}

void receiveMsg(int fd[NUM_PROC][2]) {
  int child;

  read_pipe(PARENT, &child);

  fprintf(stderr, "Child %d received message: %d\n", getpid(), child);
  fflush(stderr);

  child *= 10;

  write_pipe(CHILD, &child);

  exit(0);
}

int __main(void) {
  int channels[NUM_PROC][2];
  pid_t pid;

  initPipes(channels);

  pid = fork();

  if (pid == 0) {
    receiveMsg(channels);
  } else if (pid > 0) {
    sendMsg(channels);
  } else {
  }

  return 0;
}

void childBalls(int fd[NUM_PROC][2]) {
  int ball = 1;
  while (ball > 0) {
    read_pipe(PARENT, &ball);
    fprintf(stdout, "Child, ball is %d\n", ball);
    ball++;
    write_pipe(CHILD, &ball);
  }
  exit(0);
}

void parentBalls(int fd[NUM_PROC][2]) {
  int ball = 0, status;
  fprintf(stdout, "Parent, ball is %d\n", ball);
  ball++;
  while (ball < 10) {
    write_pipe(PARENT, &ball);
    read_pipe(CHILD, &ball);
    fprintf(stdout, "Parent, ball is %d\n", ball);
    ball++;
  }
  ball = -1;
  write_pipe(PARENT, &ball);
  waitpid(-1, &status, 0);
  fprintf(stdout, "Child exited with code %d\n", status);
}

int main(void) {
  int channels[NUM_PROC][2];
  pid_t pid;

  initPipes(channels);

  pid = fork();

  if (pid == 0) {
    childBalls(channels);
  } else if (pid > 0) {
    parentBalls(channels);
  } else {
    perror("Forking!");
  }

  return 0;
}
