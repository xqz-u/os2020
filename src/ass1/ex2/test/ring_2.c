#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define DEBUG 1
#define READ_END 0
#define WRITE_END 1
#define NUM_PROC 2

void printPipes(int fd[NUM_PROC][2]) {
  for (int i = 0; i < NUM_PROC; i++)
    printf("Pipe: %d -> %d :: READ %d WRITE %d\n", i, (i + 1) % NUM_PROC,
           fd[i][READ_END], fd[i][WRITE_END]);
  printf("\n");
}

void initPipes(int fd[NUM_PROC][2]) {
  for (int i = 0; i < NUM_PROC; i++) {
    if (pipe(fd[i]) == -1)
      perror("Piping!");
  }
#if DEBUG
  printPipes(fd);
#endif
}

void waitChildrenSuicide(int remaining) {
  int status, exit_child;
  for (int i = 0; i < remaining; i++) {
    exit_child = waitpid(-1, &status, 0);
    printf("Child %d is dead with code %d\n", exit_child, status);
  }
}

void childAction(int child_id, int fd[NUM_PROC][2], int ball, int check) {
  printf("\tChild %d entered childAction!\n", getpid());
  while (1) {
    /* First off a child needs to receive a message from its left neighbor ->
     * close current child's write end and left neighbor's read end */
    close(fd[child_id][WRITE_END]);
    printf("\tChild %d closed fd %d...\n", getpid(), fd[child_id][WRITE_END]);
    close(fd[(child_id - 1) % NUM_PROC][READ_END]);
    printf("\tChild %d closed fd %d...\n", getpid(),
           fd[(child_id - 1) % NUM_PROC][READ_END]);
    printf("Child %d waiting to read on fd %d...\n", getpid(),
           fd[child_id][READ_END]);
    while (read(fd[child_id][READ_END], &ball, sizeof(ball)))
      ;

    /* The child will then write on its right -> close child's reading end and
     * right neighbor writing end */
    close(fd[child_id][READ_END]);
    printf("\tChild %d closed fd %d...\n", getpid(), fd[child_id][READ_END]);
    close(fd[(child_id + 1) % NUM_PROC][WRITE_END]);
    printf("\tChild %d closed fd %d...\n", getpid(),
           fd[(child_id + 1) % NUM_PROC][WRITE_END]);

    if (ball + 1 == check) {
      ball = -1;
      write(fd[(child_id + 1) % NUM_PROC][READ_END], &ball, sizeof(ball));
      exit(0);
    } else if (ball < 0) {
      ball -= 1;
    } else {
      printf("Relative pid=%d, ball=%d\n", child_id, ball++);
    }

    write(fd[(child_id + 1) % NUM_PROC][READ_END], &ball, sizeof(ball));
  }
}

void parentAction(int fd[NUM_PROC][2], int ball, int check) {
  /* Parent need to start the ring! Print first output and start */
  /* communication; close parent's read and child's write*/
  printf("Relative pid=0, ball=%d\n", ball++);

  printf("Parent freeing its read end %d... \n", fd[0][READ_END]);
  if (close(fd[0][READ_END]) == -1) {
    perror("Error closing PARENT's read end!");
  }
  printf("Parent freeing child's write end %d...\n", fd[1][WRITE_END]);
  if (close(fd[0][WRITE_END]) == -1) {
    perror("Error closing child 1 write end!");
  }
  printf("Parent writing %d on its write end %d...\n", ball, fd[0][WRITE_END]);
  if (write(fd[0][WRITE_END], &ball, sizeof(ball)) == -1)
    perror("Error in writing on fd %d\n!");

  while (1) {
    /* After initialization, the first action is to read -> close parent's
     * write side and next child read side */
    /* close(fd[0][WRITE_END]); */
    /* printf("Parent closed fd %d!\n", fd[0][WRITE_END]); */
    close(fd[1][READ_END]);
    printf("Parent closed fd %d!\n", fd[1][READ_END]);
    printf("Parent waits to read on fd %d...\n", fd[0][READ_END]);
    while (read(fd[0][READ_END], &ball, sizeof(ball)))
      ;
    printf("Parent read %d!\n", ball);

    /* Parent will then write to next child, so will close his read side and
     * the child write side */
    close(fd[0][READ_END]);
    printf("Parent closed fd %d!\n", fd[0][READ_END]);
    close(fd[1][WRITE_END]);
    printf("Parent closed fd %d!\n", fd[1][WRITE_END]);

    if (ball < 0) {
      waitChildrenSuicide(NUM_PROC - 1 + ball);
      return;
    }

    printf("Relative pid=0, ball=%d\n", ball++);

    if (ball + 1 == check)
      ball = -1;

    write(fd[0][WRITE_END], &ball, sizeof(ball));
  }
}

int main(int argc, char *argv[]) {
  int channels[NUM_PROC][2];
  int i, guard, ball = 0;
  pid_t pid;

  scanf("%d", &guard);

  initPipes(channels);

  for (i = 1; i < NUM_PROC; i++) {
    pid = fork();
    if (pid == 0) {
      break;
    } else if (pid > 0) {
      continue;
    } else {
      perror("Forking!");
    }
  }

  if (pid > 0) {
    printf("Parent %d spawned all children (%d), starting parentAction!\n",
           getpid(), i);
    parentAction(channels, ball, guard);
  }

  if (pid == 0)
    childAction(i, channels, ball, guard);

  printf("Children are dead so parent %d returns :)\n", getpid());
  return 0;
}
