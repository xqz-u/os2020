#define _POSIX_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define PARENT_READ 0
#define CHILD_WRITE 1

// 1) test with one child process & killing child from parent
void testTwoProcess() {
  char *terminator = "Child is alive! Now kill me softly";
  char read_buff[100];

  int fd[2];
  pipe(fd);

  pid_t pid = fork();

  if (pid == 0) { // Child
    close(fd[PARENT_READ]);
    printf("Child pid %d, writing...\n", getpid());
    write(fd[CHILD_WRITE], terminator, strlen(terminator) + 1);
    close(fd[CHILD_WRITE]);
    printf("Now Child waits!\n");
    sleep(1);
    printf("Stopped sleeping?!\n"); // NEVER REACHED!!!
  } else if (pid > 0) {             // Parent
    int status;
    close(fd[CHILD_WRITE]);
    printf("Parent pid %d at start...\n", getpid());
    read(fd[PARENT_READ], read_buff, sizeof(read_buff));
    close(fd[PARENT_READ]);
    printf("Received message! \'%s\'\n", read_buff);

    if (!strcmp(read_buff, terminator))
      printf("... and it was what parent expected!\n");

    printf("Sending SIGTERM from parent!\n");
    kill(pid, SIGTERM);
    printf("\tSIGTERM sent to child\n");
    waitpid(pid, &status, 0);
    printf("Child %d was killed, status %d\n", pid, status);
  } else {
    perror("Forking");
  }
}

// 2) test with one child process & suicide of child when parent says so using
// signals
void child_sig_handler(int sig) {
  printf("\tChild %d received signal %d, can now terminate!\n", getpid(), sig);
  if (sig == SIGUSR1)
    exit(0);
  printf("\tI did not receive the expected signal %d so I will not suicide and "
         "be your worst zombie!\n",
         SIGUSR1);
}
void testSuicide_sig() {
  pid_t pid;
  int fd[2], status, ball = 0;
  int parent_msg;

  // Setup signal handler before forking s.t. signal
  // is not lost (actually child is terminated although
  // no SIGKILL is sent, when I thought it would hang]
  // in the while loop forever and idk why)
  /* signal(SIGUSR1, &child_sig_handler); // COMMENT THIS OUT AND SEE WHAT
   * HAPPENS */
  pipe(fd);

  pid = fork();
  if (pid == 0) {
    close(fd[PARENT_READ]);
    printf("\tThis is child %d, ball is %d, starting job...\n", getpid(), ball);
    ball += 10;
    write(fd[CHILD_WRITE], &ball, sizeof(ball));
    close(fd[CHILD_WRITE]);
    printf("\tJob executed, ball: %d, now I will wait & sleep for parent's "
           "signal\n",
           ball);
    while (1) {
      printf("\tStart sleeping!\n");
      sleep(5);
      printf("\tFinished sleeping!\n");
    }
  } else if (pid > 0) {
    close(fd[CHILD_WRITE]);
    printf("This is Parent %d, ball is %d, waiting for child message...\n",
           getpid(), ball);
    read(fd[PARENT_READ], &parent_msg, sizeof(parent_msg));
    close(fd[PARENT_READ]);
    printf("Received message from child %d, value is %d, ball is %d\n", pid,
           parent_msg, ball);
    printf("Sending termination signal to child!\n");
    kill(pid, SIGUSR1);
    waitpid(pid, &status, 0);
    printf("Child exited and status is %d. Parent exits!\n", status);
  } else {
    perror("Forking");
  }
}

// 3) spawn given processes, wait for each to execute their job on some shared
// work, make them exit when this is done and then make father terminate
void testMultiple() {}

int main(int argc, char *argv[]) {
  testTwoProcess();
  /* testSuicide_sig(); */
  /* testMultiple(); */
  printf("INSIDE MAIN, child killed, parent dies! And pid is indeed %d\n",
         getpid()); // COPIA QUESTA LINEA SOTTO OGNI TEST PRIMA DI ESEGUIRLO ED
                    // ESEGUI UN TEST ALLA VOLTA COMMENTANDO GLI ALTRI
  return 0;
}
