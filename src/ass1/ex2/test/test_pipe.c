#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define TEST 5

int first(int argc, char *argv[]) {
  pid_t pid;
  int ball = 0;
  int status;

  for (int i = 0; i < TEST; i++) {
    pid = fork();
    if (pid > 0) {
      printf("Parent! i: %d pid %d ball %d\n", i, pid, ball++);
      waitpid(pid, &status, 0);
    } else if (pid == 0) {
      printf("\tChild process i:%d pid %d ball %d\n", i, getpid(), ball++);
      return 0;
    } else {
      perror("forking");
    }
  }

  return 0;
}

int main(int argc, char *argv[]) {
  int pipefd[2];
  pid_t cpid;
  char buf;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s <string>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  if (pipe(pipefd) == -1) {
    perror("pipe");
    exit(EXIT_FAILURE);
  }

  cpid = fork();
  if (cpid == -1) {
    perror("fork");
    exit(EXIT_FAILURE);
  }

  if (cpid == 0) {    /* Child reads from pipe */
    close(pipefd[1]); /* Close unused write end */

    while (read(pipefd[0], &buf, 1) > 0)
      write(STDOUT_FILENO, &buf, 1);

    write(STDOUT_FILENO, "\n", 1);
    close(pipefd[0]);
    _exit(EXIT_SUCCESS);

  } else {            /* Parent writes argv[1] to pipe */
    close(pipefd[0]); /* Close unused read end */
    write(pipefd[1], argv[1], strlen(argv[1]));
    close(pipefd[1]); /* Reader will see EOF */
    wait(NULL);       /* Wait for child */
    exit(EXIT_SUCCESS);
  }
}
