#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define DEBUG 1
#define READ_END 0
#define WRITE_END 1
#define PARENT 0
#define CHILD 1
#define NUM_PROC 2

void printPipes(int fd[NUM_PROC][2]) {
  for (int i = 0; i < NUM_PROC; i++)
    printf("Pipe: %d -> %d :: READ %d WRITE %d\n", i, (i + 1) % NUM_PROC,
           fd[i][READ_END], fd[i][WRITE_END]);
  printf("\n");
}

void initPipes(int fd[NUM_PROC][2]) {
  for (int i = 0; i < NUM_PROC; i++) {
    if (pipe(fd[i]) == -1)
      perror("Piping!");
  }
#if DEBUG
  printPipes(fd);
#endif
}

int test1() {
  int channels[NUM_PROC][2], ball = 0, i, status, res;
  pid_t pid;

  initPipes(channels);

  for (i = 1; i < NUM_PROC; i++) {
    pid = fork();
    if (pid == 0) {
      break;
    } else if (pid > 0) {
      continue;
    } else {
      perror("Forking!");
    }
  }

  if (pid == 0) {
    printf("Child %d arrived!\n", getpid());
    // Parent needs to be able to write to child
    close(channels[PARENT][WRITE_END]);
    printf("child closed fd %d\n", channels[PARENT][WRITE_END]);
    /* close(channels[CHILD][READ_END]); NOTE useless! Same reasoning as in
     * the parent code */
    // Here, same reasoning as in the parent code
    while (1) {
      res = read(channels[PARENT][READ_END], &ball, sizeof(ball));
      if (res == -1) {
        perror("Error child reading");
        break;
      } else if (!res) {
        break;
      }
    }
    printf("Child %d, parent says ball=%d, received on fd %d!\n", getpid(),
           ball++, channels[PARENT][READ_END]);
    if (write(channels[CHILD][WRITE_END], &ball, sizeof(ball)) == -1) {
      printf("Error child writing on fd %d!\n", channels[CHILD][WRITE_END]);
      perror("Error is: ");
    }
    printf("Child wrote %d on fd %d!\n", ball, channels[CHILD][WRITE_END]);
    close(channels[CHILD][WRITE_END]);
    printf("child closed fd %d\n", channels[CHILD][WRITE_END]);
  }

  if (pid > 0) {
    printf("\tAll children spawned!\n");
    /* close(channels[PARENT][READ_END]); NOTE useless! Parent itself should
     * write on the write side of its pipe, and it's not doing it here
     * definitely! */
    // Child need to be able to write to parent
    close(channels[CHILD][WRITE_END]);
    printf("parent closed fd %d\n", channels[CHILD][WRITE_END]);
    if (write(channels[PARENT][WRITE_END], &ball, sizeof(ball)) == -1) {
      printf("Error child writing on fd %d!\n", channels[PARENT][WRITE_END]);
      perror("Error is: ");
    }
    printf("Parent wrote on fd %d\n", channels[PARENT][WRITE_END]);
    // Close writing pipe to send EOF or child will hang forever!
    close(channels[PARENT][WRITE_END]);
    printf("parent closed fd %d\n", channels[PARENT][WRITE_END]);
    printf("Parent %d sent ball=%d to child!...\n", getpid(), ball);
    printf("... and now waits for for child's msg!\n");
    // Child writes on its own write end, and pipe is passed to child's read
    // end!
    while (1) {
      res = read(channels[CHILD][READ_END], &ball, sizeof(ball));
      if (res == -1) {
        perror("Error parent reading");
        break;
      } else if (!res) {
        break;
      }
    }
    printf("Child replied with ball = %d on fd %d!\n", ball,
           channels[CHILD][READ_END]);
    printf("Child %d died, ", waitpid(pid, &status, 0));
    printf("status is %d\n", status);
  }

  return 0;
}

int test2() {
  setbuf(stdout, NULL);
  int channels[NUM_PROC][2], ball = 0, i, status, res;
  pid_t pid;
  int target = 10;

  initPipes(channels);

  for (i = 1; i < NUM_PROC; i++) {
    pid = fork();
    if (pid == 0) {
      break;
    } else if (pid > 0) {
      continue;
    } else {
      perror("Forking!");
    }
  }

  if (pid == 0) {
    while (1) {
      // Parent needs to be able to write to child
      close(channels[PARENT][WRITE_END]);
      close(channels[CHILD][WRITE_END]); // NOTE experiment
      // Here, same reasoning as in the parent code
      while (1) {
        res = read(channels[PARENT][READ_END], &ball, sizeof(ball));
        if (res == -1) {
          perror("Reading from child");
          return 0;
        } else if (!res) {
          break;
        }
      }
      printf("\t\tBall in child: %d\n", ball);

      if (ball == target) {
        printf("\tChild %d finished first!\n", getpid());
        ball = -1;
        break;
      } else if (ball < 0) {
        printf("\tParent reached target first, child %d dies!\n", getpid());
        return 0;
      }
      printf("\tChild %d, ball=%d\n", getpid(), ball);
      ball++;

      res = write(channels[CHILD][WRITE_END], &ball, sizeof(ball));
      if (res == -1) {
        /* printf("Writing from child, problem with fd %d\n", */
        /*        channels[CHILD][WRITE_END]); */
      } else if (res != sizeof(ball)) {
        printf("Error child writing! Wrote only %d bytes\n", res);
      }

      close(channels[CHILD][WRITE_END]);
      /* sleep(1); */
    }
    printf("Child %d dieing...\n", getpid());
    exit(0);
  }

  if (pid > 0) {
    while (1) {
      // Child need to be able to write to parent
      close(channels[CHILD][WRITE_END]);
      close(channels[PARENT][READ_END]); // NOTE experiment

      if (ball == target) {
        printf("Parent %d finished first!\n", getpid());
        ball = -1;
        break;
      } else if (ball < 0) {
        printf("Child died already, parent dies!\n");
        return 0;
      }
      printf("Parent %d, ball=%d!\n", getpid(), ball);
      ball++;

      res = write(channels[PARENT][WRITE_END], &ball, sizeof(ball));
      if (res == -1) {
        /* printf("Writing from parent, problem with fd %d\n", */
        /* channels[PARENT][WRITE_END]); */
      } else if (res != sizeof(ball)) {
        printf("Error parent writing, wrote only %d bytes\n", res);
      }

      // Close writing pipe to send EOF or child will hang forever!
      close(channels[PARENT][WRITE_END]);
      /* sleep(1); */

      while (1) {
        res = read(channels[CHILD][READ_END], &ball, sizeof(ball));
        if (res == -1) {
          perror("Reading from parent");
          return 0;
        } else if (!res) {
          break;
        }
      }
      printf("Parent reads %d\n", ball);
    }
    printf("Child %d died...", waitpid(pid, &status, 0));
    printf("... status is %d\n", status);
  }

  return 0;
}

int main() {
  /* test1(); */
  test2();
  return 0;
}
