#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// This struct holds a pipe to ease bidirectional communication
typedef struct Pipe {
  int r_end, w_end;
} Pipe;

// Struct that defines a process which can communicate through pipes
typedef struct Process {
  int parent;
  Pipe read, write;
  int gen;
  pid_t pid;
} Process;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

Pipe newPipe() {
  int fd[2];
  Pipe p;
  if (pipe(fd) == -1)
    perror("Piping!");
  p.r_end = fd[0];
  p.w_end = fd[1];
  return p;
}

Pipe *makePipes(int len) {
  Pipe *store = safeMalloc(len * sizeof(Pipe));
  for (int i = 0; i < len; i++)
    store[i] = newPipe();
  return store;
}

Process newProcess(pid_t proc_id, Pipe read_pipe, Pipe write_pipe,
                   int generation, int master) {
  Process proc;
  proc.read = read_pipe;
  proc.write = write_pipe;
  proc.gen = generation;
  proc.pid = proc_id;
  proc.parent = master;
  return proc;
}

// Each time a process has to read, it closes its write side of the pipe before
void readPipe(Pipe rp, int *value) {
  close(rp.w_end);
  if (read(rp.r_end, value, sizeof(int)) == -1) {
    printf("Reading from %d\n", rp.r_end);
    perror("Error");
    exit(EXIT_FAILURE);
  }
}

// Each time a process has to write, it closes its read side of the pipe before
void writePipe(Pipe wp, int *value) {
  close(wp.r_end);
  if ((write(wp.w_end, value, sizeof(int))) == -1) {
    printf("Writing to %d\n", wp.w_end);
    perror("Error");
    exit(EXIT_FAILURE);
  }
}

// This function prints the desired output, and it takes care of terminating the
// children if the target has been reached by passing a negative argument
// message regardless of the process being a parent or not
void ringTalk(Process proc, int target, int n_proc) {
  int count;
  while (1) {
    readPipe(proc.read, &count);
    if (count < 0 || count >= target) {
      if (count >= target) {
        printf("relative pid=%d: %d\n", proc.gen, count);
        fflush(stdout);
      }
      count = -1;
      writePipe(proc.write, &count);
      break;
    }
    printf("relative pid=%d: %d\n", proc.gen, count++);
    fflush(stdout);
    writePipe(proc.write, &count);
  }
  if (!proc.parent)
    return;
}

// This function just waits for children to die
void waitChildren(int tot) {
  int status, exit_pid;
  for (int i = 0; i < tot; i++) {
    exit_pid = wait(&status);
    if (!WIFEXITED(status))
      printf("Warning! child %d exited with code %d\n", exit_pid, status);
    if (WIFSIGNALED(status))
      printf("Warning! child %d was terminated by signal %d\n", exit_pid,
             WTERMSIG(status));
  }
}

int main(int argc, char *argv[]) {
  int n_proc, i;
  pid_t pid;
  Process proc;

  setbuf(stdout, NULL);

  scanf("%d", &n_proc);

  // Little hack for when there is only 1 process
  if (n_proc < 2) {
    int count = 0;
    while (count <= 50)
      printf("relative pid=0: %d\n", count++);
    return 0;
  }

  Pipe *pipe_store = makePipes(n_proc);

  for (i = 1; i < n_proc; i++) {
    pid = fork();
    if (pid == -1) {
      perror("Forking!");
      exit(EXIT_FAILURE);
    }
    if (!pid) {
      proc = newProcess(getpid(), pipe_store[i - 1], pipe_store[i], i,
                        0); // make the correct linking between child processes
      break;
    }
  }

  if (pid > 0) { // When process is parent...
    int count = 0;
    proc = newProcess(getpid(), pipe_store[n_proc - 1], pipe_store[0], 0, 1);
    printf("relative pid=%d: %d\n", proc.gen,
           count++); // The paarent starts the ring!
    writePipe(proc.write, &count);
  }

  ringTalk(proc, 50, n_proc);

  if (proc.parent)
    waitChildren(n_proc);

  free(pipe_store);
  return 0;
}
