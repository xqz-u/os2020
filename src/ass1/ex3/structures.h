#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <sys/types.h>

typedef struct FilesNode *Files;
typedef struct FilesNode {
  int size, tot;
  char **files_paths;
  __off_t bytes;
  Files next;
} FilesNode;

void *safeMalloc(int sz);
void freeStrings(char *str_arr[], int len);
Files newFiles(int len, __off_t file_size);
void freeFiles(Files f);
void resizeFilesEntries(Files f);
void printFiles(Files f);
void append(char *s, Files f);
void insert(Files *head, char *file_path, __off_t file_size);

#endif
