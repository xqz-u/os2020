#define _GNU_SOURCE
#include <assert.h>
#include <fts.h>
#include <linux/limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

// Error handling
void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  if (vfprintf(err_dest, cformat, err_args) <= 0)
    fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throw_error(fd, formatter, ...)                                        \
  errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

typedef struct FilesNode *Files;
typedef struct FilesNode {
  int size, tot;
  char **files_paths;
  __off_t bytes;
  Files next;
} FilesNode;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

void freeStrings(char *str_arr[], int len) {
  for (int i = 0; i < len; i++) {
    free(str_arr[i]);
  }
  free(str_arr);
}

Files newFiles(int len, __off_t file_size) {
  Files f = safeMalloc(sizeof(FilesNode));
  assert(f != NULL);
  f->size = len;
  f->tot = 0;
  f->bytes = file_size;
  f->files_paths = safeMalloc(f->size * sizeof(char *));
  assert(f->files_paths != NULL);
  f->next = NULL;
  return f;
}

void freeFiles(Files f) {
  if (f == NULL) {
    return;
  }
  freeFiles(f->next);
  freeStrings(f->files_paths, f->tot);
  free(f);
}

void resizeFilesEntries(Files f) {
  f->size *= 2;
  f->files_paths = realloc(f->files_paths, f->size * sizeof(char *));
  assert(f->files_paths != NULL);
}

void append(char *s, Files f) {
  if (f->tot == f->size) {
    resizeFilesEntries(f);
  }
  if (s == NULL) {
    f->files_paths[f->tot++] = NULL;
    return;
  }
  f->files_paths[f->tot++] = strdup(s);
}

void insert(Files *head, char *file_path, __off_t file_size) {
  if (*head == NULL) {
    *head = newFiles(1, file_size);
    append(file_path, *head);
    return;
  }
  Files last, tmp = *head;
  while (tmp->next != NULL && tmp->bytes != file_size) {
    tmp = tmp->next;
  }
  if (tmp->next == NULL && tmp->bytes != file_size) {
    last = newFiles(1, file_size);
    append(file_path, last);
    tmp->next = last;
    last->next = NULL;
    return;
  }
  append(file_path, tmp);
}

char *readInput() {
  int input_len = MAX_INPUT;
  char *input = safeMalloc(input_len);
  int i = 0;
  char c = fgetc(stdin);

  while (c != EOF && c != '\n') {
    input[i++] = c;
    if (i >= input_len) {
      input_len *= 2;
      input = realloc(input, input_len);
    }
    c = fgetc(stdin);
  }
  input[i] = 0; // Add null terminator

  return input;
}

char *substr(int start, int end, char *og) {
  char *ret = safeMalloc(end - start + 1);
  for (int i = start; i < end; i++)
    ret[i - start] = og[i];
  ret[end - start] = 0;
  return ret;
}

char *relativePath(char *current_folder, char *root_folder) {
  char *rel, *tmp;
  tmp = substr(strlen(root_folder), strlen(current_folder), current_folder);
  asprintf(&rel, ".%s", tmp);
  free(tmp);
  return rel;
}

int alphabetical_compare(const FTSENT **node_a, const FTSENT **node_b) {
  return strcmp((*node_a)->fts_name, (*node_b)->fts_name);
}

void LLfiles(char *fs[], Files *head) {
  FTS *file_system;
  FTSENT *fs_node;
  mode_t file_type;

  if ((file_system = fts_open(fs, FTS_LOGICAL, alphabetical_compare)) == NULL) {
    throw_error(stderr, "Opening the given fs");
  }

  while ((fs_node = fts_read(file_system)) != NULL) {
    file_type = fs_node->fts_statp->st_mode;
    if (S_ISREG(file_type)) {
      insert(head, fs_node->fts_path, fs_node->fts_statp->st_size);
    }
  }

  if (fts_close(file_system) == -1) {
    throw_error(stderr, "Closing the filesystem");
  }
}

int compareTwoFiles(char *filename1, char *filename2) {
  FILE *fp1 = fopen(filename1, "r");
  FILE *fp2 = fopen(filename2, "r");
  char ch1, ch2;
  int equal = 1;

  if (fp1 == NULL || fp2 == NULL) {
    throw_error(stderr, "Error: could not open file %s or %s\n", filename1,
                filename2);
    fclose(fp1);
    fclose(fp2);
    return -1;
  }

  do {
    ch1 = fgetc(fp1);
    ch2 = fgetc(fp2);
    if (ch1 != ch2) {
      equal = 0;
      break;
    }
  } while (ch1 != EOF && ch2 != EOF);

  if (equal) {
    equal = (ch1 == EOF && ch2 == EOF) ? 1 : 0;
  }

  fclose(fp1);
  fclose(fp2);

  return equal;
}

void compareFileGroup(char *files[], int tot, char *root) {
  int i, j;
  char *rel_path1, *rel_path2;
  for (i = 0; i < tot; i++) {
    for (j = i + 1; j < tot; j++) {
      if (compareTwoFiles(files[i], files[j])) {
        rel_path1 = relativePath(files[i], root);
        rel_path2 = relativePath(files[j], root);
        printf("\"%s\" \"%s\"\n", rel_path1, rel_path2);
        free(rel_path1);
        free(rel_path2);
      }
    }
  }
}

void findSameFiles(Files *head, char *root_dir) {
  if (*head == NULL) {
    throw_error(stderr, "The given linked list is empty!\n");
    exit(EXIT_FAILURE);
  }
  Files tmp = *head;
  while (tmp != NULL) {
    if (tmp->tot > 1) {
      compareFileGroup(tmp->files_paths, tmp->tot, root_dir);
    }
    tmp = tmp->next;
  }
}

int main(int argc, char *argv[]) {
  char *input_string = readInput();
  char *input_fs[] = {input_string, NULL};
  Files files_list = NULL;

  LLfiles(input_fs, &files_list);
  findSameFiles(&files_list, input_string);

  free(input_string);
  freeFiles(files_list);
  return 0;
}
