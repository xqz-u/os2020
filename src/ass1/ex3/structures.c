#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./structures.h"

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

void freeStrings(char *str_arr[], int len) {
  for (int i = 0; i < len; i++) {
    free(str_arr[i]);
  }
  free(str_arr);
}

Files newFiles(int len, __off_t file_size) {
  Files f = safeMalloc(sizeof(FilesNode));
  assert(f != NULL);
  f->size = len;
  f->tot = 0;
  f->bytes = file_size;
  f->files_paths = safeMalloc(f->size * sizeof(char *));
  assert(f->files_paths != NULL);
  f->next = NULL;
  return f;
}

void freeFiles(Files f) {
  if (f == NULL) {
    return;
  }
  freeFiles(f->next);
  freeStrings(f->files_paths, f->tot);
  free(f);
}

void resizeFilesEntries(Files f) {
  f->size *= 2;
  f->files_paths = realloc(f->files_paths, f->size * sizeof(char *));
  assert(f->files_paths != NULL);
}

void printFiles(Files f) {
  while (f != NULL) {
    printf("%d files of size %lub:\n", f->tot, f->bytes);
    for (int i = 0; i < f->tot; i++) {
      printf("\tidx %d: %s\n", i, f->files_paths[i]);
    }
    f = f->next;
  }
}

void append(char *s, Files f) {
  if (f->tot == f->size) {
    resizeFilesEntries(f);
  }
  if (s == NULL) {
    f->files_paths[f->tot++] = NULL;
    return;
  }
  f->files_paths[f->tot++] = strdup(s);
}

void insert(Files *head, char *file_path, __off_t file_size) {
  if (*head == NULL) {
    *head = newFiles(1, file_size);
    append(file_path, *head);
    return;
  }
  Files last, tmp = *head;
  while (tmp->next != NULL && tmp->bytes != file_size) {
    tmp = tmp->next;
  }
  if (tmp->next == NULL && tmp->bytes != file_size) {
    last = newFiles(1, file_size);
    append(file_path, last);
    tmp->next = last;
    last->next = NULL;
    return;
  }
  append(file_path, tmp);
}

void test1() {
  Files flist = newFiles(1, 100);

  append("Questa e la prima stringa", flist);
  append("Questa invece la seconda dopo resize!", flist);
  append("Questa invece la terza dopo resize!", flist);
  append("Questa invece la quarta dopo resize!", flist);
  append("Questa invece la quinta dopo resize!", flist);

  printFiles(flist);

  freeFiles(flist);
}

void test2() {
  Files f_sizes = NULL;
  insert(&f_sizes, "Prima stringa", 100);
  insert(&f_sizes, "seconda stringa", 150);
  insert(&f_sizes, "terza stringa", 100);
  for (int i = 0; i < 300; i += 50) {
    insert(&f_sizes, "Stringa di prova", i);
  }

  printFiles(f_sizes);
  freeFiles(f_sizes);
}

int __main(void) {
  /* test1(); */
  /* test2(); */
  return 0;
}
