#define _GNU_SOURCE
#include <fts.h>
#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "./debug.h"
#include "./structures.h"

char *readInput() {
  int input_len = MAX_INPUT;
  char *input = safeMalloc(input_len);
  int i = 0;
  char c = fgetc(stdin);

  while (c != EOF && c != '\n') {
    input[i++] = c;
    if (i >= input_len) {
      input_len *= 2;
      input = realloc(input, input_len);
    }
    c = fgetc(stdin);
  }
  input[i] = 0; // Add null terminator

  return input;
}

int alphabetical_compare(const FTSENT **node_a, const FTSENT **node_b) {
  return strcmp((*node_a)->fts_name, (*node_b)->fts_name);
}

char *substr(int start, int end, char *og) {
  char *ret = safeMalloc(end - start + 1);
  for (int i = start; i < end; i++)
    ret[i - start] = og[i];
  ret[end - start] = 0;
  return ret;
}

char *relativePath(char *current_folder, char *root_folder) {
  char *rel, *tmp;
  tmp = substr(strlen(root_folder), strlen(current_folder), current_folder);
  asprintf(&rel, ".%s", tmp);
  free(tmp);
  return rel;
}

void LLfiles(char *fs[], Files *head) {
  FTS *file_system;
  FTSENT *fs_node;
  mode_t file_type;

  if ((file_system = fts_open(fs, FTS_LOGICAL, &alphabetical_compare)) ==
      NULL) {
    throw_error(stderr, "Opening the given fs");
  }

  while ((fs_node = fts_read(file_system)) != NULL) {
    file_type = fs_node->fts_statp->st_mode;
    if (S_ISREG(file_type)) {
      insert(head, fs_node->fts_path, fs_node->fts_statp->st_size);
    }
  }

  if (fts_close(file_system) == -1) {
    throw_error(stderr, "Closing the filesystem");
  }
}

// TODO try using fread() and/or memcmp()
int compareTwoFiles(char *filename1, char *filename2) {
  FILE *fp1 = fopen(filename1, "r");
  FILE *fp2 = fopen(filename2, "r");
  char ch1, ch2;
  int equal = 1;

  if (fp1 == NULL || fp2 == NULL) {
    throw_error(stderr, "Error: could not open file %s or %s\n", filename1,
                filename2);
    fclose(fp1);
    fclose(fp2);
    return -1;
  }

  do {
    ch1 = fgetc(fp1);
    ch2 = fgetc(fp2);
    if (ch1 != ch2) {
      equal = 0;
      break;
    }
  } while (ch1 != EOF && ch2 != EOF);

  if (equal) {
    equal = (ch1 == EOF && ch2 == EOF) ? 1 : 0;
  }

  fclose(fp1);
  fclose(fp2);

  return equal;
}

// TODO recursive function
// NOTE small bug: when given a path like "./", equal files in that
// folder are printed as ".file1" ".file2" (fix with strchr looking for
// "/")
void compareFileGroup(char *files[], int tot, char *root) {
  int i, j;
  char *rel_path1, *rel_path2;
  for (i = 0; i < tot; i++) {
    for (j = i + 1; j < tot; j++) {
      if (compareTwoFiles(files[i], files[j])) {
        rel_path1 = relativePath(files[i], root);
        rel_path2 = relativePath(files[j], root);
        printf("\"%s\" \"%s\"\n", rel_path1, rel_path2);
        free(rel_path1);
        free(rel_path2);
      }
    }
  }
}

void findSameFiles(Files *head, char *root_dir) {
  if (*head == NULL) {
    throw_error(stderr, "The given linked list is empty!\n");
    exit(EXIT_FAILURE);
  }
  Files tmp = *head;
  while (tmp->next != NULL) {
    if (tmp->tot > 1) {
      compareFileGroup(tmp->files_paths, tmp->tot, root_dir);
    }
    tmp = tmp->next;
  }
}

int main(int argc, char *argv[]) {
  char *input_string = readInput();
  char *input_fs[] = {input_string, NULL};
  Files files_list = NULL;

  LLfiles(input_fs, &files_list);
  findSameFiles(&files_list, input_string);

  free(input_string);
  freeFiles(files_list);
  return 0;
}
