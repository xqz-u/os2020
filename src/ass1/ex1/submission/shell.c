#define _GNU_SOURCE // For asprintf() and strdup()
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define DEBUG 0
#define MAX_COMMAND_LEN 1024
#define MAX_ARGS_SIZE 20

// Error handling
void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  if (vfprintf(err_dest, cformat, err_args) <= 0)
    fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throw_error(fd, formatter, ...)                                        \
  errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

// Stack-like data structure used primarily to manipulate strings.
// This ds should be accessed with the two methods "append" and "retrieve",
// which respectively add a copy of a string to the list and return
// the original string stored in the list at some index; both methods perform
// safety checks before accessing the resource.
typedef struct List {
  char **entries;
  int size, tot;
} List;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

List makeList(int len) {
  List st;
  st.size = len;
  st.tot = 0;
  st.entries = safeMalloc(len * sizeof(char *));
  return st;
}

void freeList(List st) {
  for (int i = 0; i < st.tot; i++)
    free(st.entries[i]);
  free(st.entries);
}

void resizeList(List *lsp) {
  lsp->size *= 2;
  if ((lsp->entries = realloc(lsp->entries, lsp->size * sizeof(char *))) ==
      NULL)
    throw_error(stderr, "Failed memory reallocation of size %d!", lsp->size);
}

void append(char *s, List *lsp) {
  if (lsp->tot == lsp->size)
    resizeList(lsp);

  if (s == NULL) {
    lsp->entries[lsp->tot++] = NULL;
    return;
  }

  if ((lsp->entries[lsp->tot++] = strdup(s)) == NULL)
    throw_error(stderr, "Failed copy of string \"%s\"\n", s);
}

char *retrieve(List ls, int idx) {
  if (idx > ls.tot) {
    throw_error(stderr, "idx too high! have %d, current tot is %d", idx,
                ls.tot);
#if DEBUG
    exit(exit_FAILURE);
#endif
    return NULL;
  }
  return ls.entries[idx];
}

// Return a copy of the substring with length (end-start) from one original
// string
char *substr(int start, int end, char *og) {
  char *ret = safeMalloc(end - start + 1);
  for (int i = start; i < end; i++)
    ret[i - start] = og[i];
  ret[end - start] = 0;
  return ret;
}

// Returns a List of strings which is the separation of *s on the sep single
// character separator
List enlist(char *s, char sep) {
  List ret = makeList(15); // Arbitrary size, final one is unknown
  char *tmp;
  int start, i;

  start = 0;
  for (i = 0; i <= strlen(s); i++) {
    if (s[i] == sep || i == strlen(s)) {
      tmp = substr(start, i, s);
      append(tmp, &ret);
      start = i + 1;
      free(tmp);
    }
  }

  return ret;
}

// Concatenates the string stored at &s with the suffix *s
void addSuffix(char *suff, char **s) {
  *s = realloc(*s, strlen(*s) + strlen(suff) + 1);
  if (strcat(*s, suff) == NULL)
    throw_error(stderr, "Couldn't add suffix \"%s\" to \"%s\"\n", suff, *s);
}

// Creates the absolute path for the command line program
void pathToProgram(List *lp, char *prog) {
  // Create cmd in the form "/cmd"
  char *cmd;
  if (asprintf(&cmd, "/%s", prog) == -1)
    throw_error(stderr, "Error! Failure in asprintf() with string \"/%s\"",
                prog);

  // Add "/cmd" suffix to paths to get complete cmd path
  for (int i = 0; i < lp->tot; i++)
    addSuffix(cmd, &(lp->entries[i]));

  free(cmd);
}

// Enable optional quoting at start and end of string
void enableQuotes(char *s) {
  char double_q = '\"';
  char single_q = '\'';
  char *first = &s[0];
  char *last = &s[strlen(s) - 1];

  // Substitute with spaces since it is what we separate on
  if ((*first == double_q && *last == double_q) ||
      (*first == single_q && *last == single_q)) {
    *first = ' ';
    *last = ' ';
  }
}

// Create arguments to pass to execve()
List getInput() {
  int input_len = MAX_COMMAND_LEN;
  char *tok, *input = safeMalloc(input_len);
  List args = makeList(MAX_ARGS_SIZE);
  int i = 0;
  char c = fgetc(stdin);

  while (c != EOF && c != '\n') {
    input[i++] = c;
    if (i >= input_len) {
      input_len *= 2;
      input = realloc(input, input_len);
    }
    c = fgetc(stdin);
  }
  input[i] = 0; // Add null terminator

  enableQuotes(input);
  for (tok = strtok(input, " "); tok != NULL; tok = strtok(NULL, " "))
    append(tok, &args);

  free(input);
  return args;
}

// This function tries to execute the command line input program by resolving
// it with the user's standard path. If this failed, it returns 0
int execute(List args, List paths) {
  char *envp = {NULL}; // No key=value pair is passed here to execve()
  char *complete_cmd;
  int i = 0;

  while ((complete_cmd = retrieve(paths, i++)) != NULL)
    execve(complete_cmd, args.entries, &envp);

  return 0; // If this portion of code is reached then execve could not find
            // the program
}

int launch(List args, List paths) {
  int status, pid;

  // Fork this process
  pid = fork();

  if (pid > 0) { // Parent code
    waitpid(pid, &status, 0);
    return 1;
  } else if (pid == 0) { // Child code
    if (!execute(args, paths)) {
      return 0;
    }
  }

  return -1; // Error in forking
}

int main(int argc, char *argv[]) {

  // Parse input
  List args = getInput();

  char *filename; // Copy command name since it will be modified later
  if ((filename = retrieve(args, 0)) == NULL) {
    freeList(args);
    throw_error(stderr, "Error! Give a non void input!");
    exit(EXIT_FAILURE);
  }
  filename = strdup(filename);

  // Create search environment
  List paths = enlist(getenv("PATH"), ':');

  // Add input program to the search environment as a suffix,
  // modifying the initial list
  pathToProgram(&paths, filename);

  // Fork process and execute programs
  int exit_code = launch(args, paths);
  if (!exit_code)
    printf("Command %s not found!\n", filename);
  else if (exit_code == -1) {
    throw_error(stderr, "Failure in fork(), couldn\'t create child process!");
    exit(EXIT_FAILURE);
  }

  free(filename);
  freeList(args);
  freeList(paths);
  return 0;
}
