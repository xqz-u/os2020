#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

// Error handling
void errorsFrom(FILE *err_dest, int lineno, char const *callerFunc,
                char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  if (vfprintf(err_dest, cformat, err_args) <= 0)
    fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throw_error(fd, formatter, ...)                                        \
  errorsFrom(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

#define MAX_STRING_LEN 500
#define MAX_ARGS_SIZE 20

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

void freeStrings(char **store, int size) {
  for (int i = 0; i < size; i++)
    free(store[i]);
  free(store);
}

// NOTE not needed for ass
void printStrings(char **store, int size) {
  for (int i = 0; i < size; i++)
    printf("%d: %s\n", i, store[i]);
}

char **makeStrings(int size) { return safeMalloc(size * sizeof(char *)); }

// Double size of array after comparison against a value
int updateSize(int *size, int check) {
  if (check >= *size) {
    *size *= 2;
    return 1;
  }
  return 0;
}

// Resize string array to new_size
void resizeStrings(char ***store, int new_size) {
  if ((*store = realloc(*store, new_size * sizeof(char *))) == NULL)
    throw_error(stderr, "Failed memory allocation when resizing strings arr %p",
                store);
}

// Enable optional quoting at start and end of string
void enableQuote(char *s) {
  if ((s[0] == '\"') && (s[strlen(s) - 2] == '\"')) {
    s[0] = ' ';
    s[strlen(s) - 2] = ' ';
  }
}

// Remove trailing newline if needed
void removeNewline(char *s) {
  char *ret;
  if ((ret = strchr(s, '\n')))
    *ret = 0;
}

// Allocate space for string dest and copy string s here
int copyString(char **dest, char *s) {
  *dest = safeMalloc(strlen(s) + 1);
  if (strcpy(*dest, s) == NULL) {
    throw_error(stderr, "Error! Fail copy of %s", s);
    return 0;
  }
  return 1;
}

// Insert a string at idx in an array of strings
int insertString(char ***store, char *s, int idx) {
  if (s == NULL) {
    (*store)[idx] = NULL;
    return 1;
  }
  if (!copyString(&((*store)[idx]), s)) {
    throw_error(stderr, "Error! Failed insertion of %s", s);
    return 0;
  }
  return 1;
}

void getInput(char ***store, int *store_size, int *entries) {
  char input[MAX_STRING_LEN];
  char *tok;
  *entries = 0;

  // NOTE if since we have only one line, change to while for submission (?)
  if (fgets(input, MAX_STRING_LEN, stdin) != NULL) {
    enableQuote(input);
    tok = strtok(input, " ");
    removeNewline(tok);
    insertString(store, tok, (*entries)++);
    while ((tok = strtok(NULL, " ")) != NULL) {
      if (updateSize(store_size, *entries))
        resizeStrings(store, *store_size);
      insertString(store, tok, *entries);
      (*entries)++;
    }
    removeNewline((*store)[*entries - 1]); // remove trailing newline char
  }
}

// Return a copy of the substring with length (end-start) from one original
// string
char *substr(int start, int end, char *og) {
  char *ret = safeMalloc(end - start + 1);
  for (int i = start; i < end; i++)
    ret[i - start] = og[i];
  ret[end - start] = 0;
  return ret;
}

// Returns a list of strings which is the separation of *s on the sep single
// character separator
void enlist(char ***store, int *store_size, int *entries, char *s, char sep) {
  char *tmp;
  int start, i;
  *entries = 0;

  start = 0;
  for (i = 0; i <= strlen(s); i++) {
    if (s[i] == sep || i == strlen(s)) {
      tmp = substr(start, i, s);
      if (updateSize(store_size, *entries))
        resizeStrings(store, *store_size);
      insertString(store, tmp, (*entries)++);
      start = i + 1;
      free(tmp);
    }
  }
}

// Concatenates the string stored at &s with the suffix *s
void addSuffix(char **og, char *suff) {
  *og = realloc(*og, strlen(*og) + strlen(suff) + 1);
  if (strcat(*og, suff) == NULL)
    throw_error(stderr, "Couldn't suff suffix \"%s\" to \"%s\"\n", suff, *og);
}

// Creates the absolute path for the command line program
void pathToProgram(char ***store, int entries, char *filename) {
  // Create cmd in the form "/cmd"
  char *cmd = safeMalloc(strlen("/") + strlen(filename) + 1);
  strcat(strcpy(cmd, "/"), filename);

  // Add "/cmd" suffix to paths to get complete cmd path
  for (int i = 0; i < entries; i++)
    addSuffix(&((*store)[i]), cmd);
  free(cmd);
}

// This function tries to execute the command line input program by resolving it
// with the user's standard path. If this failed, it returns 0
int execute(char ***args, int args_entries, char **paths, int paths_entries) {
  char *envp[] = {NULL}; // No key=value pair is passed here to execve()

  for (int i = 0; i < paths_entries; i++) {
    free((*args)[0]);
    insertString(args, paths[i], 0);
    printf("Before execve!\n");
    printStrings(*args, args_entries);
    execve((*args)[0], *args, envp);
  }

  return 0; // If this portion of code is reached then execve could not find the
            // program
}

int launch(char ***args, int *args_size, int *args_entries, char **paths,
           int paths_entries) {
  pid_t pid;
  int status;

  // args to execve() are null terminated
  if (updateSize(args_size, *args_entries))
    resizeStrings(args, *args_size);
  insertString(args, NULL, ++(*args_entries));

  // Fork this process
  pid = fork();

  if (pid > 0) { // Parent code
    waitpid(pid, &status, 0);
    return 1;
  } else if (pid == 0) { // Child code
    if (!execute(args, *args_entries, paths, paths_entries)) {
      return 0;
    }
  }

  return -1; // Error in forking
}

int main(int argc, char *argv[]) {
  int args_size = MAX_ARGS_SIZE;
  int args_num; // To store total number of arguments yet unknown
  char **args = makeStrings(args_size);

  getInput(&args, &args_size, &args_num); // Parse input
  printStrings(args, args_num);

  char *filename = NULL;
  copyString(&filename, args[0]);

  // Create search environment
  int paths_size = MAX_ARGS_SIZE;
  int paths_num; // To store total number of search paths yet unknown
  char **paths = makeStrings(paths_size);
  enlist(&paths, &paths_size, &paths_num, getenv("PATH"), ':');

  // Add input program to the search environment as a suffix,
  // modifying the initial list
  pathToProgram(&paths, paths_num, args[0]);

  // Fork process and execute programs
  int exit_code = launch(&args, &args_size, &args_num, paths, paths_num);
  if (!exit_code)
    printf("Command %s not found!\n", filename);
  else if (exit_code == -1) {
    throw_error(stderr, "Failure in fork(), couldn\'t create child process!");
    exit(EXIT_FAILURE);
  }

  freeStrings(paths, paths_num);
  freeStrings(args, args_num);
  return 0;
}
