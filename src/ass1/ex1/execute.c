#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define DEBUG 0

/* Error handling */
void errors_from(FILE *err_dest, int lineno, char const *callerFunc,
                 char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  if (vfprintf(err_dest, cformat, err_args) <= 0)
    fprintf(err_dest, "Error! Couldn\'t print variadic list\n");
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throwError(fd, formatter, ...)                                         \
  errors_from(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

/* Input debug */
void printer(int msg_cnt, char *msg_list[]) {
  fprintf(stderr, "%d args\n", msg_cnt);
  for (int i = 0; i < msg_cnt; i++)
    fprintf(stderr, "%d: %s\n", i, msg_list[i]);
}
#define console_stdin(argc, argv) printer(argc, argv)

typedef struct List {
  char **entries;
  int size, tot;
} List;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

List makeList(int len) {
  List st;
  st.size = len;
  st.tot = 0;
  st.entries = safeMalloc(len * sizeof(char *));
  return st;
}

void freeList(List st) {
  for (int i = 0; i < st.tot; i++)
    free(st.entries[i]);
  free(st.entries);
}

void resizeList(List *lsp) {
  lsp->size *= 2;
  if ((lsp->entries = realloc(lsp->entries, lsp->size * sizeof(char *))) ==
      NULL)
    throwError(stderr, "Failed memory allocation!");
}

// NOTE not needed for ass1
int isEmptyList(List lsp) { return (lsp.tot == 0); }

void append(char *s, List *lsp) {
  if (lsp->tot == lsp->size)
    resizeList(lsp);

  if ((lsp->entries[lsp->tot++] = strdup(s)) == NULL)
    throwError(stderr, "Failed push of string \"%s\", have \"%s\"\n", s,
               lsp->entries[lsp->tot]);
}

char *retrieve(List ls, int idx) {
  if (idx > ls.tot - 1) {
#if DEBUG
    throwError(stderr, "idx too high! have %d, current tot is %d - 1\n", idx,
               ls.tot);
    exit(exit_FAILURE);
#endif
    return NULL;
  }
  return ls.entries[idx]; // How to return a copy which cannot be freed if
                          // execve does not return?!
}

// NOTE not needed for ass1
void printList(List ls) {
  if (isEmptyList(ls))
    throwError(stderr, "Printing empty list!");
  for (int i = 0; i < ls.tot; i++)
    printf("%d: %s\n", i, ls.entries[i]);
}

char *substr(int start, int end, char *og) {
  char *ret = safeMalloc(end - start + 1);
  for (int i = start; i < end; i++)
    ret[i - start] = og[i];
  ret[end - start] = '\0';
  return ret;
}

List enlist(char *s, char sep) {
  List ret = makeList(15);
  char *tmp;
  int start, i;

  start = i = 0;
  for (i = 0; i <= strlen(s); i++) {
    if (s[i] == sep || i == strlen(s)) {
      tmp = substr(start, i, s);
      append(tmp, &ret);
      start = i + 1;
      free(tmp);
    }
  }

  return ret;
}

void add_suffix(char *suff, char **s) {
  *s = realloc(*s, strlen(*s) + strlen(suff) + 1);
  if (strcat(*s, suff) == NULL)
    throwError(stderr, "Couldn't add suffix \"%s\" to \"%s\"\n", suff, *s);
}

void pathToProgram(List *lp, char *prog) {
  // Create cmd in the form "/cmd"
  char *cmd = safeMalloc(strlen("/") + strlen(prog) + 1);
  strcat(strcpy(cmd, "/"), prog);

  // Add "/cmd" suffix to paths to get complete cmd path
  for (int i = 0; i < lp->tot; i++)
    add_suffix(cmd, &(lp->entries[i]));

  free(cmd);
}

void getInput(char *args[], int argc, char *og_args[]) {
  for (int i = 0; i < argc - 1; i++)
    args[i] = og_args[i + 1];
  args[argc - 1] = NULL;
}

// NOTE not needed for ass1
void printTable(char **tab, int tablen) {
  for (int i = 0; i < tablen; i++)
    printf("%s\n", tab[i]);
  printf("\n");
}

int execute(char *args[], List paths) {
  char *envp[] = {NULL};

  int i = 0;
  while ((args[0] = retrieve(paths, i)) != NULL) {
    execve(args[0], args, envp);
    i++;
  }

  return 0; // If this portion of code is reached then execve could not find the
            // program
}

int launch(char *args[], List paths) {
  int status, pid;

  // Fork this process
  pid = fork();

  if (pid > 0) { // Parent code
    waitpid(pid, &status, 0);
    return 1;
  } else if (pid == 0) { // Child code
    if (!execute(args, paths)) {
      return 0;
    }
  }
  // Error in forking
  return -1;
}

int main(int argc, char *argv[]) {
  console_stdin(argc, argv);

  // Print use instructions if needed
  if (argc < 2) {
    throwError(stderr, "Usage: run as %s COMMAND ARG1 ARG2 ... ARGn", argv[0]);
    exit(EXIT_FAILURE);
  }

  // Create search environment
  List paths = enlist(getenv("PATH"), ':');

  // Add input program to the search environment as a suffix,
  // modifying the initial list
  pathToProgram(&paths, argv[1]);

  // Copy input program arguments to the args for execve
  char *args[argc];
  getInput(args, argc, argv);

  // Fork process and execute programs
  int exit_code = launch(args, paths);
  if (!exit_code)
    printf("Command %s not found!\n", argv[1]);
  else if (exit_code == -1) {
    throwError(stderr, "Failure in fork(), couldn\'t create child process!");
    exit(EXIT_FAILURE);
  }

  freeList(paths);
  return 0;
}

/* int main(int argc, char *argv[]) { */
/*   char *input = NULL; */
/*   if (fgets(input, MAX_INPUT, stdin) == NULL) */
/*     throwError(stderr, "Error in reading input line!"); */

/*   printf("%s\n", input); */

/*   return 0; */
/* } */
