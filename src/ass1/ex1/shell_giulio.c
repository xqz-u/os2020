#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

void *safeMalloc(int n) {
  void *p = calloc(1, n);
  if (p == NULL) {
    printf("Error: malloc(%d) failed. Out of memory?\n", n);
    exit(EXIT_FAILURE);
  }
  return p;
}

int findNumTokens(char *str, char c) {
  int i = 0, ntoks = 1;
  while (str[i] != '\0') {
    if (str[i] == c) {
      ntoks++;
    }
    i++;
  }
  return ntoks;
}

void findTokens(int ntoks, char *str, char *toks[][ntoks], char *delim) {
  int i = 0;
  for (char *tok = strtok(str, delim); tok != NULL; strtok(NULL, delim))
    toks[i++] = strdup(tok); // IMPORTANTE non fare toks[i] = strtok() ma crea
                             // una copia allocata di tok!
}

void readInput(char **str) {
  int size = 40;
  *str = safeMalloc(size * sizeof(char));
  int c = fgetc(stdin);
  int i = 0;
  while (c != '\n' && c != EOF) {
    (*str)[i++] = c;
    if (i >= size) {
      size *= 2;
      *str = realloc(*str, size * sizeof(char));
    }
    c = fgetc(stdin);
  }
  (*str)[i] = 0;
}

int main(int argc, char *argv[]) {
  int i;

  // read and print string
  char *str;
  readInput(&str);

  // find command and arguments as tokens
  int ntoks = findNumTokens(str, ' ');
  char *toks[ntoks + 1];
  findTokens(ntoks, str, &toks, " ");
  toks[ntoks] = NULL;

  // create string representing command
  char *cmd = toks[0];

  // array of standard search paths
  char *path = getenv("PATH");
  int npaths = findNumTokens(path, ':');
  char *paths[npaths];
  findTokens(npaths, path, paths, ":");

  // create child process
  int status;
  int pid = fork();
  if (pid == 0) {
    // child code
    int count = 0;
    for (i = 0; i < npaths; i++) {
      char *newPath = safeMalloc((strlen(paths[i]) + strlen(cmd) + 2));
      newPath = strcpy(newPath, paths[i]);
      newPath = strcat(newPath, "/");
      newPath = strcat(newPath, cmd);
      newPath = strcat(newPath, "\0");
      if (execve(newPath, toks, 0) == -1) {
        count++;
      }
      free(newPath);
    }

    if (count == npaths) {
      printf("Command %s not found!\n", cmd);
    }
  } else {
    // parent code
    waitpid(-1, &status, 0);
  }

  for (i = 0; i < ntoks; i++)
    free(toks[i]);

  free(str);
  return 0;
}
