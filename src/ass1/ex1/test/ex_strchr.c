#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  char *example = "This:is:a:colon:separated:string";
  char *ret = "0";
  char *targ = ":";

  printf("%lu\n", strlen(example));
  printf("%d\n", *targ);
  ret = strchr(example, (int)*targ);
  printf("%s %c\n", ret, (char)(int)*ret);

  return 0;
}
