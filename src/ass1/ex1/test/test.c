#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

/* Debug */
void printer(int msg_cnt, char *msg_list[]) {
  for (int i = 0; i < msg_cnt; i++)
    printf("%d: %s\n", i, msg_list[i]);
}
#define console_stdin(argc, argv) printer(argc, argv)

void errors_from(FILE *err_dest, int lineno, char const *callerFunc,
                 char *cformat, ...) {
  fprintf(err_dest, "%s:%d:%s::", __FILE__, lineno, callerFunc);
  va_list err_args;
  va_start(err_args, cformat);
  int exitCode = vfprintf(err_dest, cformat, err_args);
  assert(exitCode > 0);
  fprintf(err_dest, "\n");
  va_end(err_args);
}
#define throwError(fd, formatter, ...)                                         \
  errors_from(fd, __LINE__, __func__, formatter, ##__VA_ARGS__)

typedef struct List {
  char **entries;
  int size, tot;
} List;

void *safeMalloc(int sz) {
  void *p = calloc(sz, 1);
  if (p == NULL) {
    fprintf(stderr, "Fatal error: safeMalloc(%d) failed.\n", sz);
    exit(EXIT_FAILURE);
  }
  return p;
}

List makeList(int len) {
  List st;
  st.size = len;
  st.tot = 0;
  st.entries = safeMalloc(len * sizeof(char *));
  return st;
}

void freeList(List st) {
  for (int i = 0; i < st.tot; i++)
    free(st.entries[i]);
  free(st.entries);
}

void resizeList(List *lsp) {
  lsp->size *= 2;
  if ((lsp->entries = realloc(lsp->entries, lsp->size * sizeof(char *))) ==
      NULL)
    throwError(stderr, "Failed memory allocation!");
}

int isEmptyList(List lsp) { return (lsp.tot == 0); }

void append(char *s, List *lsp) {
  if (lsp->tot == lsp->size)
    resizeList(lsp);

  if ((lsp->entries[lsp->tot++] = strdup(s)) == NULL)
    throwError(stderr, "Failed push of string \"%s\", have \"%s\"\n", s,
               lsp->entries[lsp->tot]);
}

char *retrieve(List ls, int idx) {
  if (idx > ls.tot - 1) {
    throwError(stderr, "idx too high! have %d, current tot is %d - 1\n", idx,
               ls.tot);
    exit(EXIT_FAILURE);
  }
  return ls.entries[idx];
}

List deepCopy(List og) {
  List ret = makeList(og.size);
  ret.tot = og.tot;
  for (int i = 0; i < ret.tot; i++) {
    if ((ret.entries[i] = strdup(og.entries[i])) == NULL)
      throwError(stderr, "Failed copy of \"%s\", have \"%s\"\n", og.entries[i],
                 ret.entries[i]);
  }
  return ret;
}

void printList(List ls) {
  if (isEmptyList(ls))
    throwError(stderr, "Printing empty list!");
  for (int i = 0; i < ls.tot; i++)
    printf("%s\n", ls.entries[i]);
}

char *substr(int start, int end, char *og) {
  char *ret = safeMalloc((end - start + 1) * sizeof(char));
  for (int i = start; i < end; i++)
    ret[i - start] = og[i];
  ret[end - start] = '\0';
  return ret;
}

void test1() {
  List ex = makeList(3);
  char *str1 = "CIaoo belloo!!";
  char *str2 = "COme staiii";
  char *str3 = "Ultima stringa!!";
  char *str4 = "Dovrebbe passare per resize";

  append(str1, &ex);
  append(str2, &ex);
  append(str3, &ex);
  append(str4, &ex);
  append("Ultima per davvero", &ex);

  printList(ex);
  printf("Example retrieve!!!\n");
  for (int i = 0; i < ex.tot; i++)
    printf("%s\n", retrieve(ex, i));
  /* printf("failing... %s\n", retrieve(ex, 10)); */

  printf("%d %d\n", ex.tot, ex.size);

  List excp = deepCopy(ex);
  printf("Excp:\n");
  printList(excp);
  printf("%s\n", retrieve(excp, 3));
  /* printf("failing... %s\n", retrieve(excp, 10)); */

  freeList(excp);
  freeList(ex);
}

void test2() {
  char *PATH = getenv("PATH");
  printf("%lu %s\n\n", strlen(PATH), PATH);

  char *test1 = substr(8, strlen(PATH), PATH);
  char *ttt = substr(0, 4, "123456");
  /* printf("\n%s\n", test1); */
  /* printf("%d\n", test1[(strlen(PATH) - 8) + 1] == '\0' ? 1 : 0); */
  /* printf("%d\n", strcmp(PATH, substr(0, strlen(PATH), PATH))); */
  free(test1);
  free(ttt);

  List path_lookup = makeList(10);
  int start, i;
  char *tmp;
  start = i = 0;
  for (i = 0; i <= strlen(PATH); i++) {
    if (PATH[i] == ':' || i == strlen(PATH)) {
      tmp = substr(start, i, PATH);
      append(tmp, &path_lookup);
      start = i + 1;
      free(tmp);
    }
  }

  printList(path_lookup);
  freeList(path_lookup);
}

int main(int argc, char *argv[]) {
  test1();
  test2();
  return 0;
}
