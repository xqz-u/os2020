#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

  char input[64];
  int status;

  while (1) {
    printf("Enter input string:");
    scanf("%s", input);
    printf("%s\n", strcat(getenv("PATH"), "ls"));
    printf("input: %s\n", input);

    if (fork() == 0) {
      printf("Inside child code!\n");
      char *prog[] = {"/usr/bin/ls", "|", "grep", "example", NULL};
      execve(prog[0], prog, NULL);
    } else {
      printf("Dispatched the job to child process! going to sleep\n");
      waitpid(-1, &status, 0);
      printf("Child job finished, pid of father is %d\n", getpid());
    }
  }

  return 0;
}

/* int main(int argc, char *argv[]) { return 0; } */
